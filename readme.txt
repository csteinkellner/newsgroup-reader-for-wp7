Readme:

English:
The following programs are required by the App (there is no such thing as the Android apk-file in Windows Phone):
  - To compile the App Visual Studio 2010 with Windows Pohne 7.1 SDK is needed.
    Both the Profession-Edition and the Express-Edition (free version) can be used.
	The Windows Phone 7.1 SDK can be found here (incl. Visual Studio Express): http://msdn.microsoft.com/de-de/hh487282.aspx
	The full version of Visiual Studio 2010 (free for students) is found here: https://www.dreamspark.com/Product/Product.aspx?productid=4
	
  - To compile the App the Nuget-Extension is required (version >= 1.6):
	Visual Studio --> Tools --> Extension Manager --> Online Gallery --> Nuget Package Manager
	
  - Silverlight Toolkit for Windows Phone:
	http://silverlight.codeplex.com/ (the red download)
	
  - 
	
Compiling and starting the App:
  - Open the solution (NewsgroupReader.sln)
  - Restore NuGet Packeges
	- activate Enable NuGet Package Restore
	  (http://docs.nuget.org/docs/Workflows/Using-NuGet-without-committing-packages)
	- Select projects and click there "Manage NuGet Packages", then there should be a restore-button -> click restore
  - With the green play-Button the start-project gets started:
    This start-project can be changed by a right-click onto the project --> select as Start-Project
	Following projects can be started:
	- NewsgroupReader (the actual app)
	- NewsgroupReaderUT (the test suite)
	
Following tags can be used in the test suite:
  - Converters 		
  - Database 		
  - DataProvider	
  - TCP 
    If this test suite is started with the tag "TCP", the TCP server (this server is with the source code, too)is needed: 
	--> To configure the TCP-Server: right-click on "TCP_Server.exe - Verkn�pfung (2)" - Properties - Target: edit IP to your IP (not 127.0.0.1!!!)
	--> Configure in Code NewsgroupReaderUT-Tests-LibraryTest.cs : const string TCP_SERVER_ADDRESS to the server IP
	--> After "ECHO: HELLO" the user has to write "HELLO" back
  - DEV				
  - NN		
  - nntpDB  
  - Renderer

Deutsch:
Folgende Programme werden f�r die App ben�tigt (es gibt f�r Windows Phone nichts wie die Android-apk-Datei):
  - Zum kompilieren wird Visual Studio 2010 mit Windows Phone 7.1 SDK ben�tigt.
	Es funktioniert sowohl die Professional oder die Express-Edition (gratis Version) f�r Windows-Phone.
	Die Windows Phone 7.1 SDK findet man hier (inkl. Visual Studio Express): http://msdn.microsoft.com/de-de/hh487282.aspx
	Die Vollversion von Visual Studio 2010 (gratis f�r Studenten) gibt es hier: https://www.dreamspark.com/Product/Product.aspx?productid=4

  - Zum kompilieren der App wird die Nuget-Erweiterung (Version >= 1.6) ben�tigt:
	Visual Studio --> Extras --> Erweiterungs-Manager --> Onlinekatalog --> NuGet Package Manager
	
  - Silverlight Toolkit f�r Windows Phone:
	http://silverlight.codeplex.com/ (der rote Download)
	
Kompilieren und starten der App:
  - Solution �ffnen (NewsgroupReader.sln)
  - NuGet Packeges wiederherstellen
	- Enable NuGet Package Restore aktivieren
	  (http://docs.nuget.org/docs/Workflows/Using-NuGet-without-committing-packages)
	- Projekte ausw�hlen und dort "Manage NuGet Packages" klicken, dann sollte oben eine Restore-Meldung erscheinen -> Restore klicken.
  - Mit dem gr�nen Dreiecksymbol (Play-Button) wird das Startprojekt gestartet:
    Das Startprojekt ist das Projekt, welches im Projektmappen-Explorer fett gedruckt ist.
	Es kann durch Rechtsklick auf Projekt --> Als Startprojekt festlegen ausgew�hlt werden
	Folgende Projekte k�nnen gestartet werden:
	- NewsgroupReader (die eigentliche App)
	- NewsgroupReaderUT (die Test Suite)
	
Folgende Tags k�nnen in der Test Suite verwendet werden:
  - Converters 		
  - Database 		
  - DataProvider	
  - TCP
	Wenn die Test Suite mit dem Tag "TCP" gestartet wird, wird der TCP-server ben�tigt (im Archiv mit dem Source):
	--> Um den TCP-Server zu konfigurieren: Rechtsklick auf "TCP_Server.exe - Verkn�pfung (2)" - Eigenschaften - Ziel: �ndern der IP auf die eigene IP (nicht 127.0.0.1!!!)
	--> �ndern des Codes in NewsgroupReaderUT-Tests-LibraryTest.cs : const string TCP_SERVER_ADDRESS zur server IP
	--> Nach "ECHO: HELLO" muss der Benutzer "HELLO" zur�ckschreiben  
  - DEV				
  - NN		
  - nntpDB  
  - Renderer

