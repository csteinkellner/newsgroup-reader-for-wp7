﻿using System;
using System.Threading;
using System.Diagnostics;

namespace NNTPLibrary.Common {
    public class ThreadTimer {
        private object _lock;
        private bool _inUse;

        public TimeSpan Interval { get; set; }
        public delegate void EventHandler(object o, EventArgs e);
        public event EventHandler Tick;

        public ThreadTimer() {
            _lock = new object();
            _inUse = false;
        }

        public void Start() {
            try {
                lock (_lock) {
                    if (!_inUse) {
                        _inUse = true;
                        ThreadPool.QueueUserWorkItem(lambda =>
                        {
                            execute(Interval);
                        });
                    }
                }
            } catch(Exception ex){
                Debug.WriteLine("ThreadTimer Exception: "+ex.Message);
                if (Tick != null) {
                    Tick.Invoke(this, new EventArgs());
                }
            }
        }

        private void execute(TimeSpan t_span) {
            Thread.Sleep(t_span);
            _inUse = false;
            if (Tick != null) {
                Tick.Invoke(this, new EventArgs());
            }
        }
    }
}

