﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NNTPLibrary.Common {
    public class Rfc977Commands {
        public const string LISTGROUP = "LISTGROUP";
        public const string LIST_ACTIVE = "LIST ACTIVE";
        public const string ARTICLE = "ARTICLE";
        public const string POST = "POST";
    }
}
