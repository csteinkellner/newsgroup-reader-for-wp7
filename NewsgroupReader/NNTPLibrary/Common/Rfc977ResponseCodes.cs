﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NNTPLibrary.Common {
    public class Rfc977ResponseCodes {

        //GENERAL
        public const int POSTING_ALLOWED = 200; //Posting allowed
        public const int POSTING_PROHIBITED = 201; //Posting prohibited
        public const int TEMPORARILY_UNAVAILABLE = 400; //Service temporarily unavailable
        public const int PERMANENTLY_UNAVAILABLE = 502; //Reading service permanently unavailable

        //LISTGROUP
        public const int ARTICLE_NUMBER_FOLLOW = 211; //Article numbers follow (multi-line)
        public const int NO_SUCH_NEWSGROUP = 411; //No such newsgroup
        public const int NO_NEWSGROUP_SELECTED = 412; //No newsgroup selected
        public const int NO_GROUP_SPECIFIED = 481; //No newsgroup selected

        //LIST
        public const int INFORMATION_FOLLOWS = 215; //Information follows (multi-line)

        //ARTICLE
        public const int ARTICLE_FOLLOWS = 220; //Article follows (multi-line)
        public const int NO_ARTICLE_WITH_THAT_MESSAGE_ID = 430; //No article with that message-id
        //public const int NoNewsgroupSelected = 412; //No newsgroup selected
        public const int NO_ARTICLE_WITH_THAT_NUMBER = 423; //No article with that number
        public const int CURRENT_ARTICLE_NUMBER_IS_INVALID = 420; //Current article number is invalid


        //POST
        // ->Initial responses
        public const int SEND_ARTIVLE_TO_BE_POSTED = 340; //Send article to be posted
        public const int POSTING_NOT_PERMITTED = 440; //Posting not permitted
        // ->Subsequent responses
        public const int ARTIVLE_RECEIVED_OK = 240; //Article received OK
        public const int POSTING_FAILD = 441; //Posting failed
         
    }
}
