﻿using System;
using System.Text;
using System.Collections.Generic;

namespace NNTPLibrary.Common {

    public class Utils {

        /// <summary>
        /// Search for byte[] patern in byte[]
        /// Partially taken from: http://social.msdn.microsoft.com/Forums/en/csharpgeneral/thread/15514c1a-b6a1-44f5-a06c-9b029c4164d7
        /// </summary>
        /// <param name="arrayToSearchThrough"></param>
        /// <param name="patternToFind"></param>
        /// <returns>Index</returns>
        public static int IndexOf(byte[] arrayToSearchThrough, byte[] patternToFind) {
            if (arrayToSearchThrough == null) {
                arrayToSearchThrough = new byte[0];
            }
            if (patternToFind == null) {
                return -1;
            }
            if (patternToFind.Length > arrayToSearchThrough.Length)
                return -1;
            for (int iterator = 0; iterator <= arrayToSearchThrough.Length - patternToFind.Length; iterator++) {
                bool found = true;
                for (int counter = 0; counter < patternToFind.Length; counter++) {
                    if (arrayToSearchThrough[iterator + counter] != patternToFind[counter]) {
                        found = false;
                        break;
                    }
                }
                if (found) {
                    return iterator;
                }
            }
            return -1;
        }

        /// <summary>
        /// Combine 2 arrays
        /// </summary>
        /// <returns>return = array1 + array2</returns>
        public static byte[] Combine(byte[] array1, byte[] array2) {
            if (array1 == null) { array1 = new byte[0]; }
            if (array2 == null) { array2 = new byte[0]; }

            int size = (array1 != null ? array1.Length : 0) + (array2 != null ? array2.Length : 0);
            byte[] rv = new byte[size];
            int offset = 0;

            System.Buffer.BlockCopy(array1, 0, rv, offset, array1.Length);
            offset += array1.Length;
            System.Buffer.BlockCopy(array2, 0, rv, offset, array2.Length);
            offset += array2.Length;

            return rv;
        }

        public static byte[] Copy(byte[] array, int startIndex, int length){
            if (array == null) { array = new byte[0]; }

            byte[] result = new byte[length];
            System.Buffer.BlockCopy(array, startIndex, result, 0, length);
            return result;
        }

        public static Encoding DefaultEncoding { get { return Encoding.UTF8; }  }

        public static Encoding GetEncoding(string EncodingName) {
            try {
                return Encoding.GetEncoding(EncodingName);
            } catch { }
            //Our encodings
            if (EncodingName.ToUpper() == "ISO-8859-15") {
                return Encoding.GetEncoding("ISO-8859-1");
            } else {
                throw new NotSupportedException();
            }

        }
    }
}
