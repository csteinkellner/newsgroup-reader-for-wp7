﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace NNTPLibrary.Common {
    public class NameToCodeRelation {
        private Dictionary<int, CodeInformation> _Data;
        public Dictionary<int, CodeInformation> Relation { get { return _Data; } private set { } }

        private static NameToCodeRelation _Instance = null;

        private NameToCodeRelation() {
            _Data = new Dictionary<int, CodeInformation>();
            createDictionary();
        }

        public static NameToCodeRelation GetInstance() {
            if (_Instance == null) {
                _Instance = new NameToCodeRelation();
            }
            return _Instance;
        }

        private void createDictionary() {
            //GENERAL
            _Data.Add(Rfc977ResponseCodes.POSTING_ALLOWED, new CodeInformation() 
            { CommandNames = new string[] { }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.POSTING_PROHIBITED, new CodeInformation() 
            { CommandNames = new string[] { }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.TEMPORARILY_UNAVAILABLE, new CodeInformation() 
            { CommandNames = new string[] { }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.PERMANENTLY_UNAVAILABLE, new CodeInformation() 
            { CommandNames = new string[] { }, TerminationSequence = "\r\n" });

            //LISTGROUP
            _Data.Add(Rfc977ResponseCodes.ARTICLE_NUMBER_FOLLOW, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.LISTGROUP }, TerminationSequence = "\r\n.\r\n" });
            _Data.Add(Rfc977ResponseCodes.NO_SUCH_NEWSGROUP, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.LISTGROUP }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.NO_NEWSGROUP_SELECTED, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.LISTGROUP, Rfc977Commands.ARTICLE}, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.NO_GROUP_SPECIFIED, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.LISTGROUP }, TerminationSequence = "\r\n" });
            
            //LIST
            _Data.Add(Rfc977ResponseCodes.INFORMATION_FOLLOWS, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.LIST_ACTIVE }, TerminationSequence = "\r\n.\r\n" });

            //ARTICLE
            _Data.Add(Rfc977ResponseCodes.ARTICLE_FOLLOWS, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.ARTICLE }, TerminationSequence = "\r\n.\r\n" });
            _Data.Add(Rfc977ResponseCodes.NO_ARTICLE_WITH_THAT_MESSAGE_ID, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.ARTICLE }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.NO_ARTICLE_WITH_THAT_NUMBER, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.ARTICLE }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.CURRENT_ARTICLE_NUMBER_IS_INVALID, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.ARTICLE }, TerminationSequence = "\r\n" });

            //POST
            _Data.Add(Rfc977ResponseCodes.SEND_ARTIVLE_TO_BE_POSTED, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.POST }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.POSTING_NOT_PERMITTED, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.POST }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.ARTIVLE_RECEIVED_OK, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.POST }, TerminationSequence = "\r\n" });
            _Data.Add(Rfc977ResponseCodes.POSTING_FAILD, new CodeInformation() 
            { CommandNames = new string[] { Rfc977Commands.POST }, TerminationSequence = "\r\n" });

        }

    }
}
