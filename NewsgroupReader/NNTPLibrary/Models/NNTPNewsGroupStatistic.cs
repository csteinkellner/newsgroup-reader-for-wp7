﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NNTPLibrary.Models
{
    public class NNTPNewsGroupStatistic : NNTPObject
    {
        /// <summary>
        /// Name of newsgroup
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// Estimated number of articles in the group
        /// </summary>
        public int NumberOfArticles { get; set; }
        /// <summary>
        /// Reported low water mark
        /// </summary>
        public int LowWaterMark { get; set; }
        /// <summary>
        /// Reported high water mark
        /// </summary>
        public int HighWaterMark { get; set; }
    }
}
