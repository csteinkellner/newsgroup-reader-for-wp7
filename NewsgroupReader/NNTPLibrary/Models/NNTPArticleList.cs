﻿using System;
using System.Collections.Generic;

namespace NNTPLibrary.Models
{
    public class NNTPArticleList : NNTPObject
    {
        private List<int> _ArticleNumbers = new List<int>();
        public List<int> ArticleNumbers { get { return _ArticleNumbers; } private set { _ArticleNumbers = value; } }

        public String Group { get; set; }
        public int Low { get; set; }
        public int Hight { get; set; }
        public int Number { get; set; }
    }
}
