﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NNTPLibrary.Models
{
    public class NNTPGroup : NNTPObject
    {
        public enum EPosting { Allowed, Prohibited, Forwarded }

        public String Name { get; set; }
        public int NumberOfLastArticle { get; set; }
        public int NumberOfFirstArticle { get; set; }
        public EPosting Posting { get; set; }


    }
}
