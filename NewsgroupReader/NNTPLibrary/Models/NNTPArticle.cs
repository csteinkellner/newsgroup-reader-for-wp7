﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NNTPLibrary.Models
{
    public class NNTPArticle : NNTPObject
    {
        public int ArticleNumber { get; set; }
        public string Path { get; set; }
        public string From { get; set; }
        public string Newsgroup { get; set; }
        public string Subject { get; set; }
        public DateTime Date { get; set; }
        public string Organization { get; set; }
        public string MessageId { get; set; }
        public string[] References { get; set; }
        public string ContentType { get; set; }
        public string ContentTransferEncoding {get;set;}
        
        public string Message { get; set; }
    }
}
