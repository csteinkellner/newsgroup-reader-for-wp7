﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NNTPLibrary.Models
{
    public class NNTPGroupList : NNTPObject
    {
        private List<NNTPGroup> _Groups = new List<NNTPGroup>();
        public List<NNTPGroup> Groups {
            get {
                return _Groups;
            }
            set {
                _Groups = value;
            }
        }

    }
}
