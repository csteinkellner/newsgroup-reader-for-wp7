﻿using System;
using System.Text;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using NNTPLibrary.Net;
using NNTPLibrary.Models;
using NNTPLibrary.Parsing;
using NNTPLibrary.State;
using NNTPLibrary.Common;
using System.Windows.Threading;
using System.Collections.Generic;


namespace NNTPLibrary
{
    public partial class NNTPApi
    {
        private NetworkClient _Server = null;
        private byte[] _Data = null;
        private Context _NNTPState = null;
        private String _Dns=null;
        private int _Port=0;
        private ThreadTimer _EstablishedTimer =  new ThreadTimer();
        private int _ConnectionCounter;
        private object _lock = new object();


        public NNTPApi(String dns, UInt16 port)
        {
            _Server = new SlackSocketClientV2();// SlackSocketClient();
            _Server.Event_DataReceivedHandler += new NetworkClient.DataReceived(_Event_DataReceivedHandler);
            _Server.Event_TransferDoneHandler += new NetworkClient.TransferDone(_Server_Event_TransferDoneHandler);
            _Server.Event_ConnectionEstablishedHandler += new NetworkClient.ConnectionEstablished(_Server_Event_TCPConnectionEstablishedHandler);
            _Dns = dns;
            _Port = port;

            _EstablishedTimer.Interval = TimeSpan.FromMilliseconds(5000);
            _EstablishedTimer.Tick += new ThreadTimer.EventHandler(_EstablishedTimer_Tick);

            _NNTPState = new Context(new BaseState(), this);
        }

        public void Initialize() {
            _Server.Connect(_Dns, _Port);
            _ConnectionCounter = 0;

        }
        private DateTime dt = new DateTime();
        private void _Server_Event_TCPConnectionEstablishedHandler(bool establised, SocketError error, string message) {
            if (establised == false) {
                CallConnectionStatus(establised, error, ENNTPConnection.Unknown);
            } else {
                if (_ConnectionCounter == 0) {
                    dt = DateTime.Now;
                    _EstablishedTimer.Start();
                }
            }
            _ConnectionCounter++;
        }
        private DateTime dt2 = new DateTime();
        void _EstablishedTimer_Tick(object sender, EventArgs e) {
            if (_NNTPState.State.GetType() == typeof(BaseState)) {
                //_Server.Disconnect();
                dt2 = DateTime.Now;
                CallConnectionStatus(false, SocketError.Success, ENNTPConnection.Unknown);
            }
        }

        private void _Server_Event_TransferDoneHandler(bool error, string data) {
            if (error) {
                CallInternalError(EInternalError.TransferFailed);
            } else { /*data recieved on the server*/ }
        }

        private void _Event_DataReceivedHandler(object obj, DataEventArgs data){
            try{
                if (data.ErrorCode == null){
                    //lock isn't necessary
                    lock (_lock) {
                        _Data = Utils.Combine(_Data, data.Data); //_Data += data.Data; 
        
                        while (true) {
                            byte[] command = CommandParser.GetCommand(ref _Data);
                            if (command == null) break;
                            _NNTPState.Request(command);
                        }
                    }
                }else{
                    throw new Exception(data.ErrorCode + " " + data.Data);
                }
            }
            catch (Exception ex){
                Debug.WriteLine("Excpetion in NNTPApi: " + ex.Message);
            }
        }

        ~NNTPApi() {
            _EstablishedTimer.Tick -= new ThreadTimer.EventHandler(_EstablishedTimer_Tick);
            _Server.Event_DataReceivedHandler -= new NetworkClient.DataReceived(_Event_DataReceivedHandler);
            _Server.Event_TransferDoneHandler -= new NetworkClient.TransferDone(_Server_Event_TransferDoneHandler);
            _Server.Event_ConnectionEstablishedHandler -= new NetworkClient.ConnectionEstablished(_Server_Event_TCPConnectionEstablishedHandler);
            _Server.Dispose();
        }

        /// <summary>
        /// Get all newsgroups
        /// </summary>
        public void GetAllNewsgroupsAsynch(){
            _Server.Send(Rfc977Commands.LIST_ACTIVE + "\r\n");
        }

        /// <summary>
        /// Selects a newsgroup and lists all articlenumbers 
        /// </summary>
        public void SelectNewsgroupAndListArticles(string group){
            _Server.Send(Rfc977Commands.LISTGROUP + " " + group + "\r\n");
        }

        /// <summary>
        /// Get an article by article number
        /// </summary>
        public void GetArticle(int ArticleNumber){
            _Server.Send(Rfc977Commands.ARTICLE + " " + ArticleNumber + "\r\n");
        }
    }
}
