﻿/*
* Autor: Harald Schaffernak
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace NNTPLibrary.Net {


    public class TCPClientWP : NetworkClient {

        private const int TIME_OUT = 3000;
        private const int BUFFER_SIZE = 4096;
        public enum EcodingTyp { UTF8Encoding, UnicodeEncoding, UTF7Encoding, ISO_8859_1 }
        private Socket _sock;
        private string _ReceivedData;
        private int _port;
        private EndPoint _endPoint = null;
        private AutoResetEvent _con = null;
        private AutoResetEvent _snd = null;

        private void ConnectionWaitOne() { if (!pConnectAsynchron)_con.WaitOne(); }
        private void ConnectionReset() { if (!pConnectAsynchron)_con.Reset(); }
        private void ConnectionSet() { if (!pConnectAsynchron)_con.Set(); }


        public TCPClientWP(bool ConnectAsynchron, EcodingTyp encoding)
            : base(true, false, ConnectAsynchron, setEncoding(encoding)) {

            _con = new AutoResetEvent(false);
            _snd = new AutoResetEvent(false);
            ConnectionReset();
        }


        public override void Connect(string Host, int Port) {
            _port = Port;
            _endPoint = new DnsEndPoint(Host, Port);
            connect();
        }

        private void connect() {
            _sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            SocketAsyncEventArgs socketReceiveEventArgs = new SocketAsyncEventArgs();
            socketReceiveEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(_socketReceiveEventArgs_Completed);
            socketReceiveEventArgs.RemoteEndPoint = _endPoint;

            _sock.ConnectAsync(socketReceiveEventArgs);

            ConnectionWaitOne();
        }

        public override void Reconnect() {
            Disconnect();
            connect();
        }

        private void _socketSendEventArgs_Completed(object sender, SocketAsyncEventArgs e) {
            try {
                if (e.SocketError != SocketError.Success) {
                    OnTransferDoneHandler(e.SocketError);
                    return;
                }

                string data = UsedEncoding.GetString(e.Buffer, 0, e.Buffer.Length);

                switch (e.LastOperation) {
                    case SocketAsyncOperation.Send:
                        OnTransferDoneHandler(e.SocketError);
                        break;
                    case SocketAsyncOperation.SendTo:
                        break;
                    default:
                        break;
                }
            } finally {
                if (pSendAsynchron) {
                    e.Completed -= new EventHandler<SocketAsyncEventArgs>(_socketSendEventArgs_Completed);
                    e.Dispose();
                }
            }
        }

        private void _socketReceiveEventArgs_Completed(object sender, SocketAsyncEventArgs e) {
            if (e.SocketError != SocketError.Success) {
                Disconnect();
                OnConnectionEstablishedHandler(SocketError.SocketError);
                return;
            }

            switch (e.LastOperation) {
                case SocketAsyncOperation.Connect:
                    ConnectionSet();
                    OnConnectionEstablishedHandler(e.SocketError);
                    receive(e);
                    break;
                case SocketAsyncOperation.Receive:
                    if (e.BytesTransferred > 0) {
                        string data = UsedEncoding.GetString(e.Buffer, 0, e.BytesTransferred);

                        _ReceivedData += data;
                        if (data.Substring(data.Length - 3, 3) == ".\r\n") {
                            OnDataEventHandler(new DataEventArgs(_ReceivedData, false));
                            _ReceivedData = null;
                        } else {

                        }
                    } else {
                        //bool FUCK = true;
                    }
                    receive(e);
                    break;
                case SocketAsyncOperation.ReceiveFrom:
                    break;
                default:
                    break;
            }

        }

        private void receive(SocketAsyncEventArgs old) {
            try {
                old.Dispose();
            } catch { /*SILENCE*/}
            try {
                SocketAsyncEventArgs socketReceiveEventArgs = new SocketAsyncEventArgs();
                socketReceiveEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(_socketReceiveEventArgs_Completed);
                socketReceiveEventArgs.RemoteEndPoint = _endPoint;
                byte[] b = new byte[BUFFER_SIZE];
                socketReceiveEventArgs.SetBuffer(b, 0, BUFFER_SIZE);
                if (!_sock.ReceiveAsync(socketReceiveEventArgs)) {
                    _socketReceiveEventArgs_Completed(_sock, socketReceiveEventArgs);
                }
            } catch (Exception ex) {
                Disconnect();
                OnConnectionEstablishedHandler(SocketError.NetworkDown, ex.Message);
            }
        }

        /// <summary>
        /// Sends data to the server
        /// </summary>
        public override Boolean Send(String data) {
            try {
                SocketAsyncEventArgs _socketSendEventArgs;
                _socketSendEventArgs = new SocketAsyncEventArgs();
                _socketSendEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(_socketSendEventArgs_Completed);
                _socketSendEventArgs.RemoteEndPoint = _endPoint;

                byte[] c_data = UsedEncoding.GetBytes(data);
                _socketSendEventArgs.SetBuffer(c_data, 0, c_data.Length);

                if (!_sock.SendAsync(_socketSendEventArgs)) {
                    _socketSendEventArgs_Completed(_sock, _socketSendEventArgs);
                }
                return true;

            } catch {
                OnTransferDoneHandler(SocketError.NotConnected);
                return false;
            }
        }


        /// <summary>
        /// Closes the Socket connection and releases all associated resources
        /// </summary>
        public override void Disconnect() {
            try {
                _sock.Close();
                _sock = null;
            } catch {
                /*SILENCE*/
            }

        }

        public static Encoding setEncoding(EcodingTyp encodingtyp) {
            Encoding used_encoding = null;
            switch (encodingtyp) {
                case EcodingTyp.UTF8Encoding:
                    used_encoding = Encoding.UTF8;
                    break;
                case EcodingTyp.UnicodeEncoding:
                    used_encoding = Encoding.Unicode;
                    break;
                case EcodingTyp.UTF7Encoding:
                    throw new NotSupportedException();
                case EcodingTyp.ISO_8859_1:
                    used_encoding = Encoding.GetEncoding("ISO-8859-1");
                    break;
                default:
                    used_encoding = Encoding.UTF8;
                    break;
            }
            return used_encoding;
        }


        public override bool isConnected()
        {
            return _sock.Connected;
        }

        private string ByteLog(byte[] array, int size) {
            string output = "";
            for (int i = 0; i < size; i++) {
                if ((i % 16) == 0) {
                    output += Environment.NewLine;
                }
                output += (string.Format("{0:x}", Convert.ToInt32(array[i])));
                output += " ";
            }
            return output;
        }
    }
}
