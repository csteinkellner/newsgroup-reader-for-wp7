﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Windows.Threading;

namespace NNTPLibrary.Net {
    public abstract class NetworkClient {

        public event DataReceived Event_DataReceivedHandler;
        public delegate void DataReceived(object obj, DataEventArgs data);
        protected bool pSendAsynchron;
        protected bool pConnectAsynchron;
        protected System.Text.Encoding UsedEncoding;
        private object _lock = new object();

        public NetworkClient(bool SendAsynchron,
                             bool ConnectAsynchron, System.Text.Encoding encoding) {
            UsedEncoding = encoding;
            pSendAsynchron = SendAsynchron;
            pConnectAsynchron = ConnectAsynchron;
        }

        public abstract Boolean Send(String data);
        public abstract void Disconnect();
        public abstract void Connect(string Host, Int32 DestinationPort);
        public abstract void Reconnect();
        public abstract bool isConnected();
        public abstract void Dispose();

        protected virtual void OnDataEventHandler(DataEventArgs e) {
            if (Event_DataReceivedHandler != null) {
                Event_DataReceivedHandler(this, e as DataEventArgs);
            }
        }

        public event ConnectionEstablished Event_ConnectionEstablishedHandler;
        public delegate void ConnectionEstablished(bool establised, SocketError error, String message);
        protected virtual void OnConnectionEstablishedHandler(SocketError error, String message = null) {
            if (Event_ConnectionEstablishedHandler != null) {
                if (error == SocketError.Success) {
                    Event_ConnectionEstablishedHandler(true, error, message);
                } else {
                    Event_ConnectionEstablishedHandler(false, error, message);
                }
            }
        }
        public event TransferDone Event_TransferDoneHandler;
        public delegate void TransferDone(bool error, String data);
        protected virtual void OnTransferDoneHandler(SocketError e) {
            if (Event_TransferDoneHandler != null) {
                if (e != SocketError.Success) {
                    Event_TransferDoneHandler(true, Enum.GetName(typeof(SocketError), e));
                } else {
                    Event_TransferDoneHandler(false, null);
                }
            }
        }
    }
}
