﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

namespace NNTPLibrary.Net {
    public class SlackSocketClient : NetworkClient {
        public enum EcodingTyp { UTF8Encoding, UnicodeEncoding, UTF7Encoding, ISO_8859_1 }

        private Socket _socket = null;
        //
        private ManualResetEvent _WaitSend = new ManualResetEvent(false);
        private ManualResetEvent _WaitReceive = new ManualResetEvent(false);

        private const int TIMEOUT_MILLISECONDS = 5000;
        private const int MAX_BUFFER_SIZE = 4096;
        private int _port;
        private EndPoint _endPoint = null;
        private BlockingQueue<String> _Send;
        private Thread _ReceiveThread = null;
        private Thread _SendThread = null;
        private bool _Stop = false;

        public SlackSocketClient()
            : base(false, true, Encoding.UTF8) {
                throw new NotSupportedException("Use SlackSocketClientV2");
            _Send = new BlockingQueue<String>();

            _WaitSend.Reset();
            _WaitReceive.Reset();

            ParameterizedThreadStart pts = new ParameterizedThreadStart(this.ReceiveLoop);
            _ReceiveThread = new Thread(pts);
            _ReceiveThread.Name = "Receiving_Thread";

            ParameterizedThreadStart pts2 = new ParameterizedThreadStart(this.SendLoop);
            _SendThread = new Thread(pts2);
            _SendThread.Name = "Sending_Thread";
        }

        ~SlackSocketClient() {
            Dispose();
        }

        public override void Connect(string Host, int Port) {
            if (!_ReceiveThread.IsAlive && !_SendThread.IsAlive) {
                _Stop = false;
                Disconnect();
                _port = Port;
                _endPoint = new DnsEndPoint(Host, Port);

                _ReceiveThread.Start();
                _SendThread.Start();


                //_connection_Done.WaitOne(TIMEOUT_MILLISECONDS);
            }
            connect();
        }

        /// <summary>
        /// SlackSocketClient does not require a reconnect
        /// </summary>
        public override void Reconnect() {
            connect();
        }

        public override void Disconnect() {
            _WaitReceive.Reset();
            _WaitSend.Reset();
            if (_socket != null) {
                try {
                    _socket.Shutdown(SocketShutdown.Both);
                    _socket.Close();
                } catch (Exception ex) { Debug.WriteLine(ex.Message); }
            }
            _socket = null;
        }

        public override void Dispose() {
            _Stop = true;
            Disconnect();
            _WaitSend.Set();
            _WaitReceive.Set();
        }

        public override bool isConnected() {
            if (_socket == null) return false;
            return _socket.Connected;
        }

        private ManualResetEvent _connection_Done = new ManualResetEvent(false);
        private void connected(object sender, SocketAsyncEventArgs e) {
            _socket = e.UserToken as Socket;

            _WaitSend.Set();
            _WaitReceive.Set();
            _connection_Done.Set();
            OnConnectionEstablishedHandler(e.SocketError);
        }

        private void connect() {
            if (_socket == null || _socket.Connected == false) {
                Disconnect();

                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                SocketAsyncEventArgs socketEventArg = new SocketAsyncEventArgs();
                socketEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(connected);
                socketEventArg.RemoteEndPoint = _endPoint;
                socketEventArg.UserToken = _socket;

                //_connection_Done.Reset();
                _socket.ConnectAsync(socketEventArg);

                _connection_Done.Reset();
                _connection_Done.WaitOne(TIMEOUT_MILLISECONDS);
            }
        }

        public override Boolean Send(String data) {
            _Send.Enqueue(data);
            return true;
        }

        private void SendLoop(object o) {
            while (!_Stop) {
                send(_Send.Get());
            }
        }

        private ManualResetEvent _send_Done = new ManualResetEvent(false);
        private Boolean send(String data) {
            connect();
            _WaitSend.WaitOne();
            //_connection_Done.WaitOne();
            if (_Stop) { return false; }

            SocketAsyncEventArgs socketEventArg = new SocketAsyncEventArgs();
            try {
                if (_socket != null) {

                    socketEventArg.RemoteEndPoint = _socket.RemoteEndPoint;
                    socketEventArg.UserToken = _socket;
                    socketEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(delegate(object s, SocketAsyncEventArgs e) {
                        OnTransferDoneHandler(e.SocketError);
                        _send_Done.Set();
                    });

                    byte[] payload = Encoding.UTF8.GetBytes(data);
                    socketEventArg.SetBuffer(payload, 0, payload.Length);

                    _send_Done.Reset();

                    _socket.SendAsync(socketEventArg);
                    _send_Done.WaitOne(TIMEOUT_MILLISECONDS);
                } else {
                    OnTransferDoneHandler(SocketError.NotConnected);
                    return false;
                }
            } finally {
                socketEventArg.Dispose();
                socketEventArg = null;
            }
            return true;
        }

        private void ReceiveLoop(object o) {
            while (!_Stop) {
                string data = Receive();

                if (data != null) {
                    OnDataEventHandler(new DataEventArgs(data, false));
                }
            }
        }

        private ManualResetEvent _receive_Done = new ManualResetEvent(false);
        private string Receive() {
            _WaitReceive.WaitOne();
            if (_Stop) { return null; }
            string response = null;
            SocketAsyncEventArgs socketEventArg = new SocketAsyncEventArgs();
            try {
                if (_socket != null) {

                    socketEventArg.RemoteEndPoint = _endPoint;
                    socketEventArg.SetBuffer(new Byte[MAX_BUFFER_SIZE], 0, MAX_BUFFER_SIZE);
                    socketEventArg.UserToken = _socket;

                    socketEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(delegate(object s, SocketAsyncEventArgs e) {
                        try {
                            if (e.BytesTransferred == 0) {
                                Disconnect();
                                //connect(); //Stop communication, connect on next send(...)
                                return;
                            }
                            if (e.SocketError == SocketError.Success) {
                                response = UsedEncoding.GetString(e.Buffer, e.Offset, e.BytesTransferred);
                                response = response.Trim('\0');
                            } else {
                                OnConnectionEstablishedHandler(e.SocketError);
                            }
                        } finally {
                            e.Dispose();
                            _receive_Done.Set();
                        }
                    });

                    _receive_Done.Reset();

                    _socket.ReceiveAsync(socketEventArg);
                    _receive_Done.WaitOne();
                } else {
                    OnConnectionEstablishedHandler(SocketError.NetworkDown, "Socket is null!");
                    connect();
                }
            } catch {
                OnConnectionEstablishedHandler(SocketError.NetworkDown, "Fatal Error!");
                connect();
            } finally {
                socketEventArg.Dispose();
                socketEventArg = null;
            }
            return response;
        }



    }
}
