﻿//used from http://jaksa76.blogspot.com/2009/03/blocking-queue-in-c.html
using System;
using System.Net;
using System.Collections.Generic;
using System.Threading;

namespace NNTPLibrary.Net {
    public class BlockingQueue<T> {
        private Queue<T> q = new Queue<T>();

        public void Enqueue(T element) {
            q.Enqueue(element);
            lock (q) {
                Monitor.Pulse(q);
            }
        }

        public T Get() {
            lock (q) {
                while (q.Count == 0) {
                    Monitor.Wait(q);
                }
                return q.Dequeue();
            }
        }
    }
}
