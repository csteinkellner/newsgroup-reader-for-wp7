﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NNTPLibrary.Net {
    public class DataEventArgs {
        public DataEventArgs(byte[] data, Boolean error) {
            this.Data = data;
            this.Error = error;
        }
        public DataEventArgs(String message, String errorCode, Boolean error) {
            this.Message = message;
            this.Error = error;
            this.ErrorCode = errorCode;
        }
        public readonly byte[] Data;
        public readonly string Message;
        public readonly Boolean Error;
        public readonly String ErrorCode;
    }
}
