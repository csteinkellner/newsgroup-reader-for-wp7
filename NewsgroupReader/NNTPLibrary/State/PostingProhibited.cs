﻿using NNTPLibrary.Parsing;
using NNTPLibrary.Common;
using System.Diagnostics;

namespace NNTPLibrary.State {
    public class PostingProhibited : State {

        public override void Handle(Context context, byte[] command) {

            int code = (int)CommandParser.getResponsesCode(command);
            switch (code) {
                case Rfc977ResponseCodes.INFORMATION_FOLLOWS:
                    context.ListParser.parse(command);
                    context.State = new PostingProhibited();
                    break;
                case Rfc977ResponseCodes.ARTICLE_NUMBER_FOLLOW:
                    context.ListGroupParser.parse(command);
                    context.State = new PostingProhibited();
                    break;
                case Rfc977ResponseCodes.ARTICLE_FOLLOWS:
                    context.ArticleParser.parse(command);
                    context.State = new PostingProhibited();
                    break;
                default:
                    Debug.WriteLine("Response code not understood!");
                    context.State = new PostingProhibited();
                    return;
            }
        }
    }
}
