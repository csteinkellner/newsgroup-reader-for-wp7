﻿
namespace NNTPLibrary.State {
    public abstract class State {
        public abstract void Handle(Context context, byte[] command);
    }
}
