﻿using NNTPLibrary.Parsing;
using NNTPLibrary.Common;
using System.Diagnostics;

namespace NNTPLibrary.State {
    public class PostingAllowed : State {

        public override void Handle(Context context, byte[] command) {

            int code = (int)CommandParser.getResponsesCode(command);
            switch (code) {
                case Rfc977ResponseCodes.INFORMATION_FOLLOWS:
                    context.ListParser.parse(command);
                    context.State = new PostingAllowed();
                    break;
                case Rfc977ResponseCodes.ARTICLE_NUMBER_FOLLOW:
                    context.ListGroupParser.parse(command);
                    context.State = new PostingAllowed();
                    break;
                case Rfc977ResponseCodes.ARTICLE_FOLLOWS:
                    context.ArticleParser.parse(command);
                    context.State = new PostingAllowed();
                    break;
                case Rfc977ResponseCodes.POSTING_ALLOWED:
                    // On relogin, do nothing
                    context.State = new PostingAllowed();
                    break;
                case Rfc977ResponseCodes.POSTING_PROHIBITED:
                    // On relogin, do nothing
                    context.State = new PostingAllowed();
                    break;
                default:
                    Debug.WriteLine("Response code \"" + code + "\" not understood!");
                    context.State = new PostingAllowed();
                    return;
            }
        }
    }
}
