﻿using NNTPLibrary.Common;
using NNTPLibrary.Parsing;
using System.Diagnostics;

namespace NNTPLibrary.State {
    public class BaseState : State {
        public override void Handle(Context context, byte[] command) {

            int code = (int)CommandParser.getResponsesCode(command);
            switch (code) {
                case Rfc977ResponseCodes.POSTING_ALLOWED:
                    context.PostingAllowed.parse(command);
                    context.State = new PostingAllowed();
                    return;
                case Rfc977ResponseCodes.POSTING_PROHIBITED:
                    context.PostingProhibited.parse(command);
                    context.State = new PostingProhibited();
                    return;
                case Rfc977ResponseCodes.TEMPORARILY_UNAVAILABLE:
                    context.ServerUnavailable.parse(command);
                    context.State = new BaseState();
                    return;
                case Rfc977ResponseCodes.PERMANENTLY_UNAVAILABLE:
                    context.ServerUnavailable.parse(command);
                    context.State = new BaseState();
                    return;
                default:
                    Debug.WriteLine("Response code \"" + code + "\" not understood!");
                    context.State = new BaseState();
                    return;
            }
        }
    }
}
