﻿using System;
using System.Net;
using NNTPLibrary.Parsing;

namespace NNTPLibrary.State {
    public class Context {
        private State state;
        public IParse ListParser = null;
        public IParse ListGroupParser = null;
        public IParse ArticleParser = null;
        public IParse PostingAllowed = null;
        public IParse PostingProhibited = null;
        public IParse ServerUnavailable = null;

        public Context(State state, IFire fire) {
            this.State = state;

            ListParser = new ParseLIST(fire);
            ListGroupParser = new ParseLISTGROUP(fire);
            ArticleParser = new ParseARTICLE(fire);
            PostingAllowed = new ParserPostingAllowed(fire);
            PostingProhibited = new ParserPostingProhibited(fire);
            ServerUnavailable = new ParserServerUnavailable(fire);
        }

        public State State {
            get { return state; }
            set {
                state = value;
            }
        }

        public void Request(byte[] command) {
            state.Handle(this, command);
        }
    }
}
