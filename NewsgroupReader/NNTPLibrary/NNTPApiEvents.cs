﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using NNTPLibrary.Models;
using System.Collections.Generic;
using NNTPLibrary.Parsing;
using System.Net.Sockets;
using System.Threading;

namespace NNTPLibrary {
    /// <summary>
    /// Very important: every event has to rise in a new thread
    /// </summary>
    public partial class NNTPApi : IFire {
        #region EVENTS

        public delegate void GetAllNewsgroupsCompleted(NNTPGroupList groups);
        public event GetAllNewsgroupsCompleted Event_GetAllNewsgroupsCompleted_Handler;

        public delegate void SelectNewsgroupAndListArticlesCompleted(NNTPArticleList articles);
        public event SelectNewsgroupAndListArticlesCompleted Event_SelectNewsgroupAndListArticlesCompleted_Handler;

        public delegate void GetArticleCompleted(NNTPArticle article);
        public event GetArticleCompleted Event_GetArticleCompleted_Handler;

        public event InternalError Event_InternalError_Handler;
        public delegate void InternalError(EInternalError error);

        public event ConnectionStatus Event_ConnectionStatus_Handler;
        public delegate void ConnectionStatus(bool established, SocketError error, ENNTPConnection status);

        #endregion


        public void CallAllNewsgroupsCompleted(NNTPGroupList groups) {
            if (Event_GetAllNewsgroupsCompleted_Handler != null) {
                ThreadPool.QueueUserWorkItem(lambda =>
                {
                    Event_GetAllNewsgroupsCompleted_Handler(groups);
                });
            }
        }

        public void CallNewsgroupAndListArticlesCompleted(NNTPArticleList articles) {
            if (Event_SelectNewsgroupAndListArticlesCompleted_Handler != null) {
                ThreadPool.QueueUserWorkItem(lambda =>
                {
                    Event_SelectNewsgroupAndListArticlesCompleted_Handler(articles);
                });
            }
        }

        public void CallArticleCompleted(NNTPArticle article) {
            if (Event_GetArticleCompleted_Handler != null) {
                ThreadPool.QueueUserWorkItem(lambda =>
                {
                    Event_GetArticleCompleted_Handler(article);
                });
            }
        }


        public void CallConnectionStatus(bool establised, SocketError error, ENNTPConnection status) {
            if (Event_ConnectionStatus_Handler != null) {
                ThreadPool.QueueUserWorkItem(lambda =>
                {
                    Event_ConnectionStatus_Handler(establised, error, status);
                });
            }
        }

        public void CallInternalError(EInternalError error) {
            if (Event_InternalError_Handler != null) {
                ThreadPool.QueueUserWorkItem(lambda =>
                {
                    Event_InternalError_Handler(error);
                });
            }
        }
    }
}
