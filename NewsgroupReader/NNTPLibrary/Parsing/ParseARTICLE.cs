﻿using NNTPLibrary.Models;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Text;
using NNTPLibrary.Common;
using System;
using System.Collections.Generic;

namespace NNTPLibrary.Parsing {
    public class ParseARTICLE : IParse {
        private IFire _FireCommand;

        public ParseARTICLE(IFire fire) {
            _FireCommand = fire;
        }
        public void parse(byte[] data) {
            NNTPArticle article = new NNTPArticle();
            article.Data = data;

            int index = Utils.IndexOf(data, Utils.DefaultEncoding.GetBytes("\r\n\r\n"));
            if (index == -1) { _FireCommand.CallInternalError(EInternalError.ArticleParsingFailed); return; }
            try {
                string Header = new string(Utils.DefaultEncoding.GetChars(Utils.Copy(data, 0, index)));

                //Header
                String[] lines = Header.Split('\n');
                fillHeader(lines, ref article);

                //Body
                byte[] Content = Utils.Copy(data, index, data.Length - index);
                Encoding found_encoding = getEncoding(article.ContentType);
                article.Message = new string(found_encoding.GetChars(Utils.Copy(Content, 4, Content.Length - 9)));


                _FireCommand.CallArticleCompleted(article);
            } catch (Exception ex) {
                Debug.WriteLine(ex.Message);
                _FireCommand.CallInternalError(EInternalError.ArticleParsingFailed);
            }
        }

        private Encoding getEncoding(string ContentType) {
            string encoding = "";
            try {
                int start_index = ContentType.IndexOf("charset=") + 8;
                encoding = ContentType.Substring(start_index, ContentType.Length - start_index);
                int end_index = encoding.IndexOf(";");
                if (end_index != -1) {
                    encoding = encoding.Substring(0, end_index);
                }
                encoding = encoding.Trim('\"');
                return Utils.GetEncoding(encoding);
            } catch {
                Debug.WriteLine("Encoding not supporde by Windows Phone OS: " + encoding + " " + ContentType);
            }
            return Utils.DefaultEncoding;
        }

        private void fillHeader(string[] lines, ref NNTPArticle article) {

            foreach (String line in lines) {
                if (line.IndexOf(' ') == -1 || line.IndexOf(' ') == 0) { continue; }
                string t_line = line.Substring(0, line.IndexOf(' '));

                switch (t_line) {
                    case "220":
                        string tmp = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r');
                        article.ArticleNumber = Convert.ToInt32(tmp.Substring(0, tmp.IndexOf(' ')));
                        break;
                    case "Path:":
                        article.Path = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r');
                        break;
                    case "From:":
                        article.From = parseHeader(line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r'));
                        break;
                    case "Newsgroups:":
                        article.Newsgroup = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r');
                        break;
                    case "Subject:":
                        article.Subject = parseHeader(line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r'));
                        break;
                    case "Date:":
                        //string format = "d MMM yyyy HH:mm:ss zzz";
                        string t_time = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r');
                        t_time = t_time.Replace(" (UTC)", ""); // This isn't a falid datetime, but we try ...
                        article.Date = DateTime.Parse(t_time); // W3CDateTime.Parse(t_time).DateTime;
                        break;
                    case "Organization:":
                        article.Organization = parseHeader(line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r'));
                        break;
                    case "Message-ID:":
                        article.MessageId = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r');
                        break;
                    case "References:":
                        string refs = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r');
                        article.References = refs.Split(' ');
                        break;
                    case "Content-Type:":
                        article.ContentType = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r');
                        break;
                    case "Content-Transfer-Encoding:":
                        article.ContentTransferEncoding = line.Substring(line.IndexOf(' ') + 1, line.Length - (line.IndexOf(' ') + 1)).Trim('\r');
                        break;
                    default:
                        break;
                }
            }
        }

        private string parseHeader(string input) {
            try {
                if (input.IndexOf("=?") == -1) { return input; }

                string encoding = input.Substring(input.IndexOf("=?") + 2, input.IndexOf("?Q?") - input.IndexOf("=?") - 2);
                string content = input.Substring(input.IndexOf("Q?") + 2, input.LastIndexOf("?=") - input.IndexOf("Q?") - 2);

                List<string> byte_str = new List<string>();
                for (int i = 0; i < content.Length; i++) {
                    if (content[i] == '=') {
                        char[] a_byte = new char[] { content[i + 1], content[i + 2] };
                        byte_str.Add(new string(a_byte));
                    }
                }

                byte[] result_array = new byte[byte_str.Count];

                for (int i = 0; i < byte_str.Count; i++) {
                    byte b = byte.Parse(byte_str[i], System.Globalization.NumberStyles.HexNumber);
                    result_array[i] = b;
                }

                string my_result = "";
                try {
                    my_result = new string(Utils.GetEncoding(encoding).GetChars(result_array));
                } catch {
                    my_result = new string(Utils.DefaultEncoding.GetChars(result_array));
                }

                int k = 0;
                string my_restult_str = "";
                for (int i = 0; i < content.Length; i++) {
                    if (content[i] == '=' && k < my_result.Length) {
                        my_restult_str += my_result[k++];
                        i += 2;
                    } else {
                        my_restult_str += content[i];
                    }
                }

                return my_restult_str;
            } catch {
                return input;
            }
        }
    }
}
