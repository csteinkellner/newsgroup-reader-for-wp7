﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NNTPLibrary.Parsing {
    public class ParserPostingProhibited : IParse {
        private IFire _FireCommand;

        public ParserPostingProhibited(IFire fire) {
            _FireCommand = fire;
        }

        public void parse(byte[] data) {
            _FireCommand.CallConnectionStatus(true, System.Net.Sockets.SocketError.Success, ENNTPConnection.PostingProhibited);
        }
    }
}
