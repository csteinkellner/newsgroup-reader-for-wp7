﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using NNTPLibrary.Common;

namespace NNTPLibrary.Parsing {
    public class ParserServerUnavailable : IParse {
        private IFire _FireCommand;

        public ParserServerUnavailable(IFire fire) {
            _FireCommand = fire;
        }

        public void parse(byte[] data) {
            int code = (int)CommandParser.getResponsesCode(data);

            switch (code) {
                case Rfc977ResponseCodes.TEMPORARILY_UNAVAILABLE:
                    _FireCommand.CallConnectionStatus(false, System.Net.Sockets.SocketError.Success, ENNTPConnection.TemporarilyUnavailable);
                    return;
                case Rfc977ResponseCodes.PERMANENTLY_UNAVAILABLE:
                    _FireCommand.CallConnectionStatus(false, System.Net.Sockets.SocketError.Success, ENNTPConnection.PostingProhibited);
                    return;
                default:
                    _FireCommand.CallConnectionStatus(false, System.Net.Sockets.SocketError.Success, ENNTPConnection.Unknown);
                    return;
            }
        }
    }
}
