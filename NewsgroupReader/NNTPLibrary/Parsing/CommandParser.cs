﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Diagnostics;
using NNTPLibrary.Common;
using System.Text;

namespace NNTPLibrary.Parsing {
    public class CommandParser {

        public static byte[] GetCommand(ref byte[] data) {
            int? t_code = getResponsesCode(data);
            if (t_code == null) {
                if (data != null ) {
                    Debug.WriteLine("A command could not be interpreted. Data: " + new string(Encoding.UTF8.GetChars(data)));
                }
                data = null;
                return null;
            }

            int code = (int)t_code;

            if (!NameToCodeRelation.GetInstance().Relation.ContainsKey(code)) {
                Debug.WriteLine("Command: " + code + " not found. Data: "+ new string(Encoding.UTF8.GetChars(data)));
                data = null;
                return null;
            }
            CodeInformation info = null;
            NameToCodeRelation.GetInstance().Relation.TryGetValue(code, out info);
            byte[] command, part;
            extractCommand(info.TerminationSequence, data, out command, out part);

            data = part;
            return command;
        }

        //private static void extractCommand(string TerminationSequence, string data, out string head, out string tail) {

        //    Debug.Assert(TerminationSequence != null && TerminationSequence != "");

        //    int end = data.IndexOf(TerminationSequence);
        //    if (end != -1) {
        //        end += TerminationSequence.Length;
        //        head = data.Substring(0, end);
        //        tail = data.Substring(end, data.Length - end);

        //        data = tail;
        //    } else {
        //        head = null;
        //        tail = data;
        //    }
        //}

        private static void extractCommand(string TerminationSequence, byte[] data, out byte[] head, out byte[] tail) {

            Debug.Assert(TerminationSequence != null && TerminationSequence != "");
            byte[] termination_sequence = Encoding.UTF8.GetBytes(TerminationSequence);

            int end = Utils.IndexOf(data, termination_sequence);

            if (end != -1) {
                end += TerminationSequence.Length;

                head = Utils.Copy(data, 0, end);
                tail = Utils.Copy(data, end, data.Length - end);

                data = tail;
            } else {
                head = null;
                tail = data;
            }
        }

        //public static int? getResponsesCode(string command) {
        //    try {
        //        return Convert.ToInt32(command.Substring(0, 3));
        //    } catch {
        //        return null;
        //    }
        //}

        public static int? getResponsesCode(byte[] command) {
            try {
                return Convert.ToInt32(new string(Encoding.UTF8.GetChars(Utils.Copy(command, 0, 3))));
            } catch {
                return null;
            }
        }
    }
}
