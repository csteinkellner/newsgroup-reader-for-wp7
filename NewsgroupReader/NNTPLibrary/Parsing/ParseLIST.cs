﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using NNTPLibrary.Models;
using NNTPLibrary.Common;

namespace NNTPLibrary.Parsing {
    public class ParseLIST : IParse {
        private IFire _FireCommand;
        public ParseLIST(IFire fire) {
            _FireCommand = fire;
        }
        public void parse(byte[] data) {
            NNTPGroupList obj = new NNTPGroupList() { Data = data };
            //TODO: change to NameToCodeRealteion
            string text = new string(Utils.DefaultEncoding.GetChars(data));
            String[] lines = text.Split('\n');
            int linenumber = 0;
            
            foreach (String line in lines) {
                //Simple checks
                int checksum = 0;
                linenumber++;

                if (linenumber == 0) continue;
                NNTPGroup group = new NNTPGroup();

                String[] parts = line.Replace("\r", "").Split(' ');
                int iterator = 0;
                foreach (String p in parts) {
                    if (iterator == 0) {
                        group.Name = p;
                        checksum |= 1;
                    } else if (iterator == 1) {
                        try {
                            group.NumberOfLastArticle = Convert.ToInt32(p);
                            checksum |= 2;
                        } catch { /*SILENCE*/}
                    } else if (iterator == 2) {
                        try {
                            group.NumberOfFirstArticle = Convert.ToInt32(p);
                            checksum |= 4;
                        } catch { /*SILENCE*/}
                    } else if (iterator == 3) {
                        if (p == "y") {
                            group.Posting = NNTPLibrary.Models.NNTPGroup.EPosting.Allowed;
                            checksum |= 8;
                        } else if (p == "n") {
                            group.Posting = NNTPLibrary.Models.NNTPGroup.EPosting.Prohibited;
                            checksum |= 8;
                        } else if (p == "m") {
                            group.Posting = NNTPLibrary.Models.NNTPGroup.EPosting.Forwarded;
                            checksum |= 8;
                        }
                    }
                    iterator++;
                }
                
                if (checksum == 15) {
                    obj.Groups.Add(group);
                }
            }

            _FireCommand.CallAllNewsgroupsCompleted(obj);

        }
    }
}
