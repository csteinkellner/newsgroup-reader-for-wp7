﻿using System;
using System.Net;
using NNTPLibrary.Models;
using System.Collections.Generic;
using NNTPLibrary.Common;

namespace NNTPLibrary.Parsing {
    public class ParseLISTGROUP : IParse {
        private IFire _FireCommand;
        public ParseLISTGROUP(IFire fire) {
            _FireCommand = fire;
        }
        public void parse(byte[] data) {
            NNTPArticleList obj = new NNTPArticleList() { Data = data };

            string text = new string(Utils.DefaultEncoding.GetChars(data));

            String[] all_lines = text.Split('\n');

            string[] items = all_lines[0].Split(' ');
            obj.Number = Convert.ToInt32(items[1]);
            obj.Low = Convert.ToInt32(items[2]);
            obj.Hight = Convert.ToInt32(items[3]);
            obj.Group = items[4].Trim('\r');

            for(int iterator=1; iterator< all_lines.Length; iterator++){
                string text1 = all_lines[iterator].Trim('\r');
                int num=0;
                if (Int32.TryParse(text1, out num)) {
                    obj.ArticleNumbers.Add(num);
                }
            }

            _FireCommand.CallNewsgroupAndListArticlesCompleted( obj);
        }
    }
}
