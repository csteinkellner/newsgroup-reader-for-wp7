﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NNTPLibrary.Models;

namespace NNTPLibrary.Parsing
{
    public interface IParse
    {
        void parse(byte[] data);
    }
}
