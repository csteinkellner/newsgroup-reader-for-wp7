﻿using System;
using System.Net;
using NNTPLibrary.Models;
using System.Net.Sockets;

namespace NNTPLibrary.Parsing {
    public enum ENNTPConnection { Unknown, PostingAllowed = 200, PostingProhibited = 201, 
                                  TemporarilyUnavailable = 400, PermanentlyUnavailable = 502 };
    public enum EInternalError { Unknown, ArticleParsingFailed, TransferFailed };

    public interface IFire {

        void CallAllNewsgroupsCompleted(NNTPGroupList groups);

        void CallNewsgroupAndListArticlesCompleted(NNTPArticleList articles);

        void CallArticleCompleted(NNTPArticle article);

        void CallConnectionStatus(bool establised, SocketError error, ENNTPConnection status);

        void CallInternalError(EInternalError error);
    }
}
