﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.ComponentModel;

namespace NewsgroupReader.Controls
{
    public partial class LevelVisualizer : UserControl
    {
        private Brush _fill;
        private int _boxWidth;
        private int _maxLevel;

        public static DependencyProperty LevelProperty = DependencyProperty.Register("Level", 
            typeof(int), typeof(LevelVisualizer),
            new PropertyMetadata(0, new PropertyChangedCallback(LevelVisualizer.OnVisualPropertyChanged)));

        public static void OnVisualPropertyChanged(DependencyObject obj,DependencyPropertyChangedEventArgs e)
        {
            ((LevelVisualizer) obj).updateVisualization();
        }


        public int Level
        {
            get
            {
                return (int)GetValue(LevelProperty);
            }
            set
            {
                SetValue(LevelProperty, value);
            }
        }


        public LevelVisualizer()
        {
            InitializeComponent();

            _maxLevel = 4;
            _fill = new SolidColorBrush(Colors.Black);
        }

        public int MaxLevel
        {
            get
            {
                return _maxLevel;
            }

            set
            {
                if (_maxLevel != value)
                {
                    _maxLevel = (value >= 3) ? value : 3;
                    updateVisualization();
                }
            }
        }

        public int BoxWidth
        {
            get
            {
                return _boxWidth;
            }

            set
            {
                if (_boxWidth != value)
                {
                    _boxWidth = value;

                    foreach (Object obj in Container.Children)
                    {
                        if (obj is Rectangle)
                        {
                            Rectangle rect = (obj as Rectangle);
                            if ((Level > _maxLevel) && (rect.Tag == null))
                            {
                                rect.Width = ((_maxLevel - 1) * 2 - 1) * _boxWidth;
                            }
                            else
                            {
                                rect.Width = _boxWidth;
                            }

                            if (rect.Tag != null)
                            {
                                rect.Height = Container.Height;
                            }
                            else
                            {
                                rect.Height = _boxWidth;
                                rect.Margin = new Thickness(0, 0, _boxWidth, 0);
                            }
                        }

                        if (obj is Grid)
                        {
                            foreach (Object o in (obj as Grid).Children)
                            {
                                if (o is Rectangle)
                                {
                                    (o as Rectangle).Width = ((_maxLevel - 1) * 2 - 1) * _boxWidth;
                                    (o as Rectangle).Height = _boxWidth;
                                }
                            }
                            (obj as Grid).Height = Container.Height;
                            (obj as Grid).Width = ((_maxLevel - 1) * 2 - 1) * _boxWidth;
                            (obj as Grid).Margin = new Thickness(0, 0, _boxWidth, 0);
                        }
                    }

                    if (Level > _maxLevel)
                    {
                        this.Width = (_maxLevel * 2 - 1) * _boxWidth;
                    }
                    else
                    {
                        if (Level > 0)
                        {
                            this.Width = (Level * 2 - 1) * _boxWidth;
                        }
                        else
                        {
                            this.Width = 0;
                        }
                    }
                }
            }
        }

        //public int Level
        //{
        //    get
        //    {
        //        return _level;
        //    }

        //    set
        //    {
        //        if (_level != value)
        //        {
        //            _level = value;
        //            updateVisualization();
        //        }
        //    }
        //}

        public Brush Fill
        {
            get
            {
                return _fill;
            }

            set
            {
                if (_fill != value)
                {
                    _fill = value;

                    foreach (Object obj in Container.Children)
                    {
                        if (obj is Rectangle)
                        {
                            (obj as Rectangle).Fill = _fill;
                        }

                        if (obj is Grid)
                        {
                            foreach (Object o in (obj as Grid).Children)
                            {
                                if (o is Rectangle)
                                {
                                    (o as Rectangle).Fill = _fill;
                                }

                                if (o is TextBlock)
                                {
                                    (o as TextBlock).Foreground = _fill;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void updateVisualization()
        {
            Container.Children.Clear();

            if (Level > 0)
            {
                if (Level > _maxLevel)
                {
                    Grid cont = new Grid();
                    Rectangle rect = new Rectangle();
                    cont.Width = _boxWidth * ((_maxLevel - 1) * 2 - 1) * _boxWidth;
                    cont.Height = Container.Height;
                    cont.Margin = new Thickness(0, 0, _boxWidth, 0);
                    rect.Width = _boxWidth * ((_maxLevel - 1) * 2 - 1) * _boxWidth;
                    rect.Height = _boxWidth;
                    rect.Fill = _fill;
                    cont.Children.Add(rect);
                    Container.Children.Add(cont);

                    rect = new Rectangle();
                    rect.Width = _boxWidth;
                    rect.Height = Container.Height;
                    rect.Tag = 1;
                    rect.Fill = _fill;

                    Container.Children.Add(rect);

                    this.Width = (_maxLevel * 2 - 1) * _boxWidth;

                    // Number
                    TextBlock label = new TextBlock();
                    label.Foreground = _fill;
                    label.FontWeight = FontWeights.Bold;
                    label.TextAlignment = TextAlignment.Center;
                    label.Text = Level.ToString();
                    cont.Children.Add(label);
                }
                else
                {
                    Rectangle rect;

                    for (int iterator = 1; iterator < Level; iterator++)
                    {
                        rect = new Rectangle();
                        rect.Width = _boxWidth;
                        rect.Height = _boxWidth;
                        rect.Margin = new Thickness(0, 0, _boxWidth, 0);
                        rect.Fill = _fill;
                        Container.Children.Add(rect);
                    }

                    rect = new Rectangle();
                    rect.Width = _boxWidth;
                    rect.Height = Container.Height;
                    rect.Tag = 1;
                    rect.Fill = _fill;

                    Container.Children.Add(rect);

                    if (_boxWidth <= 0)
                    {
                        this.Width = 0;
                    }
                    else
                    {
                        this.Width = (Level * 2 - 1) * _boxWidth;
                    }
                }
            }
        }
    }
}
