﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using NewsgroupReader.Resources;

namespace NewsgroupReader.Classes
{
    public class HTMLRenderer
    {
        private static List<Color> getSortedColorList(Color accentColor)
        {
            Color[] accentColorArray = { Color.FromArgb(255, 27, 161, 226),
                                   Color.FromArgb(255, 160, 80, 0),
                                   Color.FromArgb(255, 51, 153, 51),
                                   Color.FromArgb(255, 162, 193, 57),
                                   Color.FromArgb(255, 216, 0, 115),
                                   Color.FromArgb(255, 240, 150, 9),
                                   Color.FromArgb(255, 230, 113, 184),
                                   Color.FromArgb(255, 162, 0, 255),
                                   Color.FromArgb(255, 229, 20, 0),
                                   Color.FromArgb(255, 0, 171, 169)};

            List<Color> accentColors = new List<Color>(accentColorArray);

            foreach (Color color in accentColors)
            {
                if (color == accentColor)
                {
                    accentColors.Remove(color);
                    accentColors.Insert(0, color);
                    break;
                }
            }
            return accentColors;
        }

        private static String getColorListAsCSS(Color accentColor)
        {
            List<Color> list = getSortedColorList(accentColor);

            StringBuilder result = new StringBuilder();

            for (int iterator = 0; iterator < list.Count; iterator++)
            {
                result.Append("\n.level_" + iterator.ToString() + " {\n");
                result.Append("  background-color: rgba(" + list[iterator].R.ToString() + ", " + list[iterator].G.ToString() + ", " + list[iterator].B.ToString() + ", 0.1);\n");
                result.Append("  border-color: rgb(" + list[iterator].R.ToString() + ", " + list[iterator].G.ToString() + ", " + list[iterator].B.ToString() + ");\n");
                result.Append("}\n");
            }

            return result.ToString();
        }

        private static String renderCitationsAndSignature(String post)
        {
            String[] lines = post.Split('\n');
            StringBuilder result = new StringBuilder();
            bool foundSignature = false;

            int citationLevel = 0;
            for (int iterator = 0; iterator < lines.Length; iterator++)
            {
                if ((foundSignature) || (lines[iterator] == "-- "))
                {
                    if (foundSignature)
                    {
                        result.Append(lines[iterator]);
                    }
                    else
                    {
                        foundSignature = true;
                        result.Append("<div class=\"signature\">");
                    }
                }
                else
                {
                    int currentCitationLevel = 0;
                    int length = 0;
                    while ((lines[iterator].Length > length) && (lines[iterator][length] == '>'))
                    {
                        currentCitationLevel++;
                        if (currentCitationLevel > citationLevel)
                        {
                            citationLevel = currentCitationLevel;
                            result.Append("<div class=\"citation level_" + (citationLevel - 1) % 9 + "\">");
                            length += 30; // Length of <div class...
                        }
                        length++; // ">"
                    }
                    if (currentCitationLevel < citationLevel)
                    {
                        for (int counter = 0; counter < (citationLevel - currentCitationLevel); counter++)
                        {
                            result.Append("</div>");
                        }
                        citationLevel = currentCitationLevel;
                    }
                    result.Append(lines[iterator].Substring(currentCitationLevel));
                    result.Append("<br />");
                }
            }
            if (foundSignature)
            {
                // Close Signature
                result.Append("</div>");
            }
            return result.ToString();
        }

        private static String renderBoldText(String post)
        {
            StringBuilder output = new StringBuilder();
            StringBuilder bold = new StringBuilder();
            bool boldmode = false;
            for (int iterator = 0; iterator < post.Length; iterator++)
            {
                if (boldmode)
                {
                    if (post[iterator] == '*')
                    {
                        output.Append("<span class=\"bold\">" + bold.ToString() + "</span>");
                        bold.Clear();
                        boldmode = false;
                    }
                    else
                    {
                        bold.Append(post[iterator]);
                    }
                }
                else
                {
                    if (post[iterator] == '*')
                    {
                        boldmode = true;
                    }
                    else
                    {
                        output.Append(post[iterator]);
                    }
                }
            }

            if (bold.Length > 0)
            {
                output.Append("*" + bold.ToString());
            }

            return output.ToString();
        }

        private static String renderItalicText(String post)
        {
            StringBuilder output = new StringBuilder();
            StringBuilder italic = new StringBuilder();
            bool boldmode = false;
            for (int iterator = 0; iterator < post.Length; iterator++)
            {
                if (boldmode)
                {
                    if (post[iterator] == '/')
                    {
                        output.Append("<span class=\"italic\">" + italic.ToString() + "</span>");
                        italic.Clear();
                        boldmode = false;
                    }
                    else
                    {
                        italic.Append(post[iterator]);
                    }
                }
                else
                {
                    if (post[iterator] == '/')
                    {
                        boldmode = true;
                    }
                    else
                    {
                        output.Append(post[iterator]);
                    }
                }
            }

            if (italic.Length > 0)
            {
                output.Append("/" + italic.ToString());
            }

            return output.ToString();
        }

        private static String renderUnderlineText(String post)
        {
            StringBuilder output = new StringBuilder();
            StringBuilder underline = new StringBuilder();
            bool boldmode = false;
            for (int iterator = 0; iterator < post.Length; iterator++)
            {
                if (boldmode)
                {
                    if (post[iterator] == '_')
                    {
                        output.Append("<span class=\"underline\">" + underline.ToString() + "</span>");
                        underline.Clear();
                        boldmode = false;
                    }
                    else
                    {
                        underline.Append(post[iterator]);
                    }
                }
                else
                {
                    if (post[iterator] == '_')
                    {
                        boldmode = true;
                    }
                    else
                    {
                        output.Append(post[iterator]);
                    }
                }
            }

            if (underline.Length > 0)
            {
                output.Append("_" + underline.ToString());
            }

            return output.ToString();
        }

        private static String renderSmileys(String post, bool lightTheme)
        {
            String result = post;

            if (lightTheme)
            {
                result = result.Replace(":O", HTMLResources.SmileyAstonishedDark);
                result = result.Replace(":)", HTMLResources.SmileySmileDark);
                result = result.Replace(":(", HTMLResources.SmileySadDark);
                result = result.Replace("o_O", HTMLResources.SmileyWthDark);
                result = result.Replace(":P", HTMLResources.SmileyTongueDark);
                result = result.Replace(":3", HTMLResources.SmileyHappyDark);
                result = result.Replace(":D", HTMLResources.SmileyBigSmileDark);
                result = result.Replace("xD", HTMLResources.SmileyLOLDark);
                result = result.Replace(";)", HTMLResources.SmileyWinkDark);

                result = result.Replace(":-O", HTMLResources.SmileyAstonishedDark);
                result = result.Replace(":-)", HTMLResources.SmileySmileDark);
                result = result.Replace(":-(", HTMLResources.SmileySadDark);
                result = result.Replace(":-P", HTMLResources.SmileyTongueDark);
                result = result.Replace(":-3", HTMLResources.SmileyHappyDark);
                result = result.Replace(":-D", HTMLResources.SmileyBigSmileDark);
                result = result.Replace("XD", HTMLResources.SmileyLOLDark);
                result = result.Replace(";-)", HTMLResources.SmileyWinkDark);
            }
            else
            {
                result = result.Replace(":O", HTMLResources.SmileyAstonishedLight);
                result = result.Replace(":)", HTMLResources.SmileySmileLight);
                result = result.Replace(":(", HTMLResources.SmileySadLight);
                result = result.Replace("o_O", HTMLResources.SmileyWthLight);
                result = result.Replace(":P", HTMLResources.SmileyTongueLight);
                result = result.Replace(":3", HTMLResources.SmileyHappyLight);
                result = result.Replace(":D", HTMLResources.SmileyBigSmileLight);
                result = result.Replace("xD", HTMLResources.SmileyLOLLight);
                result = result.Replace(";)", HTMLResources.SmileyWinkLight);

                result = result.Replace(":-O", HTMLResources.SmileyAstonishedLight);
                result = result.Replace(":-)", HTMLResources.SmileySmileLight);
                result = result.Replace(":-(", HTMLResources.SmileySadLight);
                result = result.Replace(":-P", HTMLResources.SmileyTongueLight);
                result = result.Replace(":-3", HTMLResources.SmileyHappyLight);
                result = result.Replace(":-D", HTMLResources.SmileyBigSmileLight);
                result = result.Replace("XD", HTMLResources.SmileyLOLLight);
                result = result.Replace(";-)", HTMLResources.SmileyWinkLight);
            }

            return result;
        }

        private static String renderContent(String post, bool lightTheme)
        {
            String result = renderItalicText(post);
            result = renderUnderlineText(result);
            result = renderBoldText(result);
            result = renderCitationsAndSignature(result);
            result = renderSmileys(result, lightTheme);
            return result;
        }

        public static String renderPostToHTML(String post, bool lightTheme, Color accentColor)
        {
            String result;
            if (lightTheme)
            {
                result = HTMLResources.SiteSkeleton.Replace("%TEXTCOLOR%", "rgb(0, 0, 0)");
                result = result.Replace("%BACKGROUNDCOLOR%", "rgb(255, 255, 255)");
                result = result.Replace("%SIGNATURECOLOR%", "rgb(242, 242, 242)");
            }
            else
            {
                result = HTMLResources.SiteSkeleton.Replace("%TEXTCOLOR%", "rgb(255, 255, 255)");
                result = result.Replace("%BACKGROUNDCOLOR%", "rgb(0, 0, 0)");
                result = result.Replace("%SIGNATURECOLOR%", "rgb(13, 13, 13)");
            }

            result = result.Replace("%CITATIONCOLORS%", getColorListAsCSS(accentColor));

            result = result.Replace("%CONTENT%", renderContent(post, lightTheme));

            return result;
        }
    }
}