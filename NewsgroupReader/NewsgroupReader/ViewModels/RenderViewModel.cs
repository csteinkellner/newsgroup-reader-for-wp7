﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using NewsgroupReader.Models;
using System.Windows.Data;
using GalaSoft.MvvmLight.Command;
using System.Windows.Controls;
using System.ComponentModel;
using NNTPLibrary;
using NNTPLibrary.Models;
using NewsgroupReader.Helpers;
using System.Windows.Threading;
using System.Windows;
using System.Collections.Generic;
using NewsgroupReader.Classes;
using System.Windows.Media;
using System.Text;

namespace NewsgroupReader.ViewModels
{
    public class RenderViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public new event PropertyChangedEventHandler PropertyChanged;
        public readonly INavigationService _navigationService;

        private String _articleSubject = "";
        private String _articleBody = "";
        private String _articleAuthor = "";
        private String _articleAuthorEmail = "";
        private String _articleGroup = "";
        private String _articleDate = "";

        public RelayCommand LoadedCommand
        {
            get;
            private set;
        }

        public String ArticleSubject
        {
            get
            {
                return _articleSubject;
            }
            set
            {
                if (_articleSubject != value)
                {
                    _articleSubject = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ArticleSubject"));
                    }

                }
            }
        }

        public String ArticleBody
        {
            get
            {
                return _articleBody;
            }
            set
            {
                if (_articleBody != value)
                {
                    _articleBody = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ArticleBody"));
                    }

                }
            }
        }

        public String ArticleAuthor
        {
            get
            {
                return _articleAuthor;
            }
            set
            {
                if (_articleAuthor != value)
                {
                    _articleAuthor = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ArticleAuthor"));
                    }

                }
            }
        }

        public String ArticleAuthorEmail
        {
            get
            {
                return _articleAuthorEmail;
            }
            set
            {
                if (_articleAuthorEmail != value)
                {
                    _articleAuthorEmail = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ArticleAuthorEmail"));
                    }

                }
            }
        }

        public String ArticleGroup
        {
            get
            {
                return _articleGroup;
            }
            set
            {
                if (_articleGroup != value)
                {
                    _articleGroup = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ArticleGroup"));
                    }

                }
            }
        }

        public String ArticleDate
        {
            get
            {
                return _articleDate;
            }
            set
            {
                if (_articleDate != value)
                {
                    _articleDate = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ArticleDate"));
                    }

                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public RenderViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            LoadedCommand = new RelayCommand(Loaded);
        }

        private void Loaded()
        {
            ViewModelLocator locator = ((ViewModelLocator)App.Current.Resources["Locator"]);
            if (locator != null)
            {
                ArticleItem articleItem = locator.Articles.SelectedItem;
                if (articleItem != null)
                {
                    String[] author = articleItem.From.Split(' ');
                    if (author.Length > 1)
                    {
                        StringBuilder builder = new StringBuilder();
                        ArticleAuthorEmail = author[author.Length - 1];
                        for (int iterator = 0; iterator < (author.Length - 1); iterator++)
                        {
                            builder.Append(author[iterator] + " ");
                        }
                        ArticleAuthor = builder.ToString();
                    }
                    else
                    {
                        ArticleAuthor = articleItem.From;
                    }
                    ArticleSubject = articleItem.Subject;
                    ArticleGroup = articleItem.Newsgroup;
                    ArticleDate = articleItem.Date.ToLocalTime().ToLongTimeString();
                    ArticleBody = HTMLRenderer.renderPostToHTML(articleItem.Message, 
                        ((Visibility)Application.Current.Resources["PhoneDarkThemeVisibility"] != Visibility.Visible), 
                        (Color)Application.Current.Resources["PhoneAccentColor"]);
                }
            }
        }
    }
}