using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using NewsgroupReader.Models;
using GalaSoft.MvvmLight.Command;
using NewsgroupReader.Helpers;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using NewsgroupReader.Resources;
using System.Windows;

namespace NewsgroupReader.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class RemoveServersViewModel : ViewModelBase
    {
        private ObservableCollection<ServerItem> _items = new ObservableCollection<ServerItem>();
        public readonly INavigationService _navigationService;

        private ServerItem _serverItem;

        public ObservableCollection<ServerItem> Items
        {
            get
            {
                return _items;
            }
        }

        public RelayCommand LoadedCommand
        {
            get;
            private set;
        }

        public RelayCommand<ServerItem> MenuItemClickedCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public RemoveServersViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            MenuItemClickedCommand = new RelayCommand<ServerItem>(MenuItemClicked);
            LoadedCommand = new RelayCommand(Loaded);
        }

        private void Loaded()
        {
            _items.Clear();
            List<ServerItem> items = DataProvider.DataProvider.GetAllServers();
            for (int iterator = 0; iterator < items.Count; iterator++)
            {
                _items.Add(items[iterator]);
            }
        }

        private void DialogMessageCallback(MessageBoxResult result)
        {
            if (result == MessageBoxResult.OK)
            {
                _items.Remove(_serverItem);
                DataProvider.DataProvider.DeleteServer(_serverItem);
            }
        }

        private void MenuItemClicked(ServerItem item)
        {
            // A server has been chosen.
            if (item != null)
            {
                _serverItem = item;

                var message = new DialogMessage(String.Format(AppResources.RemoveServerMessage, item.Name), DialogMessageCallback)
                {
                    Button = MessageBoxButton.OKCancel,
                    Caption = AppResources.WarningBoxTitle
                };

                Messenger.Default.Send(message);
            }
        }

    }
}