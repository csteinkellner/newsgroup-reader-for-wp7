﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight;
using System.ComponentModel;
using NewsgroupReader.Helpers;
using System.Collections.ObjectModel;
using System.Windows.Data;
using NewsgroupReader.Models;
using NNTPLibrary.Models;
using GalaSoft.MvvmLight.Command;
using System.Collections.Generic;

namespace NewsgroupReader.ViewModels
{
    public class ThreadsViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public new event PropertyChangedEventHandler PropertyChanged;
        public readonly INavigationService _navigationService;

        private ObservableCollection<ArticleItem> _items = new ObservableCollection<ArticleItem>();
        private List<ArticleItem> _allItems = new List<ArticleItem>();
        private CollectionViewSource _filter = new CollectionViewSource();
        private String _filterText = "";

        public ArticleItem SelectedItem { get; set; }

        ServerItem _serverItem;
        GroupItem _groupItem;

        public RelayCommand<ArticleItem> MenuItemClickedCommand
        {
            get;
            private set;
        }

        public List<ArticleItem> FetchedArticles
        {
            get
            {
                return _allItems;
            }
        }

        public RelayCommand LoadedCommand
        {
            get;
            private set;
        }

        public ICollectionView Items
        {
            get
            {
                return _filter.View;
            }
        }

        public String FilterText
        {
            get
            {
                return _filterText;
            }
            set
            {
                if (_filterText != value)
                {
                    _filterText = value;
                    _filter.View.Refresh();

                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FilterText"));
                    }

                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public ThreadsViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            MenuItemClickedCommand = new RelayCommand<ArticleItem>(MenuItemClicked);
            LoadedCommand = new RelayCommand(Loaded);

            DataProvider.DataProvider.Event_GetAllArticlesEvent_Handler +=
                        new DataProvider.DataProvider.GetAllArticlesEvent(GetAllArticlesEvent);

            _filter.Filter += Filter_Method;
            _filter.Source = _items;
            _filter.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Ascending));
        }

        private void MenuItemClicked(ArticleItem item)
        {
            // A thread has been chosen.
            if (item != null)
            {
                SelectedItem = item;
                _navigationService.NavigateTo(new Uri(ViewModelLocator.ArticlesUrl, UriKind.Relative));
            }
        }

        void Filter_Method(object sender, FilterEventArgs e)
        {
            if (e.Item != null)
                e.Accepted = ( ((ArticleItem)e.Item).Subject.Contains(_filterText) ||
                    ((ArticleItem)e.Item).From.Contains(_filterText) );
        }

        public void GetAllArticlesEvent(ArticleItem article)
        {
            // Keep in memory
            _allItems.Add(article);

            if ((article.References == null) || (article.References.Length == 0) || ((article.References.Length == 1) && (article.References[0].Equals(""))))
            {
                _items.Add(article);
            }
        }

        private void Loaded()
        {
            ViewModelLocator locator = ((ViewModelLocator)App.Current.Resources["Locator"]);
            if (locator != null)
            {
                _serverItem = locator.Main.SelectedItem;
                _groupItem = locator.Groups.SelectedItem;

                if ((_groupItem != null) && (_serverItem != null))
                {
                    _items.Clear();
                    _allItems.Clear();
                    // Fetch from DB
                    List<ArticleItem> db = DataProvider.DataProvider.getInstance(
                        _serverItem.Url).GetAllArticles(_groupItem.Description);
                    foreach (ArticleItem item in db)
                    {
                        _allItems.Add(item);
                        if ((item.References == null) || (item.References.Length == 0) || ((item.References.Length == 1) && (item.References[0].Equals(""))))
                        {
                            _items.Add(item);
                        }
                    }
                }
            }
        }
    }
}
