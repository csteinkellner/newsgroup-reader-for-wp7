﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using NewsgroupReader.Models;
using GalaSoft.MvvmLight.Command;
using NewsgroupReader.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using NewsgroupReader.Resources;

namespace NewsgroupReader.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class NewServerViewModel : ViewModelBase
    {
        public readonly INavigationService _navigationService;

        public ServerItem ServerItem { get; set; } 

        /// <summary>
        /// Initializes a new instance of the NewServerViewModel class.
        /// </summary>
        public NewServerViewModel(INavigationService navigationService)
        {
            ServerItem = new ServerItem("", "", "", "", "", 0);
            _navigationService = navigationService;
            OKCommand = new RelayCommand(OKButtonClicked);
            CancelCommand = new RelayCommand(CancelButtonClicked);
        }

        public RelayCommand OKCommand
        {
            get;
            private set;
        }

        public RelayCommand CancelCommand
        {
            get;
            private set;
        }

        public void CancelButtonClicked()
        {
            _navigationService.NavigateTo(new Uri(ViewModelLocator.MainUrl, UriKind.Relative));
        }

        public void OKButtonClicked()
        {
            if ((ServerItem.Name == "") || (ServerItem.Url == ""))
            {
                var message = new DialogMessage(AppResources.RequiredFieldsMessage, null)
                {
                    Button = MessageBoxButton.OK,
                    Caption = AppResources.InformationBoxTitle
                };

                Messenger.Default.Send(message);
            }
            else
            {
                DataProvider.DataProvider.StoreServer(ServerItem);
                _navigationService.NavigateTo(new Uri(ViewModelLocator.ChooseGroupsUrl, UriKind.Relative));
            }
        }
    }
}