﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using NewsgroupReader.Models;
using System.Windows.Data;
using GalaSoft.MvvmLight.Command;
using System.Windows.Controls;
using System.ComponentModel;
using NNTPLibrary;
using NNTPLibrary.Models;
using NewsgroupReader.Helpers;
using System.Windows.Threading;
using System.Windows;
using System.Collections.Generic;

namespace NewsgroupReader.ViewModels
{
    public class ArticlesViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public new event PropertyChangedEventHandler PropertyChanged;
        public readonly INavigationService _navigationService;

        private ObservableCollection<ArticleItem> _items = new ObservableCollection<ArticleItem>();
        private CollectionViewSource _filter = new CollectionViewSource();
        private String _filterText = "";

        public ArticleItem SelectedItem { get; set; }
        ServerItem _serverItem;
        GroupItem _groupItem;
        ArticleItem _threadItem;
        List<ArticleItem> _allItems;

        public RelayCommand LoadedCommand
        {
            get;
            private set;
        }

        public RelayCommand<ArticleItem> MenuItemClickedCommand
        {
            get;
            private set;
        }

        public ICollectionView Items
        {
            get
            {
                return _filter.View;
            }
        }

        public String FilterText
        {
            get
            {
                return _filterText;
            }
            set
            {
                if (_filterText != value)
                {
                    _filterText = value;
                    _filter.View.Refresh();

                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FilterText"));
                    }

                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public ArticlesViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            MenuItemClickedCommand = new RelayCommand<ArticleItem>(MenuItemClicked);
            LoadedCommand = new RelayCommand(Loaded);

            _filter.Filter += Filter_Method;
            _filter.Source = _items;
            _filter.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
        }

        private void MenuItemClicked(ArticleItem item)
        {
            // An article has been chosen.
            if (item != null)
            {
                SelectedItem = item;
                _navigationService.NavigateTo(new Uri(ViewModelLocator.RenderUrl, UriKind.Relative));
            }
        }

        void Filter_Method(object sender, FilterEventArgs e)
        {
            if (e.Item != null)
                e.Accepted = (((ArticleItem)e.Item).Subject.Contains(_filterText) ||
                    ((ArticleItem)e.Item).From.Contains(_filterText));
        }

        private void Loaded()
        {
            ViewModelLocator locator = ((ViewModelLocator)App.Current.Resources["Locator"]);
            if (locator != null)
            {
                _serverItem = locator.Main.SelectedItem;
                _groupItem = locator.Groups.SelectedItem;
                _threadItem = locator.Threads.SelectedItem;
                _allItems = locator.Threads.FetchedArticles;

                if ((_threadItem != null) && (_groupItem != null) && (_serverItem != null) && (_allItems != null))
                {
                    _items.Clear();
                    _items.Add(_threadItem);
                    foreach (ArticleItem item in _allItems)
                    {
                        if ((item != _threadItem) && ((item.References != null) && (item.References.Length > 0) && (item.References[0] == _threadItem.MessageId)))
                        {
                            _items.Add(item);
                        }
                    }
                }
            }
        }
    }
}