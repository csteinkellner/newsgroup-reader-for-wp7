﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using NewsgroupReader.Models;
using System.Windows.Data;
using GalaSoft.MvvmLight.Command;
using System.Windows.Controls;
using System.ComponentModel;
using NNTPLibrary;
using NNTPLibrary.Models;
using NewsgroupReader.Helpers;
using System.Windows.Threading;
using System.Windows;
using System.Collections.Generic;

namespace NewsgroupReader.ViewModels
{
    public class GroupsViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public new event PropertyChangedEventHandler PropertyChanged;
        public readonly INavigationService _navigationService;

        private ObservableCollection<GroupItem> _items = new ObservableCollection<GroupItem>();
        private CollectionViewSource _filter = new CollectionViewSource();
        private String _filterText = "";

        public GroupItem SelectedItem { get; set; }

        public RelayCommand<GroupItem> MenuItemClickedCommand
        {
            get;
            private set;
        }

        public RelayCommand UpdateGroupsCommand
        {
            get;
            private set;
        }

        public RelayCommand LoadedCommand
        {
            get;
            private set;
        }

        public ICollectionView Items
        {
            get
            {
                return _filter.View;
            }
        }

        public String FilterText
        {
            get
            {
                return _filterText;
            }
            set
            {
                if (_filterText != value)
                {
                    _filterText = value;
                    _filter.View.Refresh();

                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FilterText"));
                    }

                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public GroupsViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            MenuItemClickedCommand = new RelayCommand<GroupItem>(MenuItemClicked);
            LoadedCommand = new RelayCommand(Loaded);
            UpdateGroupsCommand = new RelayCommand(UpdateGroups);

            _filter.Filter += Filter_Method;
            _filter.Source = _items;
            _filter.SortDescriptions.Add(new SortDescription("Description", ListSortDirection.Ascending));
        }

        private void MenuItemClicked(GroupItem item)
        {
            // A group has been chosen.
            if (item != null)
            {
                SelectedItem = item;
                _navigationService.NavigateTo(new Uri(ViewModelLocator.ThreadsUrl, UriKind.Relative));
            }
        }

        private void UpdateGroups()
        {
            _navigationService.NavigateTo(new Uri(ViewModelLocator.UpdateGroupsUrl, UriKind.Relative));
        }

        void Filter_Method(object sender, FilterEventArgs e)
        {
            if (e.Item != null)
                e.Accepted = ((GroupItem)e.Item).Name.Contains(_filterText);
        }

        private void Loaded()
        {
            ViewModelLocator locator = ((ViewModelLocator)App.Current.Resources["Locator"]);
            if (locator != null)
            {
                ServerItem serverItem = locator.Main.SelectedItem;

                if (serverItem != null)
                {
                    // Fetch Groups
                    _items.Clear();

                    for (int iterator = 0; iterator < serverItem.SubscribedGroups.Count; iterator++)
                    {
                        _items.Add(serverItem.SubscribedGroups[iterator]);
                    }
                }
            }
        }
    }
}