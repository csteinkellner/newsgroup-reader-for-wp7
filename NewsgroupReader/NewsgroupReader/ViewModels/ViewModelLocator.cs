/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:NewsgroupReader"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using NewsgroupReader.Helpers;
using GalaSoft.MvvmLight.Ioc;

namespace NewsgroupReader.ViewModels
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        public const string RenderUrl = @"/Views/RenderView.xaml";
        public const string NewServerUrl = @"/Views/NewServerView.xaml";
        public const string MainUrl = @"/Views/MainView.xaml";
        public const string ChooseGroupsUrl = @"/Views/ChooseGroupsView.xaml";
        public const string GroupsUrl = @"/Views/GroupsView.xaml";
        public const string ThreadsUrl = @"/Views/ThreadsView.xaml";
        public const string ArticlesUrl = @"/Views/ArticlesView.xaml";
        public const string AboutUrl = @"/Views/AboutView.xaml";
        public const string RemoveServersUrl = @"/Views/RemoveServersView.xaml";
        public const string UpdateGroupsUrl = @"/Views/UpdateGroupsView.xaml";

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time services and viewmodels
            ////}
            ////else
            ////{
            ////    // Create run time services and view models
            ////}

            SimpleIoc.Default.Register<INavigationService, NavigationService>();
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<ChooseGroupsViewModel>();
            SimpleIoc.Default.Register<NewServerViewModel>();
            SimpleIoc.Default.Register<GroupsViewModel>();
            SimpleIoc.Default.Register<ThreadsViewModel>();
            SimpleIoc.Default.Register<ArticlesViewModel>();
            SimpleIoc.Default.Register<RenderViewModel>();
            SimpleIoc.Default.Register<RemoveServersViewModel>();
            SimpleIoc.Default.Register<UpdateGroupsViewModel>();
        }

        /// <summary>
        /// Gets the Main property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MainViewModel>();
            }
        }

        /// <summary>
        /// Gets the ChooseGroups property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ChooseGroupsViewModel ChooseGroups
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ChooseGroupsViewModel>();
            }
        }

        /// <summary>
        /// Gets the NewServer property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public NewServerViewModel NewServer
        {
            get
            {
                return SimpleIoc.Default.GetInstance<NewServerViewModel>();
            }
        }

        /// <summary>
        /// Gets the NewServer property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public GroupsViewModel Groups
        {
            get
            {
                return SimpleIoc.Default.GetInstance<GroupsViewModel>();
            }
        }

        /// <summary>
        /// Gets the NewServer property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ThreadsViewModel Threads
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ThreadsViewModel>();
            }
        }

        /// <summary>
        /// Gets the NewServer property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ArticlesViewModel Articles
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ArticlesViewModel>();
            }
        }

        /// <summary>
        /// Gets the NewServer property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public RenderViewModel Render
        {
            get
            {
                return SimpleIoc.Default.GetInstance<RenderViewModel>();
            }
        }

        /// <summary>
        /// Gets the NewServer property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public RemoveServersViewModel RemoveServers
        {
            get
            {
                return SimpleIoc.Default.GetInstance<RemoveServersViewModel>();
            }
        }

        /// <summary>
        /// Gets the NewServer property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public UpdateGroupsViewModel UpdateGroups
        {
            get
            {
                return SimpleIoc.Default.GetInstance<UpdateGroupsViewModel>();
            }
        }
    }
}