﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using NewsgroupReader.Models;
using System.Windows.Data;
using GalaSoft.MvvmLight.Command;
using System.Windows.Controls;
using System.ComponentModel;
using NNTPLibrary;
using NNTPLibrary.Models;
using NewsgroupReader.Helpers;
using System.Windows.Threading;
using System.Windows;
using System.Collections.Generic;

namespace NewsgroupReader.ViewModels
{
    public class ChooseGroupsViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public new event PropertyChangedEventHandler PropertyChanged;
        public readonly INavigationService _navigationService;

        private ObservableCollection<GroupItem> _items = new ObservableCollection<GroupItem>();
        private CollectionViewSource _filter = new CollectionViewSource();
        private String _filterText = "";

        private ServerItem _serverItem;

        public ICollectionView Items
        {
            get
            {
                return _filter.View;
            }
        }

        public RelayCommand LoadedCommand
        {
            get;
            private set;
        }

        public String FilterText
        {
            get
            {
                return _filterText;
            }
            set
            {
                if (_filterText != value)
                {
                    _filterText = value;
                    _filter.View.Refresh();

                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FilterText"));
                    }

                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public ChooseGroupsViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            OKCommand = new RelayCommand(OKButtonClicked);
            CancelCommand = new RelayCommand(CancelButtonClicked);
            LoadedCommand = new RelayCommand(Loaded);

            DataProvider.DataProvider.Event_GetAllNewsgroupsEvent_Handler += new DataProvider.DataProvider.GetAllNewsgroupsEvent(
                delegate(List<GroupItem> groups)
                {
                    foreach (GroupItem item in groups)
                    {
                        _items.Add(item);
                    }
                });

            _filter.Filter += Filter_Method;
            _filter.Source = _items;
            _filter.SortDescriptions.Add(new SortDescription("Description", ListSortDirection.Ascending));
        }

        void Filter_Method(object sender, FilterEventArgs e)
        {
            if (e.Item != null)
                e.Accepted = ((GroupItem)e.Item).Name.Contains(_filterText);
        }

        public RelayCommand OKCommand
        {
            get;
            private set;
        }

        public RelayCommand CancelCommand
        {
            get;
            private set;
        }        

        public void CancelButtonClicked()
        {
            // Navigate back to Main-Page
            _navigationService.NavigateTo(new Uri(ViewModelLocator.MainUrl, UriKind.Relative));
        }

        public void OKButtonClicked()
        {
            // Get Groups
            foreach (GroupItem item in _items)
            {
                if (item.Chosen)
                {
                    _serverItem.SubscribedGroups.Add(item);
                }
            }

            // Server has already been added at this point, so just update the groups
            DataProvider.DataProvider.getInstance(_serverItem.Url).UpdateGroups(_serverItem);
            // Reset ServerItem
            ViewModelLocator locator = ((ViewModelLocator)App.Current.Resources["Locator"]);
            if (locator != null)
            {
                locator.NewServer.ServerItem = new ServerItem("", "", "", "", "", 0);
            }
            // Navigate back to Server-Page
            _navigationService.NavigateTo(new Uri(ViewModelLocator.MainUrl, UriKind.Relative));
        }

        private void Loaded()
        {
            ViewModelLocator locator = ((ViewModelLocator)App.Current.Resources["Locator"]);
            if (locator != null)
            {
                _serverItem = locator.NewServer.ServerItem;
                if (_serverItem != null)
                {
                    // Fetch Groups
                    _items.Clear();

                    DataProvider.DataProvider.getInstance(_serverItem.Url).GetAllNewsgroups();
                }
            }
        }
    }
}