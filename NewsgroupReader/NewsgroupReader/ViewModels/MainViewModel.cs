using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using NewsgroupReader.Models;
using GalaSoft.MvvmLight.Command;
using NewsgroupReader.Helpers;
using System.Collections.Generic;

namespace NewsgroupReader.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<ServerItem> _items = new ObservableCollection<ServerItem>();
        public readonly INavigationService _navigationService;

        public ObservableCollection<ServerItem> Items
        {
            get
            {
                return _items;
            }
        }

        public RelayCommand AddServerCommand
        {
            get;
            private set;
        }

        public RelayCommand RemoveServersCommand
        {
            get;
            private set;
        }

        public RelayCommand AboutCommand
        {
            get;
            private set;
        }

        public RelayCommand<ServerItem> MenuItemClickedCommand
        {
            get;
            private set;
        }

        public RelayCommand LoadedCommand
        {
            get;
            private set;
        }

        public ServerItem SelectedItem { get; set; }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            LoadedCommand = new RelayCommand(Loaded);
            MenuItemClickedCommand = new RelayCommand<ServerItem>(MenuItemClicked);

            AddServerCommand = new RelayCommand(AddServerClicked);
            RemoveServersCommand = new RelayCommand(RemoveServersClicked);
            AboutCommand = new RelayCommand(AboutClicked);
        }

        private void AddServerClicked()
        {
            _navigationService.NavigateTo(new Uri(ViewModelLocator.NewServerUrl, UriKind.Relative));
        }

        private void RemoveServersClicked()
        {
            _navigationService.NavigateTo(new Uri(ViewModelLocator.RemoveServersUrl, UriKind.Relative));
        }

        private void AboutClicked()
        {
            _navigationService.NavigateTo(new Uri(ViewModelLocator.AboutUrl, UriKind.Relative));
        }

        private void MenuItemClicked(ServerItem item)
        {
            // A server has been chosen.
            if (item != null)
            {
                SelectedItem = item;
                _navigationService.NavigateTo(new Uri(ViewModelLocator.GroupsUrl, UriKind.Relative));
            }
        }

        private void Loaded()
        {
            _items.Clear();
            List<ServerItem> items = DataProvider.DataProvider.GetAllServers();
            for (int iterator = 0; iterator < items.Count; iterator++)
            {
                _items.Add(items[iterator]);
            }

            _navigationService.RemoveBackStack();
        }
    }
}