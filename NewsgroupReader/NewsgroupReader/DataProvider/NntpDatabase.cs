﻿using System.Collections.Generic;
using NewsgroupReader.Models;
using DatabaseAccess.Models;

namespace NewsgroupReader.DataProvider {
    public class NntpDatabase {
        private int _ServerId;
        private static object _lock = new object();

        public NntpDatabase(string DnsServerAddress) {
            lock (_lock) {
                _ServerId = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerIdByName(DnsServerAddress);
            }
        }

        public List<ArticleItem>  GetArticlesByGroup(string newsGroup_name) {
            lock (_lock) {
                List<ArticleItem> result = new List<ArticleItem>();

                List<DatabaseAccess.Models.Article> data = DatabaseAccess.ViewModels.NNTPDatabase.GetArticlesByGroup(_ServerId, newsGroup_name);

                if (data != null) {
                    foreach (DatabaseAccess.Models.Article item in data) {
                        string[] references = item.ArticleReferences.Split(' ');
                        ArticleItem view_item = new ArticleItem(item.ArticleNumber, item.ArticlePath, item.ArticleFrom, item.NewsGroup.NewsGroupName, item.ArticleSubject, item.ArticleDate,
                            item.ArticleOrganization, item.ArticleMessageId, item.ArticleMessage, item.ArticleUnread, references, item.ArticleContentType, item.ArticleContentTransferEncoding)
                            {
                                //ArticleNumber = item.ArticleNumber,
                                //Date = item.ArticleDate,
                                //From = item.ArticleFrom,
                                //Message = item.ArticleMessage,
                                //MessageId = item.ArticleMessageId,
                                //Newsgroup = item.ArticleNG,
                                //Organization = item.ArticleOrganization,
                                //Path = item.ArticlePath,
                                //Subject = item.ArticleSubject
                            };
                        result.Add(view_item);
                    }
                }

                return result;
            }
        }

        public static List<ServerItem> getServerItems() {
            lock (_lock) {
                List<ServerItem> list = new List<ServerItem>();
                foreach (NewsServer server in DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers) {
                    ServerItem item = new ServerItem(server.NewsServerName, server.NewsServerDescription, server.NewsServerUrl, server.NewsServerUserName, server.NewsServerUserMail, -1);
                    List<NewsGroup> groups = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsGroupsByServer(server.NewsServerId);
                    foreach (NewsGroup group1 in groups) {
                        GroupItem groupItem = new GroupItem(group1.NewsGroupName);
                        item.SubscribedGroups.Add(groupItem);
                    }
                    list.Add(item);
                }
                return list;
            }
        }
        
        public static void AddNewsServer(ServerItem server) {
            lock (_lock) {
                DatabaseAccess.ViewModels.NNTPDatabase.AddNewsServer(server.Name, server.Description, server.Url, server.Username, server.Usermail);

                foreach (GroupItem item in server.SubscribedGroups) {
                    DatabaseAccess.ViewModels.NNTPDatabase.AddNewsGroup(item.Description, DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerIdByName(server.Url));
                }
            }
        }

        public List<GroupItem> getGroups() {
            lock (_lock) {
                List<GroupItem> list = new List<GroupItem>();
                foreach (NewsGroup group1 in DatabaseAccess.ViewModels.NNTPDatabase.GetNewsGroupsByServer(_ServerId)) {
                    GroupItem item = new GroupItem(group1.NewsGroupName);
                    list.Add(item);
                }
                return list;
            }
        }

        public void AddArticles(List<ArticleItem> articles) {
            lock (_lock) {
                foreach (ArticleItem article in articles) {
                    string references = "";
                    for (int iterator = 0; iterator < article.References.Length; iterator++) {
                        references += article.References[iterator];
                        if (iterator != (article.References.Length - 1))
                            references += ' ';
                    }
                    DatabaseAccess.ViewModels.NNTPDatabase.AddArticle(article.Date, article.From, article.Message, article.Newsgroup, article.MessageId, article.ArticleNumber, article.Organization,
                        article.Path, article.Subject, article.Unread, references, article.ContentType, article.ContentTransferEncoding);
                }
            }
        }

        public void AddArticle(ArticleItem article) {
            lock (_lock) {
                string references = "";
                if (article.References != null) {
                    for (int iterator = 0; iterator < article.References.Length; iterator++) {
                        references += article.References[iterator];
                        if (iterator != (article.References.Length - 1))
                            references += ' ';
                    }
                }
                DatabaseAccess.ViewModels.NNTPDatabase.AddArticle(article.Date, article.From, article.Message, article.Newsgroup, article.MessageId, article.ArticleNumber, article.Organization,
                    article.Path, article.Subject, article.Unread, references, article.ContentType, article.ContentTransferEncoding);
            }
        }

        //public List<ArticleItem> GetArticles(string newsgroup_name)
        //{
        //    List<Article> articles = DatabaseAccess.ViewModels.NNTPDatabase.GetArticlesByGroup(_ServerId, newsgroup_name);
        //    List<ArticleItem> items = new List<ArticleItem>();
        //    foreach (Article article in articles)
        //    {
        //        ArticleItem item = new ArticleItem(article.ArticleNumber, article.ArticlePath, article.ArticleFrom, article.NewsGroup.NewsGroupName, article.ArticleSubject, article.ArticleDate, article.ArticleOrganization, article.ArticleMessageId, article.ArticleMessage, article.ArticleUnread);
        //        items.Add(item);
        //    }
        //    return items;
        //}

        public List<ArticleItem> GetArticlesByServer() {
            lock (_lock) {
                List<ArticleItem> items = new List<ArticleItem>();
                List<NewsGroup> groups = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsGroupsByServer(_ServerId);
                foreach (NewsGroup newsGroup in groups) {
                    List<Article> articles = DatabaseAccess.ViewModels.NNTPDatabase.GetArticlesByGroup(newsGroup.NewsGroupId);
                    foreach (Article article in articles) {
                        string[] references = article.ArticleReferences.Split(' ');
                        ArticleItem item = new ArticleItem(article.ArticleNumber, article.ArticlePath, article.ArticleFrom, article.NewsGroup.NewsGroupName, article.ArticleSubject, article.ArticleDate,
                            article.ArticleOrganization, article.ArticleMessageId, article.ArticleMessage, article.ArticleUnread, references, article.ArticleContentType, article.ArticleContentTransferEncoding);
                        items.Add(item);
                    }
                }

                return items;
            }
        }

        public static void DeleteNewsServer(ServerItem server) {
            lock (_lock) {
                int id = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerIdByName(server.Url);
                if (id != -1)
                    DatabaseAccess.ViewModels.NNTPDatabase.DeleteNewsServer(id);
            }
        }

        public void UpdateGroups(ServerItem server) {
            lock (_lock) {
                List<NewsGroup> groups = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsGroupsByServer(_ServerId);
                List<bool> used_groups = new List<bool>();
                for (int iterator = 0; iterator < groups.Count; iterator++)
                    used_groups.Add(false);

                foreach (GroupItem group1 in server.SubscribedGroups) {
                    bool found = false;
                    for (int iterator = 0; iterator < groups.Count; iterator++) {
                        if (groups[iterator].NewsGroupName == group1.Description) {
                            found = true;
                            used_groups[iterator] = true;
                            break;
                        }
                    }
                    if (!found)
                        DatabaseAccess.ViewModels.NNTPDatabase.AddNewsGroup(group1.Description, _ServerId);
                }

                for (int iterator = 0; iterator < groups.Count; iterator++) {
                    if (!used_groups[iterator])
                        DatabaseAccess.ViewModels.NNTPDatabase.DeleteNewsGroup(groups[iterator]);
                }

            }
        }
    }
}
