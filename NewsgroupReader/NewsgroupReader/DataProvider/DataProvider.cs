﻿using NewsgroupReader.Models;
using System.Collections.Generic;
using NNTPLibrary;
using NNTPLibrary.Models;
using System.Windows;
using System.Collections.ObjectModel;
using DatabaseAccess.ViewModels;
using DatabaseAccess.Models;
using System.Threading;
using NNTPLibrary.Parsing;
using System.Net.Sockets;
using System;

namespace NewsgroupReader.DataProvider {
    public enum EError {
        Unknown, ConnectionError, NNTPUnknown, NNTPPostingAllowed,
        NNTPPostingProhibited, NNTPTemporarilyUnavailable, NNTPPermanentlyUnavailable
    };

    public class DataProvider {
        #region Events

        public delegate void GetAllNewsgroupsEvent(List<GroupItem> groups);
        public static event GetAllNewsgroupsEvent Event_GetAllNewsgroupsEvent_Handler;

        public delegate void GetAllArticlesEvent(ArticleItem thread);
        public static event GetAllArticlesEvent Event_GetAllArticlesEvent_Handler;

        public delegate void ServerAvailable(bool available);
        /// <summary>
        /// Called from static void IsServerAvailable(...)
        /// </summary>
        public static event ServerAvailable Event_ServerAvailable_Handler;

        #endregion

        private static DataProvider _instance;
        private static Dictionary<string, ServerModel> _servers;
        /// <summary>
        /// Current Server Model
        /// </summary>
        private static ServerModel _csm;
        private const int TIMEOUT = 1000;

        #region STATIC

        public static DataProvider getInstance(string Servername) {
            if (_instance == null) {
                _instance = new DataProvider();
            }

            if (_servers.ContainsKey(Servername)) {
                _csm = _servers[Servername];
            } else {
                ServerModel model = new ServerModel(Servername);
                _servers.Add(Servername, model);
                _csm = model;
                _instance.Initialize();
            }
            return _instance;
        }


        public static void IsServerAvailable(string Servername) {
            NNTPApi check_server = new NNTPApi(Servername, 119);
            check_server.Event_ConnectionStatus_Handler += new NNTPApi.ConnectionStatus(Event_ConnectionStatus_Handler);
            check_server.Initialize();
        }

        private static void Event_ConnectionStatus_Handler(bool established,
                                    System.Net.Sockets.SocketError error,
                                    NNTPLibrary.Parsing.ENNTPConnection status) {
            if (Event_ServerAvailable_Handler != null) {
               Event_ServerAvailable_Handler.Invoke(established);
            }
        }

        public static List<ServerItem> GetAllServers() {
            return NntpDatabase.getServerItems();
        }

        public static void StoreServer(ServerItem server) {
            NntpDatabase.AddNewsServer(server);
        }

        public static void DeleteServer(ServerItem server) {
            NntpDatabase.DeleteNewsServer(server);
        }

        #endregion

        public DataProvider() {
            _servers = new Dictionary<string, ServerModel>();

        }

        private void Initialize() {
            _csm.NntpNetwork.Event_ConnectionStatus_Handler += new NNTPApi.ConnectionStatus(api_Event_ConnectionStatusHandler);
            _csm.NntpNetwork.Event_InternalError_Handler += new NNTPApi.InternalError(api_Event_InternalError_Handler);

            _csm.NntpNetwork.Event_GetAllNewsgroupsCompleted_Handler 
                += new NNTPApi.GetAllNewsgroupsCompleted(NntpNetwork_Event_GetAllNewsgroupsCompleted_Handler);
            _csm.NntpNetwork.Event_SelectNewsgroupAndListArticlesCompleted_Handler 
                += new NNTPApi.SelectNewsgroupAndListArticlesCompleted(NntpNetwork_Event_SelectNewsgroupAndListArticlesCompleted_Handler);
            _csm.NntpNetwork.Event_GetArticleCompleted_Handler += new NNTPApi.GetArticleCompleted(NntpNetwork_Event_GetArticleCompleted_Handler);
        }

        #region NEWSGROUPS

        private bool _CurrentlyLoadingNewsgroups = false;
        public void GetAllNewsgroups() {
            // Pure Online
            if (!_CurrentlyLoadingNewsgroups) {
                _CurrentlyLoadingNewsgroups = true;
                _csm.NntpNetwork.GetAllNewsgroupsAsynch();
            }
        }
        private void NntpNetwork_Event_GetAllNewsgroupsCompleted_Handler(NNTPGroupList groups) {
            List<GroupItem> list = new List<GroupItem>();
            foreach (NNTPGroup group in groups.Groups) {
                list.Add(new GroupItem(group.Name));
            }

            if (Event_GetAllNewsgroupsEvent_Handler != null) {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    Event_GetAllNewsgroupsEvent_Handler(list);
                });
            }
            _CurrentlyLoadingNewsgroups = false;
        }
        public List<GroupItem> GetGroups() {
            return _csm.NntpDatabase.getGroups();
        }

        public void UpdateGroups(ServerItem server) {
            _csm.NntpDatabase.UpdateGroups(server);
        }

        #endregion

        #region ARTICLES

        private ManualResetEvent _WaitForArticle = new ManualResetEvent(false);
        private ManualResetEvent _WaitForArticles = new ManualResetEvent(false);
        private bool _StopLoadingArticles = false;
        public List<ArticleItem> GetAllArticles(string Newsgroup) {

            _StopLoadingArticles = true;
            _WaitForArticles.WaitOne(TIMEOUT);
            _StopLoadingArticles = false;

            _csm.NntpNetwork.SelectNewsgroupAndListArticles(Newsgroup); // Call Newsserver

            return _csm.NntpDatabase.GetArticlesByGroup(Newsgroup);
        }
        private void NntpNetwork_Event_SelectNewsgroupAndListArticlesCompleted_Handler(NNTPArticleList articles) {
            try {
                List<ArticleItem> db_articels = _csm.NntpDatabase.GetArticlesByGroup(articles.Group);
                List<int> new_articles = buildSymmetricDifference(articles.ArticleNumbers, ArticlesToListOfArticleNumber(db_articels));
                new_articles.Sort();
                new_articles.Reverse();
                foreach (int article in new_articles) {
                    _WaitForArticle.Reset();
                    _csm.NntpNetwork.GetArticle(article);
                    _WaitForArticle.WaitOne(TIMEOUT);
                    if (_StopLoadingArticles) { return; }
                }
            } finally {
                _StopLoadingArticles = false;
                _WaitForArticles.Set();
            }
        }
        private void NntpNetwork_Event_GetArticleCompleted_Handler(NNTPArticle article) {
            ArticleItem article_item = new ArticleItem(article.ArticleNumber, article.Path,
            article.From, article.Newsgroup, article.Subject, DateTime.Now,
            article.Organization, article.MessageId, article.Message, true,
            article.References, article.ContentType, article.ContentTransferEncoding);

            if (Event_GetAllArticlesEvent_Handler != null) {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    Event_GetAllArticlesEvent_Handler(article_item);
                });
            }
            _csm.NntpDatabase.AddArticle(article_item);
            _WaitForArticle.Set();

        }

        #endregion

        #region ERROR_HANDLING

        /// <summary>
        /// TODO: implement
        /// </summary>
        private void api_Event_InternalError_Handler(NNTPLibrary.Parsing.EInternalError error) {
        }
        /// <summary>
        /// TODO: implement
        /// </summary>
        private void api_Event_ConnectionStatusHandler(bool established,
                                            System.Net.Sockets.SocketError error,
                                            NNTPLibrary.Parsing.ENNTPConnection status) {
            EError e = EError.NNTPUnknown;
            if (!established) {
                e = EError.ConnectionError;
            } else {
                if (error != System.Net.Sockets.SocketError.Success) {
                    e = EError.ConnectionError;
                } else {
                    switch (status) {
                        case NNTPLibrary.Parsing.ENNTPConnection.PostingAllowed:
                            e = EError.NNTPPostingAllowed;
                            break;
                        case NNTPLibrary.Parsing.ENNTPConnection.PostingProhibited:
                            e = EError.NNTPPostingProhibited;
                            break;
                        case NNTPLibrary.Parsing.ENNTPConnection.TemporarilyUnavailable:
                            e = EError.NNTPTemporarilyUnavailable;
                            break;
                        case NNTPLibrary.Parsing.ENNTPConnection.PermanentlyUnavailable:
                            e = EError.NNTPPermanentlyUnavailable;
                            break;
                        default:
                            e = EError.NNTPUnknown;
                            break;
                    }
                }
            }

        }
        
        #endregion

        #region HELPERS

        //TODO: Bad ... AllItems will by changed
        private List<int> buildSymmetricDifference(List<int> AllItems, List<int> Subset) { 
            foreach(int item in Subset){
                AllItems.Remove(item);
            }
            return AllItems;
        }

        private List<int> ArticlesToListOfArticleNumber(List<ArticleItem> articles) {
            List<int> indexes = new List<int>();
            foreach (ArticleItem article in articles) {
                indexes.Add(article.ArticleNumber);
            }
            return indexes;
        }
        #endregion
    }
}
