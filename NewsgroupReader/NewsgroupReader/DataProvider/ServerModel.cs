﻿
using NNTPLibrary;

namespace NewsgroupReader.DataProvider {
    public class ServerModel {
        public NNTPApi NntpNetwork { get; set; }
        public NntpDatabase NntpDatabase { get; set; }

        public ServerModel(string Servername) {
            NntpNetwork = new NNTPApi(Servername, 119);
            NntpNetwork.Initialize();

            NntpDatabase = new NntpDatabase(Servername);

        }
    }
}
