﻿using System;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using NewsgroupReader.Resources;
using NewsgroupReader.ViewModels;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;

namespace NewsgroupReader.Views
{
    public partial class MainView : PhoneApplicationPage
    {
        MainViewModel _viewModel;

        public MainView()
        {
            InitializeComponent();

            Messenger.Default.Register<DialogMessage>(
                this,
                msg =>
                {
                    var result = MessageBox.Show(
                        msg.Content,
                        msg.Caption,
                        msg.Button);

                    if (msg.Callback != null)
                    {
                        // Send callback
                        msg.ProcessCallback(result);
                    }
                });
        }

        private void PhoneApplicationPage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationBarIconButton item = (ApplicationBar.Buttons[0] as ApplicationBarIconButton);
            if (item != null)
            {
                item.Text = AppResources.AddServerMenuItem;
            }

            item = (ApplicationBar.Buttons[1] as ApplicationBarIconButton);
            if (item != null)
            {
                item.Text = AppResources.RemoveServersButton;
            }

            item = (ApplicationBar.Buttons[2] as ApplicationBarIconButton);
            if (item != null)
            {
                item.Text = AppResources.AboutButton;
            }

            // Get ViewModel
            _viewModel = DataContext as MainViewModel;
        }

        private void addServerItem_Click(object sender, EventArgs e)
        {
            _viewModel.AddServerCommand.Execute(sender);
        }

        private void aboutItem_Click(object sender, EventArgs e)
        {
            _viewModel.AboutCommand.Execute(sender);
        }

        private void removeServerItem_Click(object sender, EventArgs e)
        {
            _viewModel.RemoveServersCommand.Execute(sender);
        }
    }
}