﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using NewsgroupReader.Resources;
using Microsoft.Phone.Shell;
using NewsgroupReader.ViewModels;
using GalaSoft.MvvmLight.Messaging;

namespace NewsgroupReader.Views
{
    public partial class NewServerView : PhoneApplicationPage
    {
        NewServerViewModel _viewModel;

        public NewServerView()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            ApplicationBarIconButton cancelButton = ApplicationBar.Buttons[0] as ApplicationBarIconButton;
            if (cancelButton != null)
            {
                cancelButton.Text = AppResources.CancelButton;
            }

            ApplicationBarIconButton okButton = ApplicationBar.Buttons[1] as ApplicationBarIconButton;
            if (okButton != null)
            {
                okButton.Text = AppResources.OKButton;
            }

            // Get ViewModel
            _viewModel = DataContext as NewServerViewModel;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            _viewModel.OKCommand.Execute(sender);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            // Hack for Windows Phone 7
            (sender as TextBox).GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            // Navigate back to Main-Page
            _viewModel.CancelCommand.Execute(sender);
        }
    }
}