﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace NewsgroupReader.Views
{
    public partial class RenderView : PhoneApplicationPage
    {
        public RenderView()
        {
            InitializeComponent();
        }

        private void WebBrowser_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Browser.Opacity = 1.0;
        }

        private void Browser_Navigating(object sender, NavigatingEventArgs e)
        {
            Browser.Opacity = 0.0;
        }
    }
}