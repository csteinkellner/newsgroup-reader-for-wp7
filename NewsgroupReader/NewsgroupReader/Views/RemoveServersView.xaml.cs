﻿using System;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using NewsgroupReader.Resources;
using NewsgroupReader.ViewModels;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;

namespace NewsgroupReader.Views
{
    public partial class RemoveServersView : PhoneApplicationPage
    {
        RemoveServersView _viewModel;

        public RemoveServersView()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            // Get ViewModel
            _viewModel = DataContext as RemoveServersView;
        }
    }
}