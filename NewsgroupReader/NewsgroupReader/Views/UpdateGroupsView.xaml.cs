﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using NewsgroupReader.ViewModels;
using Microsoft.Phone.Shell;
using NewsgroupReader.Resources;

namespace NewsgroupReader.Views
{
    public partial class UpdateGroupsView : PhoneApplicationPage
    {
        UpdateGroupsViewModel _viewModel;

        public UpdateGroupsView()
        {
            InitializeComponent();
        }

        private void FilterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            // Hack for Windows Phone 7
            (sender as TextBox).GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            _viewModel.OKCommand.Execute(sender);
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            // Get ViewModel
            _viewModel = DataContext as UpdateGroupsViewModel;

            ApplicationBarIconButton okButton = ApplicationBar.Buttons[0] as ApplicationBarIconButton;
            if (okButton != null)
            {
                okButton.Text = AppResources.OKButton;
            }
        }
    }
}