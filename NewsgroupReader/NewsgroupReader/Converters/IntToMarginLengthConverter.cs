﻿using System;
using System.Windows.Data;

namespace NewsgroupReader.Converters
{
    public class IntToMarginLengthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int int_value = (int)value;
            if ((int_value >= 10) && (int_value <= 99))
                return new System.Windows.Thickness(0, -5, 0, 0);
            else if ((int_value >= 1) && (int_value <= 9))
                return new System.Windows.Thickness(29, -5, 0, 0);
            else if (int_value >= 100)
                return new System.Windows.Thickness(20, -15, 0, 0);
            else
                return new System.Windows.Thickness(25, 0, 0, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return 0;
        }
    }
}
