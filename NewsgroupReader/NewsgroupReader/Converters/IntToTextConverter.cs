﻿using System;
using System.Windows.Data;

namespace NewsgroupReader.Converters
{
    public class IntToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int int_value = (int)value;
            if (int_value >= 100)
                return "..";
            else if ((int_value >= 1) && (int_value <= 99))
                return int_value;
            else
                return "°";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return 0;
        }
    }
}
