﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace NewsgroupReader.Converters
{
    public class IntToBackColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int int_value = (int)value;
            Color currentAccentColorHex = (Color)Application.Current.Resources["PhoneAccentColor"];
            if (int_value == 0)
                currentAccentColorHex.A = 128;

            return new SolidColorBrush(currentAccentColorHex);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return 0;
        }
    }
}
