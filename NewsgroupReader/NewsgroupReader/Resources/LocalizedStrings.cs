﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NewsgroupReader.Resources
{
    public class LocalizedStrings
    {
        public LocalizedStrings()
        {
        }

        private static NewsgroupReader.Resources.AppResources localizedResources = new NewsgroupReader.Resources.AppResources();

        public NewsgroupReader.Resources.AppResources LocalizedResources { get { return localizedResources; } }

        private static NewsgroupReader.Resources.ApplicationInformation applicationInformation = new NewsgroupReader.Resources.ApplicationInformation();

        public NewsgroupReader.Resources.ApplicationInformation ApplicationInformation { get { return applicationInformation; } }
    }
}
