﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Navigation;

namespace NewsgroupReader.Helpers
{
    public interface INavigationService
    {
        event NavigatingCancelEventHandler Navigating;
        void NavigateTo(Uri pageUri);
        void RemoveBackStack();
        void RemoveBackEntry();
        void GoBack();
    }
}
