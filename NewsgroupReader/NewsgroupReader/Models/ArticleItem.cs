﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using NNTPLibrary.Models;
using System.Collections.Generic;

namespace NewsgroupReader.Models
{
    public class ArticleItem
    {
        private int _articleNumber;
        public int ArticleNumber
        {
            get
            {
                return _articleNumber;
            }
            set
            {
                _articleNumber = value;
            }
        }

        public int Depth
        {
            get
            {
                if ((_references != null) && (!((_references.Length == 1) && (_references[0] == ""))))
                {
                    return _references.Length;
                }
                return 0;
            }
        }

        public override bool Equals(object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to ArticleItem return false.
            ArticleItem p = obj as ArticleItem;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (_articleNumber == p.ArticleNumber);
        }

        private string _path;
        public string Path
        {
            get
            {
                return _path;
            }
            set
            {
                _path = value;
            }
        }

        private string _from;
        public string From
        {
            get
            {
                return _from;
            }
            set
            {
                _from = value;
            }
        }

        private string _newsgroup;
        public string Newsgroup
        {
            get
            {
                return _newsgroup;
            }
            set
            {
                _newsgroup = value;
            }
        }

        private string _subject;
        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                _subject = value;
            }
        }

        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }

        private string _organization;
        public string Organization
        {
            get
            {
                return _organization;
            }
            set
            {
                _organization = value;
            }
        }

        private string _messageId;
        public string MessageId
        {
            get
            {
                return _messageId;
            }
            set
            {
                _messageId = value;
            }
        }

        private string _message;
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }

        private bool _unread;
        public bool Unread
        {
            get
            {
                return _unread;
            }
            set
            {
                _unread = value;
            }
        }

        private string[] _references;
        public string[] References
        {
            get
            {
                return _references;
            }
            set
            {
                _references = value;
            }
        }

        private string _contentType;
        public string ContentType
        {
            get
            {
                return _contentType;
            }
            set
            {
                _contentType = value;
            }
        }

        private string _contentTransferEncoding;
        public string ContentTransferEncoding
        {
            get
            {
                return _contentTransferEncoding;
            }
            set
            {
                _contentTransferEncoding = value;
            }
        }

        public ArticleItem(int article_number, string path, string from, string newsgroup, string subject, DateTime date, 
            string organization, string messageId, string message, bool unread, string[] references, string contentType, string contentTransferEncoding)
        {
            _articleNumber = article_number;
            _path = path;
            _from = from;
            _newsgroup = newsgroup;
            _subject = subject;
            _date = date;
            _organization = organization;
            _messageId = messageId;
            _message = message;
            _unread = unread;
            _references = references;
            _contentType = contentType;
            _contentTransferEncoding = contentTransferEncoding;
        }
    }
}
