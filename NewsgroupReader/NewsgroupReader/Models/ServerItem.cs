﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace NewsgroupReader.Models
{
    public class ServerItem
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String Url { get; set; }
        public String Username { get; set; }
        public String Usermail { get; set; }
        public int NumUnread { get; set; }

        private List<GroupItem> _subscribed;

        public List<GroupItem> SubscribedGroups
        {
            get
            {
                return _subscribed;
            }
        }

        public override bool Equals(object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to ServerItem return false.
            ServerItem p = obj as ServerItem;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Url == p.Url);
        }

        public bool Vis
        {
            get
            {
                return NumUnread != 0;
            }
        }

        public ServerItem(String Name, String Description, String Url, String Username, String Usermail, int NumUnread)
        {
            this.Name = Name;
            this.Description = Description;
            this.Url = Url;
            this.Username = Username;
            this.Usermail = Usermail;
            this.NumUnread = NumUnread;

            _subscribed = new List<GroupItem>();
        }
    }
}
