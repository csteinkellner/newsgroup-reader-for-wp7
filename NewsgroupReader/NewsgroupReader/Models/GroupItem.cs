﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NewsgroupReader.Models {
    public class GroupItem {
        private String _name;
        public String Name {
            get {
                int index = _name.LastIndexOf('.');
                if ((index > 0) && ((index + 1) < _name.Length)) {
                    return _name.Substring(index + 1);
                } else {
                    return _name;
                }
            }
        }

        public override bool Equals(object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to GroupItem return false.
            GroupItem p = obj as GroupItem;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (_name == p._name);
        }

        public String Description {
            get {
                return _name;
            }
        }

        public int NumUnread { get; set; }
        public bool Vis {
            get {
                return NumUnread != 0;
            }
        }

        public bool Chosen { get; set; }

        public GroupItem() {
        }

        public GroupItem(string name) {
            _name = name;
        }

        public GroupItem(string Name, int NumUnread, bool Chosen) {
            _name = Name; this.NumUnread = NumUnread; this.Chosen = Chosen;
        }

    }
}
