﻿using System.Windows;
using System.Windows.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Silverlight.Testing;
using DatabaseAccess;
using DatabaseAccess.Models;
using DatabaseAccess.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;


namespace NewsgroupReaderUT.Tests
{
    [TestClass]
    [Tag("Database")]
    public class DatabaseTest
    {
        [TestMethod]
        [Description("Test DB-Existing after app-start")]
        public static void TestExists()
        {
            using (NNTPDataContext db = new NNTPDataContext("Data Source=isostore:/NNTPDataBase.sdf"))
            {
                Assert.IsTrue(db.DatabaseExists());
            }
        }

        private static NewsServer getTestServerData()
        {
            NewsServer newServer = new NewsServer
            {
                NewsServerName = "TestServer",
                NewsServerDescription = "description",
                NewsServerUrl = "test.test",
                NewsServerUserName = "Testuser",
                NewsServerUserMail = "test@test.com"
            };
            return newServer;
        }

        public static void CreateServer()
        {
            DatabaseAccess.ViewModels.NNTPDatabase.AddNewsServer("TestServer", "description", "test.test", "Testuser", "test@test.com");
        }

        [TestMethod]
        [Description("Testing creating and get server")]
        public static void TestCreateServer()
        {
            NewsServer newServer = getTestServerData();
            List<NewsServer> test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty(newServer.NewsServerName, newServer.NewsServerUrl);//DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty(newServer.NewsServerName, newServer.NewsServerUrl, newServer.NewsServerUserMail, newServer.NewsServerUserName);
            int old_value = test.Count;

            CreateServer();
            test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty(newServer.NewsServerName, newServer.NewsServerUrl);//DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty(newServer.NewsServerName, newServer.NewsServerUrl, newServer.NewsServerUserMail, newServer.NewsServerUserName);
            Assert.IsTrue(old_value + 1 == test.Count);
            RemoveServer();
        }

        [TestMethod]
        [Description("Test Server get by ID")]
        public static void TestGetServerById()
        {
            CreateServer();

            int id = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0].NewsServerId;
            NewsServer server = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerById(id);
            Assert.Equals(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0], server);

            RemoveServer();
        }

        public static void RemoveServer()
        {
            NewsServer server = getTestServerData();
            DatabaseAccess.ViewModels.NNTPDatabase.DeleteNewsServer(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0]);
        }

        [TestMethod]
        [Description("Testing delete server")]
        public static void TestDeleteServer()
        {
            CreateServer();
            NewsServer server = getTestServerData();
            List<NewsServer> test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty(server.NewsServerName, server.NewsServerUrl);//DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty(server.NewsServerName, server.NewsServerUrl, server.NewsServerUserMail, server.NewsServerUserName);
            int old_value = test.Count;

            RemoveServer();
            test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty(server.NewsServerName, server.NewsServerUrl);//DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty(server.NewsServerName, server.NewsServerUrl, server.NewsServerUserMail, server.NewsServerUserName);
            Assert.IsTrue(old_value - 1 == test.Count);
        }

        [TestMethod]
        [Description("Test Server get by Property")]
        public static void TestGetServerByProperty()
        {
            CreateServer();


            List<NewsServer> test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty("TestServer", "test.test");
            Assert.IsTrue(test.Count != 0);

            test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsServerByProperty("test@tets.com", "Testuser");
            Assert.IsFalse(test.Count != 0);

            RemoveServer();
        }

        public static void CreateNewsgroups()
        {
            NewsServer server = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0];
            int old_number = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count;
            DatabaseAccess.ViewModels.NNTPDatabase.AddNewsGroup("Test", server.NewsServerId);
            DatabaseAccess.ViewModels.NNTPDatabase.AddNewsGroup("Test2", server);
        }

        public static void RemoveNewsgroups()
        {
            int old_number = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count;
            DatabaseAccess.ViewModels.NNTPDatabase.DeleteNewsGroup(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups[old_number - 1].NewsGroupId);
            DatabaseAccess.ViewModels.NNTPDatabase.DeleteNewsGroup(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups[old_number - 2]);
        }

        [TestMethod]
        [Description("Test Group creation")]
        public static void TestNewsGroupCreation()
        {
            CreateServer();

            NewsServer server = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0];
            int old_number = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count;

            CreateNewsgroups();

            Assert.IsTrue(old_number + 2 == DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count);

            RemoveNewsgroups();

            RemoveServer();
        }

        [TestMethod]
        [Description("Test Group deletion")]
        public static void TestNewsGroupDeletion()
        {
            CreateServer();
            CreateNewsgroups();
            int old_number = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count;

            RemoveNewsgroups();            

            Assert.IsTrue(old_number - 2 == DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count);

            RemoveServer();
        }

        [TestMethod]
        [Description("Test Get Group by Server")]
        public static void TestGetGroupByServer()
        {
            CreateServer();
            CreateNewsgroups();

            NewsServer server = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0];
            List<NewsGroup> test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsGroupsByServer(server.NewsServerId);
            Assert.IsTrue(test.Count >= 2);

            test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsGroupsByServer(server.NewsServerId);
            Assert.IsTrue(test.Count >= 2);

            RemoveNewsgroups();
            RemoveServer();
        }

        [TestMethod]
        [Description("Test get Group by id")]
        public static void TestGetGroupById()
        {
            CreateServer();
            CreateNewsgroups();

            int id = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups[0].NewsGroupId;
            NewsGroup test = DatabaseAccess.ViewModels.NNTPDatabase.GetNewsGroupById(id);
            Assert.Equals(test, DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups[0]);

            RemoveNewsgroups();
            RemoveServer();
        }

        public static void CreateArticles()
        {
            DateTime date = new DateTime(2012, 06, 03);
            DatabaseAccess.ViewModels.NNTPDatabase.AddArticle(date, "Hans", "Testtext", "Test", " dd ", 34, "org", "path", "subTest", true, "1 2 3", "utf-8", "trans");

            NewsGroup group1 = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups[0];
            DatabaseAccess.ViewModels.NNTPDatabase.AddArticle(date, "Hans", "Testtext", group1.NewsGroupId, "ee", 22, "org", "path", "subTest", true, "1 2 3", "utf-8", "trans");

            DatabaseAccess.ViewModels.NNTPDatabase.AddArticle(date, "Hans", "Testtext", group1.NewsGroupId, "ff", 22, "org", "path", "subTest", true, "1 2 3", "utf-8", "trans");
        }

        [TestMethod]
        [Description("Test Article creation")]
        public static void TestArticleCreation()
        {
            CreateServer();
            CreateNewsgroups();

            int old_number = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count;
            CreateArticles();
            Assert.IsTrue(old_number + 3 == DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count);
            RemoveArticles();

            RemoveNewsgroups();
            RemoveServer();
        }

        public static void RemoveArticles()
        {
            int old_number = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count;
            DatabaseAccess.ViewModels.NNTPDatabase.DeleteArticle(DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[old_number - 1]);
            DatabaseAccess.ViewModels.NNTPDatabase.DeleteArticle(DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[old_number - 2].ArticleId);
            DatabaseAccess.ViewModels.NNTPDatabase.DeleteArticle(DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[old_number - 3]);
        }

        [TestMethod]
        [Description("Test Article deletion")]
        public static void TestArticleDeletion()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();
            int old_number = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count;
            RemoveArticles();
            Assert.IsTrue(old_number - 3 == DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count);
            RemoveNewsgroups();
            RemoveServer();
        }

        [TestMethod]
        [Description("Test get Articles by Group")]
        public static void TestGetArticlesByGroup()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            NewsGroup group1 = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups[0];
            List<Article> test = DatabaseAccess.ViewModels.NNTPDatabase.GetArticlesByGroup(group1.NewsGroupId);
            Assert.IsTrue(test.Count >= 2);

            test = DatabaseAccess.ViewModels.NNTPDatabase.GetArticlesByGroup(group1.NewsGroupId);
            Assert.IsTrue(test.Count >= 2);

            RemoveArticles();
            RemoveNewsgroups();
            RemoveServer();
        }

        [TestMethod]
        [Description("Test get Articles by id")]
        public static void TestGetArticlesById()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            int id = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[0].ArticleId;
            Article test = DatabaseAccess.ViewModels.NNTPDatabase.GetArticleById(id);
            Assert.Equals(test, DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[0]);

            RemoveArticles();
            RemoveNewsgroups();
            RemoveServer();
        }
         
        [TestMethod]
        [Description("Test setting Article read")]
        public static void TestSetArticleUnread()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            int id = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count - 1].ArticleId;
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.GetArticleById(id).ArticleUnread);
            DatabaseAccess.ViewModels.NNTPDatabase.SetArticleUnread(false, id);
            Assert.IsFalse(DatabaseAccess.ViewModels.NNTPDatabase.GetArticleById(id).ArticleUnread);
            Assert.IsFalse(DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count - 1].ArticleUnread);

            RemoveArticles();
            RemoveNewsgroups();
            RemoveServer();
        }

        [TestMethod]
        [Description("Test setting Article message")]
        public static void TestSetArticleMessage()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            int id = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count - 1].ArticleId;

            DatabaseAccess.ViewModels.NNTPDatabase.SetArticleMessage("new Message", id);
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.GetArticleById(id).ArticleMessage.Equals("new Message"));
            Assert.IsFalse(DatabaseAccess.ViewModels.NNTPDatabase.AllArticles[DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count - 1].ArticleMessage.Equals("newMessage"));

            RemoveArticles();
            RemoveNewsgroups();
            RemoveServer();
        }

        [TestMethod]
        [Description("Test if deleting server deletes all items (Newsgroups and Articles of Server)")]
        public static void TestDeleteServerAndItemsBelow()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            RemoveServer();

            int server_nr = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers.Count;
            int group_nr = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count;
            int article_nr = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count;
             
            Assert.IsTrue((server_nr + group_nr + article_nr) == 0);
        }

        [TestMethod]
        [Description("Test if deleting newsgroup deletes all newsgroup articles too")]
        public static void TestDeleteGroupAndItsArticles()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            RemoveNewsgroups();

            int group_nr = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count;
            int article_nr = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count;

            Assert.IsTrue((group_nr + article_nr) == 0);
            RemoveServer();
        }

        [TestMethod]
        [Description("Test if double entries are prohibited")]
        public static void TestDoubleEntries()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            int server_nr1 = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers.Count;
            int group_nr1 = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count;
            int article_nr1 = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count;

            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            int server_nr2 = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers.Count;
            int group_nr2 = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count;
            int article_nr2 = DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count;

            Assert.IsTrue(server_nr1 == server_nr2);
            Assert.IsTrue(group_nr1 == group_nr2);
            Assert.IsTrue(article_nr1 == article_nr2);

            RemoveServer(); 
        }

        [TestMethod]
        [Description("Test if double entries are prohibited and server gets overwritten")]
        public static void TestDoubleEntries2()
        {
            CreateServer();
            CreateNewsgroups();
            CreateArticles();

            int server_nr1 = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers.Count;

            DatabaseAccess.ViewModels.NNTPDatabase.AddNewsServer("TestServer1", "description1", "test.test", "Testuser1", "test@test.com1");

            int server_nr2 = DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers.Count;
            Assert.IsTrue(server_nr1 == server_nr2);
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0].NewsServerDescription.Equals("description1"));
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0].NewsServerName.Equals("TestServer1"));
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0].NewsServerUserName.Equals("Testuser1"));
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0].NewsServerUserMail.Equals("test@test.com1"));

            RemoveServer();
        }
    }
}
