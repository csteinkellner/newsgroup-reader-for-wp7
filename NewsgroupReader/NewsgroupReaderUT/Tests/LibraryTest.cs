﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NNTPLibrary.Net;
using System.Net.Sockets;
using System.Threading;
using NNTPLibrary.Parsing;
using NNTPLibrary.Models;
using Microsoft.Silverlight.Testing;
using NNTPLibrary;
using NNTPLibrary.Common;
using System.Collections.Generic;
using System.Text;

namespace NewsgroupReaderUT.Tests {
    [TestClass]
    [Tag("TCP")]
    public class LibraryTest {
        const string TCP_SERVER_ADDRESS = "192.168.2.23";
        const int TCP_SERVER_ADDRESS_PORT = 4000;
        const string NNTP_SERVER_ADDRESS = "news.tugraz.at";
        const int NNTP_SERVER_ADDRESS_PORT = 119;
        const string NNTP_SERVER_NEWSGROUP = "tu-graz.test";
        const int NNTP_SERVER_ARTICLE_NUMBER = 49733;
        const int TIMEOUT = 35000;
        const float ALLOWED_ERROR_RATE = 0.01f;

        #region NETWORK_FUNDAMENTALS

        [TestMethod]
        public static void ConnectionTest() {
            NetworkClient network = new SlackSocketClientV2();

            bool rise = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            network.Event_ConnectionEstablishedHandler += new NetworkClient.
                ConnectionEstablished(delegate(bool establised, SocketError error, String message) {
                Assert.IsTrue(establised);
                rise = true;
                wait.Set();
            });

            wait.Reset();
            network.Connect(TCP_SERVER_ADDRESS, TCP_SERVER_ADDRESS_PORT);
            wait.WaitOne(TIMEOUT);

            Assert.IsTrue(rise);
            Assert.IsTrue(network.isConnected());
            network.Dispose();
            Assert.IsFalse(network.isConnected());
        }

        [TestMethod]
        public static void DisconnectionTest() {
            NetworkClient network = new SlackSocketClientV2();
            
            bool rise = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            network.Event_ConnectionEstablishedHandler += new NetworkClient.
                ConnectionEstablished(delegate(bool establised, SocketError error, String message) {
                Assert.IsTrue(establised);
                Assert.IsTrue(network.isConnected());
                rise = true;
                wait.Set();
            });

            wait.Reset();
            network.Connect(TCP_SERVER_ADDRESS, TCP_SERVER_ADDRESS_PORT);
            wait.WaitOne(TIMEOUT);

            Assert.IsTrue(rise);
            Assert.IsTrue(network.isConnected());
            network.Dispose();
            Assert.IsFalse(network.isConnected());
        }

        [TestMethod]
        public static void SendSimpleTest() {
            NetworkClient network = new SlackSocketClientV2();

            bool rise = false;
            ManualResetEvent wait1 = new ManualResetEvent(false);
            network.Event_ConnectionEstablishedHandler += new NetworkClient.
                ConnectionEstablished(delegate(bool establised, SocketError error, String message) {
                Assert.IsTrue(establised);
                rise = true;
                wait1.Set();
            });

            wait1.Reset();
            network.Connect(TCP_SERVER_ADDRESS, TCP_SERVER_ADDRESS_PORT);
            wait1.WaitOne(TIMEOUT);

            Assert.IsTrue(rise);
            Assert.IsTrue(network.isConnected());

            bool event_fired = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            network.Event_TransferDoneHandler += new NetworkClient.TransferDone(delegate(bool error, string data) {
                Assert.IsFalse(error);
                event_fired = true;
                wait.Set();
            });

            wait.Reset();
            network.Send("TEST");
            wait.WaitOne(TIMEOUT);
            network.Dispose();

            Assert.IsTrue(event_fired);
        }

        [TestMethod]
        public static void ReceiveSimpleTest() {
            NetworkClient network = new SlackSocketClientV2();

            network.Event_TransferDoneHandler += new NetworkClient.TransferDone(delegate(bool error, string data) {
                Assert.IsFalse(error);
            });

            bool fired = false;
            ManualResetEvent wait1 = new ManualResetEvent(false);
            network.Event_ConnectionEstablishedHandler += new NetworkClient.
                ConnectionEstablished(delegate(bool establised, SocketError error, String message) {
                Assert.IsTrue(establised);
                fired = true;
                wait1.Set();
            });

            wait1.Reset();
            network.Connect(TCP_SERVER_ADDRESS, TCP_SERVER_ADDRESS_PORT);
            wait1.WaitOne(TIMEOUT);

            Assert.IsTrue(fired);
            Assert.IsTrue(network.isConnected());
            
            String receive = "";
            ManualResetEvent wait = new ManualResetEvent(false);
            network.Event_DataReceivedHandler += new NetworkClient.DataReceived(delegate(object obj, DataEventArgs data) {
                receive = new string(Encoding.UTF8.GetChars(data.Data));
                wait.Set();
            });

            Assert.IsTrue(network.Send("ECHO HELLO"));
            wait.Reset();
            wait.WaitOne(TIMEOUT);
            network.Dispose();

            Assert.IsTrue(receive == "HELLO");
        }

        [TestMethod]
        public static void TCPServerAvailableNNTPNotOkayTest() {
            NNTPApi api = new NNTPApi(TCP_SERVER_ADDRESS, TCP_SERVER_ADDRESS_PORT);

            bool fired = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            api.Event_ConnectionStatus_Handler += new NNTPApi
                .ConnectionStatus(delegate(bool establised, SocketError error, ENNTPConnection status) {

                Assert.IsTrue(!establised);
                fired = true;
                wait.Set();
            });

            wait.Reset();
            api.Initialize();
            wait.WaitOne(TIMEOUT);
            Assert.IsTrue(fired);
        }

        [TestMethod]
        public static void TCPServerAvailableNNTPOkayTest() {

            NNTPApi api = new NNTPApi(NNTP_SERVER_ADDRESS, NNTP_SERVER_ADDRESS_PORT);

            bool fired = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            api.Event_ConnectionStatus_Handler += new NNTPApi
                .ConnectionStatus(delegate(bool establised, SocketError error, ENNTPConnection status) {

                Assert.IsTrue(establised && error == SocketError.Success && status == ENNTPConnection.PostingAllowed);
                fired = true;
                wait.Set();
            });

            wait.Reset();
            api.Initialize();
            wait.WaitOne();
            Assert.IsTrue(fired);
        }

        [TestMethod]
        public static void TCPServerAvailableWrongPortTest() {

            NNTPApi api = new NNTPApi(NNTP_SERVER_ADDRESS, 42);

            bool fired = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            api.Event_ConnectionStatus_Handler += new NNTPApi
                .ConnectionStatus(delegate(bool establised, SocketError error, ENNTPConnection status) {

                Assert.IsTrue(!establised && error == SocketError.TimedOut && status == ENNTPConnection.Unknown);
                fired = true;
                wait.Set();
            });

            wait.Reset();
            api.Initialize();
            wait.WaitOne(TIMEOUT);
            Assert.IsTrue(fired);
        }

        #endregion

        #region NETWORK_EXTENDED

        [TestMethod]
        public static void NNTP1Test() {

            NNTPApi api = new NNTPApi(NNTP_SERVER_ADDRESS, NNTP_SERVER_ADDRESS_PORT);

            api.Initialize();

            List<String> errors = new List<string>();
            api.Event_InternalError_Handler += new NNTPApi.InternalError(delegate(EInternalError error) {
                errors.Add(error.ToString());
            });
            api.Event_ConnectionStatus_Handler += new NNTPApi.ConnectionStatus(delegate(bool establised, SocketError error, ENNTPConnection status) {
                errors.Add(establised.ToString() + " " + error.ToString() + " " + status.ToString());
            });


            bool groups_fired = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            api.Event_GetAllNewsgroupsCompleted_Handler += new NNTPApi.GetAllNewsgroupsCompleted(
                delegate(NNTPGroupList groups) {
                    Assert.IsTrue(groups.Groups != null && groups.Groups.Count > 0);
                    groups_fired = true;
                    wait.Set();
                });

            wait.Reset();
            api.GetAllNewsgroupsAsynch();
            wait.WaitOne(TIMEOUT);
            Assert.IsTrue(groups_fired);


            bool articles_fired = false;
            ManualResetEvent wait2 = new ManualResetEvent(false);
            api.Event_SelectNewsgroupAndListArticlesCompleted_Handler += new
                NNTPApi.SelectNewsgroupAndListArticlesCompleted(delegate(NNTPArticleList articles) {
                Assert.IsTrue(articles.ArticleNumbers != null && articles.ArticleNumbers.Count > 0);
                articles_fired = true;
                wait2.Set();
            });

            wait2.Reset();
            api.SelectNewsgroupAndListArticles(NNTP_SERVER_NEWSGROUP);
            wait2.WaitOne(TIMEOUT);
            Assert.IsTrue(articles_fired);


            bool article_fired = false;
            ManualResetEvent wait3 = new ManualResetEvent(false);
            api.Event_GetArticleCompleted_Handler += new
                NNTPApi.GetArticleCompleted(delegate(NNTPArticle article) {
                Assert.IsTrue(article != null && article.Message != null);
                article_fired = true;
                wait3.Set();
            });

            wait3.Reset();
            api.GetArticle(NNTP_SERVER_ARTICLE_NUMBER);
            wait3.WaitOne(TIMEOUT);
            Assert.IsTrue(article_fired);
        }

        [TestMethod]
        public static void NNTP2Test() {

            NNTPApi api = new NNTPApi(NNTP_SERVER_ADDRESS, NNTP_SERVER_ADDRESS_PORT);

            api.Initialize();

            int error_counter = 0;
            List<String> errors = new List<string>();
            api.Event_InternalError_Handler += new NNTPApi.InternalError(delegate(EInternalError error) {
                errors.Add(error.ToString());
                if (error == EInternalError.TransferFailed) {
                    error_counter++;
                } else {
                    Assert.Fail();
                }
            });
            api.Event_ConnectionStatus_Handler += new NNTPApi.ConnectionStatus(delegate(bool establised, SocketError error, ENNTPConnection status) {
                errors.Add(establised.ToString() + " " + error.ToString() + " " + status.ToString());
                if (!establised || error != SocketError.Success) {
                    error_counter++;
                }
            });

            bool groups_fired = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            api.Event_GetAllNewsgroupsCompleted_Handler += new NNTPApi.GetAllNewsgroupsCompleted(
                delegate(NNTPGroupList groups) {
                    Assert.IsTrue(groups.Groups != null && groups.Groups.Count > 0);
                    groups_fired = true;
                    wait.Set();
                });

            wait.Reset();
            api.GetAllNewsgroupsAsynch();
            wait.WaitOne(TIMEOUT);
            Assert.IsTrue(groups_fired);

            NNTPArticleList all_articles = new NNTPArticleList();
            bool articles_fired = false;
            ManualResetEvent wait2 = new ManualResetEvent(false);
            api.Event_SelectNewsgroupAndListArticlesCompleted_Handler += new
                NNTPApi.SelectNewsgroupAndListArticlesCompleted(delegate(NNTPArticleList articles) {
                Assert.IsTrue(articles.ArticleNumbers != null && articles.ArticleNumbers.Count > 0);
                all_articles = articles;
                articles_fired = true;
                wait2.Set();
            });

            wait2.Reset();
            api.SelectNewsgroupAndListArticles(NNTP_SERVER_NEWSGROUP);
            wait2.WaitOne(TIMEOUT);
            Assert.IsTrue(articles_fired);

            List<NNTPArticle> collect = new List<NNTPArticle>();
            int count_article = 0;
            ManualResetEvent wait3 = new ManualResetEvent(false);
            api.Event_GetArticleCompleted_Handler += new
                NNTPApi.GetArticleCompleted(delegate(NNTPArticle article) {
                Assert.IsTrue(article != null && article.Message != null);
                collect.Add(article);
                count_article++;
                wait3.Set();
            });


            foreach (int index in all_articles.ArticleNumbers) {
                wait3.Reset();
                api.GetArticle(index);
                wait3.WaitOne(TIMEOUT);

            }

            Assert.IsTrue((count_article + error_counter) == all_articles.ArticleNumbers.Count);
        }

        [TestMethod]
        [Tag("NN")]
        public static void NNTP3Test() {
            NNTPApi api = new NNTPApi(NNTP_SERVER_ADDRESS, NNTP_SERVER_ADDRESS_PORT);

            api.Initialize();

            int error_counter = 0;
            List<String> errors = new List<string>();
            api.Event_InternalError_Handler += new NNTPApi.InternalError(delegate(EInternalError error) {
                errors.Add(error.ToString());
                if (error == EInternalError.TransferFailed) {
                    error_counter++;
                } else {
                    Assert.Fail();
                }
            });
            api.Event_ConnectionStatus_Handler += new NNTPApi.ConnectionStatus(delegate(bool establised, SocketError error, ENNTPConnection status) {
                errors.Add(establised.ToString() + " " + error.ToString() + " " + status.ToString());
                if (!establised || error != SocketError.Success) {
                    error_counter++;
                }
            });

            List<NNTPArticle> collect = new List<NNTPArticle>();
            bool articles_fired = false;
            int article_count = 0;

            ManualResetEvent wait2 = new ManualResetEvent(false);
            api.Event_SelectNewsgroupAndListArticlesCompleted_Handler += new
                NNTPApi.SelectNewsgroupAndListArticlesCompleted(delegate(NNTPArticleList articles) {
                Assert.IsTrue(articles.ArticleNumbers != null && articles.ArticleNumbers.Count > 0);

                article_count = articles.ArticleNumbers.Count;
                articles.ArticleNumbers.Reverse();

                ManualResetEvent wait3 = new ManualResetEvent(false);
                api.Event_GetArticleCompleted_Handler += new
                    NNTPApi.GetArticleCompleted(delegate(NNTPArticle article) {
                    Assert.IsTrue(article != null && article.Message != null);
                    collect.Add(article);
                    wait3.Set();
                });

                foreach (int index in articles.ArticleNumbers) {
                    wait3.Reset();
                    api.GetArticle(index);
                    wait3.WaitOne(TIMEOUT);
                }
                articles_fired = true;

                wait2.Set();
            });

            wait2.Reset();
            api.SelectNewsgroupAndListArticles(NNTP_SERVER_NEWSGROUP);
            wait2.WaitOne(TIMEOUT * 245);

            Assert.IsTrue(articles_fired);
            Assert.IsTrue(collect.Count > 0);
            Assert.IsTrue((collect.Count + error_counter) == article_count);
            Assert.IsTrue(error_counter <= (int)(((float)article_count) * ALLOWED_ERROR_RATE));
        }

        #endregion

        #region PARSING

        #region COMMANDPARSER

        [TestMethod]
        public static void CommandParserResponsesCodeTest() {
            string text = "215 list of newsgroups follows\r\n" +
                            "misc.test 3002322 3000234 y\r\n" +
                            "comp.risks 442001 441099 m\r\n" +
                            "alt.rfc-writers.recovery 4 1 y\r\n" +
                            "tx.natives.recovery 89 56 y\r\n" +
                            "tx.natives.recovery.d 11 9 n\r\n" +
                            ".\r\n";
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            int? code = CommandParser.getResponsesCode(data);
            Assert.IsTrue(code != null && code == 215);
        }

        [TestMethod]
        public static void CommandParserResponsesCodeWrongTestcaseTest() {
            string text = "215 list of newsgroups follows\r\n" +
                            ".\r\n";
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            int? code = CommandParser.getResponsesCode(data);
            Assert.IsFalse(code == 216);
        }

        [TestMethod]
        public static void CommandParserResponsesCodeNotAvailableTest() {
            string text = "list of newsgroups follows\r\n" +
                            ".\r\n";
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            int? code = CommandParser.getResponsesCode(data);
            Assert.IsTrue(code == null);
        }

        [TestMethod]
        public static void CommandParserSimpleMultilineLISTTest() {
            string text = "215 list of newsgroups follows\r\n" +
                            "misc.test 3002322 3000234 y\r\n" +
                            "comp.risks 442001 441099 m\r\n" +
                            "alt.rfc-writers.recovery 4 1 y\r\n" +
                            "tx.natives.recovery 89 56 y\r\n" +
                            "tx.natives.recovery.d 11 9 n\r\n" +
                            ".\r\n";
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            byte[] command_b = CommandParser.GetCommand(ref data);

            string command = new string(Utils.DefaultEncoding.GetChars(command_b));
            Assert.IsTrue(data.Length == 0 && command == "215 list of newsgroups follows\r\n" +
                                                    "misc.test 3002322 3000234 y\r\n" +
                                                    "comp.risks 442001 441099 m\r\n" +
                                                    "alt.rfc-writers.recovery 4 1 y\r\n" +
                                                    "tx.natives.recovery 89 56 y\r\n" +
                                                    "tx.natives.recovery.d 11 9 n\r\n" +
                                                    ".\r\n");

        }

        [TestMethod]
        public static void CommandParserSimpleMultilineLISTGROUPTest() {
            string text = "211 2000 3000234 3002322 misc.test list follows\r\n" +
                            "3000234\r\n" +
                            "3000237\r\n" +
                            "3000238\r\n" +
                            "3000239\r\n" +
                            "3002322\r\n" +
                            ".\r\n";
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            byte[] command_b = CommandParser.GetCommand(ref data);

            string command = new string(Utils.DefaultEncoding.GetChars(command_b));
            Assert.IsTrue(data.Length == 0 && command == "211 2000 3000234 3002322 misc.test list follows\r\n" +
                            "3000234\r\n" +
                            "3000237\r\n" +
                            "3000238\r\n" +
                            "3000239\r\n" +
                            "3002322\r\n" +
                            ".\r\n");

        }

        [TestMethod]
        public static void CommandParserSimpleMultilineARTICLETest() {
            string text = "220 3000234 <45223423@example.com>\r\n" +
                            "Path: pathost!demo!whitehouse!not-for-mail\r\n" +
                            "From: \"Demo User\" <nobody@example.net>\r\n" +
                            "Newsgroups: misc.test\r\n" +
                            "Subject: I am just a test article\r\n" +
                            "Date: 6 Oct 1998 04:38:40 -0500\r\n" +
                            "Organization: An Example Net, Uncertain, Texas\r\n" +
                            "Message-ID: <45223423@example.com>\r\n" +
                            "\r\n" +
                            "This is just a test article.\r\n" +
                            ".\r\n";
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            byte[] command_b = CommandParser.GetCommand(ref data);

            string command = new string(Utils.DefaultEncoding.GetChars(command_b));

            Assert.IsTrue(data.Length == 0 && command == "220 3000234 <45223423@example.com>\r\n" +
                            "Path: pathost!demo!whitehouse!not-for-mail\r\n" +
                            "From: \"Demo User\" <nobody@example.net>\r\n" +
                            "Newsgroups: misc.test\r\n" +
                            "Subject: I am just a test article\r\n" +
                            "Date: 6 Oct 1998 04:38:40 -0500\r\n" +
                            "Organization: An Example Net, Uncertain, Texas\r\n" +
                            "Message-ID: <45223423@example.com>\r\n" +
                            "\r\n" +
                            "This is just a test article.\r\n" +
                            ".\r\n");

        }

        [TestMethod]
        public static void CommandParserSimpleSinglelinePOSTTest() {
            string text = "340 Input article; end with <CR-LF>.<CR-LF>\r\n";

            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            byte[] command_b = CommandParser.GetCommand(ref data);
            string command = new string(Utils.DefaultEncoding.GetChars(command_b));

            Assert.IsTrue(data.Length == 0 && command == "340 Input article; end with <CR-LF>.<CR-LF>\r\n");

        }

        [TestMethod]
        public static void CommandParserExtended1Test() {
            string text = "340 Input article; end with <CR-LF>.<CR-LF>\r\n";
            text += "220 3000234 <45223423@example.com>\r\n" +
                    "Path: pathost!demo!whitehouse!not-for-mail\r\n" +
                    "From: \"Demo User\" <nobody@example.net>\r\n" +
                    "Newsgroups: misc.test\r\n" +
                    "Subject: I am just a test article\r\n" +
                    "Date: 6 Oct 1998 04:38:40 -0500\r\n" +
                    "Organization: An Example Net, Uncertain, Texas\r\n" +
                    "Message-ID: <45223423@example.com>\r\n" +
                    "\r\n" +
                    "This is just a test article.\r\n" +
                    ".\r\n";
            text += "211 2000 3000234 3002322 misc.test list follows\r\n" +
                    "3000234\r\n" +
                    "3000237\r\n" +
                    "3000238\r\n" +
                    "3000239\r\n" +
                    "3002322\r\n" +
                    ".\r\n";

            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            byte[] command_b = CommandParser.GetCommand(ref data);

            string command = new string(Utils.DefaultEncoding.GetChars(command_b));
            string str_data = new string(Utils.DefaultEncoding.GetChars(data));

            Assert.IsTrue(str_data == "220 3000234 <45223423@example.com>\r\n" +
                    "Path: pathost!demo!whitehouse!not-for-mail\r\n" +
                    "From: \"Demo User\" <nobody@example.net>\r\n" +
                    "Newsgroups: misc.test\r\n" +
                    "Subject: I am just a test article\r\n" +
                    "Date: 6 Oct 1998 04:38:40 -0500\r\n" +
                    "Organization: An Example Net, Uncertain, Texas\r\n" +
                    "Message-ID: <45223423@example.com>\r\n" +
                    "\r\n" +
                    "This is just a test article.\r\n" +
                    ".\r\n" +
                    "211 2000 3000234 3002322 misc.test list follows\r\n" +
                    "3000234\r\n" +
                    "3000237\r\n" +
                    "3000238\r\n" +
                    "3000239\r\n" +
                    "3002322\r\n" +
                    ".\r\n"
                && command == "340 Input article; end with <CR-LF>.<CR-LF>\r\n");

            command_b = CommandParser.GetCommand(ref data);
            command = new string(Utils.DefaultEncoding.GetChars(command_b));
            str_data = new string(Utils.DefaultEncoding.GetChars(data));

            Assert.IsTrue(str_data == "211 2000 3000234 3002322 misc.test list follows\r\n" +
                    "3000234\r\n" +
                    "3000237\r\n" +
                    "3000238\r\n" +
                    "3000239\r\n" +
                    "3002322\r\n" +
                    ".\r\n"
                && command == "220 3000234 <45223423@example.com>\r\n" +
                    "Path: pathost!demo!whitehouse!not-for-mail\r\n" +
                    "From: \"Demo User\" <nobody@example.net>\r\n" +
                    "Newsgroups: misc.test\r\n" +
                    "Subject: I am just a test article\r\n" +
                    "Date: 6 Oct 1998 04:38:40 -0500\r\n" +
                    "Organization: An Example Net, Uncertain, Texas\r\n" +
                    "Message-ID: <45223423@example.com>\r\n" +
                    "\r\n" +
                    "This is just a test article.\r\n" +
                    ".\r\n");

            command_b = CommandParser.GetCommand(ref data);
            command = new string(Utils.DefaultEncoding.GetChars(command_b));
            str_data = new string(Utils.DefaultEncoding.GetChars(data));

            Assert.IsTrue(str_data == ""
                && command == "211 2000 3000234 3002322 misc.test list follows\r\n" +
                    "3000234\r\n" +
                    "3000237\r\n" +
                    "3000238\r\n" +
                    "3000239\r\n" +
                    "3002322\r\n" +
                    ".\r\n");
        }

        [TestMethod]
        public static void CommandParserExtendedWrongResponseCodePostionTest() {
            string text = ".\r\n340 Input article; end with <CR-LF>.<CR-LF>\r\n" +
                            "211 2000 3000234 3002322 misc.test list follows\r\n" +
                            "3000234\r\n" +
                            "3000237\r\n" +
                            "3000238\r\n";
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            byte[] command = CommandParser.GetCommand(ref data);

            Assert.IsTrue(data == null && command == null);

        }

        [TestMethod]
        public static void CommandParserExtendedNotSupportedTest() {
            string text = "666 Input article; end with <CR-LF>.<CR-LF>\r\n" +
                            "211 2000 3000234 3002322 misc.test list follows\r\n" +
                            "3000234\r\n" +
                            "3000237\r\n" +
                            "3000238\r\n";

            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            byte[] command = CommandParser.GetCommand(ref data);

            Assert.IsTrue(data == null && command == null);
        }

        #endregion

        [TestMethod]
        public static void DataParserLISTTest() {
            string text = "215 list of newsgroups follows\r\n" +
                            "misc.test 3002322 3000234 y\r\n" +
                            "comp.risks 442001 441099 m\r\n" +
                            ".\r\n";
            FireDummy fire = new FireDummy();
            IParse parser = new ParseLIST(fire);
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            parser.parse(data);

            Assert.IsFalse(fire.Groups == null);
            Assert.IsFalse(fire.Groups.Groups == null);

            Assert.IsTrue(fire.Groups.Groups.Count == 2);
            NNTPGroup grp1 = fire.Groups.Groups[0];
            Assert.IsTrue(grp1.Name == "misc.test" && grp1.NumberOfFirstArticle == 3000234 &&
                grp1.NumberOfLastArticle == 3002322 && grp1.Posting == NNTPGroup.EPosting.Allowed);

            NNTPGroup grp2 = fire.Groups.Groups[1];
            Assert.IsTrue(grp2.Name == "comp.risks" && grp2.NumberOfFirstArticle == 441099 &&
                grp2.NumberOfLastArticle == 442001 && grp2.Posting == NNTPGroup.EPosting.Forwarded);
        }

        [TestMethod]
        public static void DataParserLISTGROUPSTest() {
            string text = "211 2000 3000234 3002322 misc.test list follows\r\n" +
                            "3000234\r\n" +
                            "3000237\r\n" +
                            ".\r\n";
            FireDummy fire = new FireDummy();
            IParse parse = new ParseLISTGROUP(fire);
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            parse.parse(data);

            Assert.IsFalse(fire.Articles == null);
            Assert.IsFalse(fire.Articles.ArticleNumbers == null);

            Assert.IsTrue(fire.Articles.ArticleNumbers.Count == 2);
            int art_number1 = fire.Articles.ArticleNumbers[0];
            Assert.IsTrue(art_number1 == 3000234);

            int art_number2 = fire.Articles.ArticleNumbers[1];
            Assert.IsTrue(art_number2 == 3000237);
        }

        [TestMethod]
        public static void DataParserARTICLETest() {
            string text = "220 3000234 <45223423@example.com>\r\n" +
                            "Path: pathost!demo!whitehouse!not-for-mail\r\n" +
                            "From: \"Demo User\" <nobody@example.net>\r\n" +
                            "Newsgroups: misc.test\r\n" +
                            "Subject: I am just a test article\r\n" +
                            "Date: 6 Oct 1998 05:38:40 -0500\r\n" +
                            "Organization: An Example Net, Uncertain, Texas\r\n" +
                            "Message-ID: <45223423@example.com>\r\n" +
                            "\r\n" +
                            "This is just a test article.\r\n" +
                            "Line two.\r\n" +
                            ".\r\n";
            FireDummy fire = new FireDummy();
            IParse parse = new ParseARTICLE(fire);
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            parse.parse(data);
            Assert.IsTrue(fire.Error == EInternalError.Unknown);

            Assert.IsFalse(fire.Article == null);
            Assert.IsTrue(fire.Article.ArticleNumber == 3000234);
            Assert.IsTrue(fire.Article.Date == new DateTime(1998, 10, 6, 12, 38, 40));
            Assert.IsTrue(fire.Article.From == "\"Demo User\" <nobody@example.net>");
            Assert.IsTrue(fire.Article.Message == "This is just a test article.\r\nLine two.");
            Assert.IsTrue(fire.Article.MessageId == "<45223423@example.com>");
            Assert.IsTrue(fire.Article.Newsgroup == "misc.test");
            Assert.IsTrue(fire.Article.Organization == "An Example Net, Uncertain, Texas");
            Assert.IsTrue(fire.Article.Subject == "I am just a test article");

        }

        [TestMethod]
        [Tag("DEV")]
        public static void DataParserARTICLE2Test() {
            string raw_data = "220 2080 <in1qd6$af4$1@fstgss00.tu-graz.ac.at> article\r\n" +
                            "Path: news.tugraz.at!not-for-mail\r\n" +
                            "From: Wolfgang Kopp <wolfgang.kopp@student.tugraz.at>\r\n" +
                            "Newsgroups: tu-graz.lv.ew\r\n" +
                            "Subject: Re: [Assignment 1] Frage zu 1.1.4 - Bewertungsfunktion\r\n" +
                            "Date: Thu, 31 Mar 2011 14:00:36 +0200\r\n" +
                            "Organization: Technische Universitaet Graz\r\n" +
                            "Lines: 16\r\n" +
                            "Message-ID: <in1qd6$af4$1@fstgss00.tu-graz.ac.at>\r\n" +
                            "References: <imsmu9$5eo$1@fstgss00.tu-graz.ac.at>\r\n" +
                            "NNTP-Posting-Host: 88-117-13-180.adsl.highway.telekom.at\r\n" +
                            "Mime-Version: 1.0\r\n" +
                            "Content-Type: text/plain; charset=ISO-8859-1; format=flowed\r\n" +
                            "Content-Transfer-Encoding: 8bit\r\n" +
                            "X-Trace: fstgss00.tu-graz.ac.at 1301572838 10724 88.117.13.180 (31 Mar 2011 12:00:38 GMT)\r\n" +
                            "X-Complaints-To: news@tugraz.at\r\n" +
                            "NNTP-Posting-Date: Thu, 31 Mar 2011 12:00:38 +0000 (UTC)\r\n" +
                            "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; de; rv:1.9.2.15) Gecko/20110303 Lightning/1.0b2 Thunderbird/3.1.9\r\n" +
                            "In-Reply-To: <imsmu9$5eo$1@fstgss00.tu-graz.ac.at>\r\n" +
                            "Xref: news.tugraz.at tu-graz.lv.ew:2080\r\n" +
                            "\r\n" +
                            "Das beste alpha wäre jenes, bei dem der geringste Fehler am Test set \r\n" +
                            "gemacht wird. In diesem Fall verwendet man halt eine regularisierte \r\n" +
                            "Fehlerfunktion.\r\n" +
                            "\r\n" +
                            "LG\r\n" +
                            "\r\n" +
                            "Am 29.03.2011 15:30, schrieb Wolfgang Wieser:\r\n" +
                            "> Hallo,\r\n" +
                            ">\r\n" +
                            "> bei Aufgabe 1.1.4 müssen wir ja Plot in Abhängigkeit der Güte von alpha\r\n" +
                            "> machen. Wie wird denn bewertet wie gut das alpha ist?\r\n" +
                            "> Mein Ansatz wäre gewesen die Summe vom MSE-Vektor zu verwenden.\r\n" +
                            ">\r\n" +
                            "> Schon Mal vielen Dank für die Antwort\r\n" +
                            "> Wolfgang Wieser\r\n" +
                            "\r\n" +
                            ".\r\n";


            byte[] encodedBytes = Encoding.GetEncoding("ISO-8859-1").GetBytes(raw_data);

            FireDummy fire = new FireDummy();
            IParse parse = new ParseARTICLE(fire);

            parse.parse(encodedBytes);

            Assert.IsTrue(fire.Error == EInternalError.Unknown);

            Assert.IsFalse(fire.Article == null);
            Assert.IsTrue(fire.Article.ArticleNumber == 2080);
            Assert.IsTrue(fire.Article.Date.Year == 2011 && fire.Article.Date.Month == 3 &&
                fire.Article.Date.Day == 31 && fire.Article.Date.Hour == 14 &&
                fire.Article.Date.Minute == 0 && fire.Article.Date.Second == 36);
            Assert.IsTrue(fire.Article.From == "Wolfgang Kopp <wolfgang.kopp@student.tugraz.at>");
            Assert.IsTrue(fire.Article.MessageId == "<in1qd6$af4$1@fstgss00.tu-graz.ac.at>");
            Assert.IsTrue(fire.Article.Newsgroup == "tu-graz.lv.ew");
            Assert.IsTrue(fire.Article.Organization == "Technische Universitaet Graz");
            Assert.IsTrue(fire.Article.Subject == "Re: [Assignment 1] Frage zu 1.1.4 - Bewertungsfunktion");
            Assert.IsTrue(fire.Article.Message == "Das beste alpha wäre jenes, bei dem der geringste Fehler am Test set \r\n" +
                "gemacht wird. In diesem Fall verwendet man halt eine regularisierte \r\n" +
                "Fehlerfunktion.\r\n" +
                "\r\n" +
                "LG\r\n" +
                "\r\n" +
                "Am 29.03.2011 15:30, schrieb Wolfgang Wieser:\r\n" +
                "> Hallo,\r\n" +
                ">\r\n" +
                "> bei Aufgabe 1.1.4 müssen wir ja Plot in Abhängigkeit der Güte von alpha\r\n" +
                "> machen. Wie wird denn bewertet wie gut das alpha ist?\r\n" +
                "> Mein Ansatz wäre gewesen die Summe vom MSE-Vektor zu verwenden.\r\n" +
                ">\r\n" +
                "> Schon Mal vielen Dank für die Antwort\r\n" +
                "> Wolfgang Wieser\r\n");
        }

        [TestMethod]
        public static void DataParserARTICLE3Test() {
            string text = "220 49673 <jo14qe$8mm$1@news.tugraz.at> article\r\n" +
                            "Path: news.tugraz.at!not-for-mail\r\n" +
                            "From: \"Thomas Tscherning\" <thomas.tscherning@gmx.at>\r\n" +
                            "Newsgroups: tu-graz.test\r\n" +
                            "Subject: Re: test12345\r\n" +
                            "Date: Fri, 4 May 2012 19:48:46 +0200\r\n" +
                            "Organization: Technische Universitaet Graz\r\n" +
                            "Lines: 5\r\n" +
                            "Message-ID: <jo14qe$8mm$1@news.tugraz.at>\r\n" +
                            "References: <jo14pl$8l6$1@news.tugraz.at>\r\n" +
                            "NNTP-Posting-Host: 84-119-42-232.dynamic.xdsl-line.inode.at\r\n" +
                            "Mime-Version: 1.0\r\n" +
                            "Content-Type: text/plain;\r\n" +
                            "	format=flowed;\r\n" +
                            "	charset=\"iso-8859-1\";\r\n" +
                            "	reply-type=response\r\n" +
                            "Content-Transfer-Encoding: 7bit\r\n" +
                            "X-Trace: news.tugraz.at 1336153742 8918 84.119.42.232 (4 May 2012 17:49:02 GMT)\r\n" +
                            "X-Complaints-To: news@tugraz.at\r\n" +
                            "NNTP-Posting-Date: Fri, 4 May 2012 17:49:02 +0000 (UTC)\r\n" +
                            "In-Reply-To: <jo14pl$8l6$1@news.tugraz.at>\r\n" +
                            "X-Priority: 3\r\n" +
                            "X-MSMail-Priority: Normal\r\n" +
                            "X-Newsreader: Microsoft Windows Mail 6.0.6002.18197\r\n" +
                            "X-MimeOLE: Produced By Microsoft MimeOLE V6.0.6002.18463\r\n" +
                            "Xref: news.tugraz.at tu-graz.test:49673\r\n" +
                            "\r\n" +
                            "blablabla\r\n" +
                            "\"Thomas Tscherning\" <thomas.tscherning@gmx.at> schrieb im Newsbeitrag \r\n" +
                            "news:jo14pl$8l6$1@news.tugraz.at...\r\n" +
                            "> \r\n" +
                            "\r\n" +
                            ".\r\n";

            FireDummy fire = new FireDummy();
            IParse parse = new ParseARTICLE(fire);
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            parse.parse(data);
            Assert.IsTrue(fire.Error == EInternalError.Unknown);

            Assert.IsFalse(fire.Article == null);
            Assert.IsTrue(fire.Article.ArticleNumber == 49673);
            Assert.IsTrue(fire.Article.Date.Year == 2012 && fire.Article.Date.Month == 5 &&
                fire.Article.Date.Day == 4 && fire.Article.Date.Hour == 19 &&
                fire.Article.Date.Minute == 48 && fire.Article.Date.Second == 46);
            Assert.IsTrue(fire.Article.From == "\"Thomas Tscherning\" <thomas.tscherning@gmx.at>");
            Assert.IsTrue(fire.Article.MessageId == "<jo14qe$8mm$1@news.tugraz.at>");
            Assert.IsTrue(fire.Article.Newsgroup == "tu-graz.test");
            Assert.IsTrue(fire.Article.Organization == "Technische Universitaet Graz");
            Assert.IsTrue(fire.Article.Subject == "Re: test12345");
            Assert.IsTrue(fire.Article.Message == "blablabla\r\n" +
                            "\"Thomas Tscherning\" <thomas.tscherning@gmx.at> schrieb im Newsbeitrag \r\n" +
                            "news:jo14pl$8l6$1@news.tugraz.at...\r\n" +
                            "> \r\n");
        }

        [TestMethod]
        public static void DataParserARTICLE4Test() {
            string text = "220 49630 <jn2qjn$r8o$1@news.tugraz.at> article\r\n" +
                            "Path: news.tugraz.at!not-for-mail\r\n" +
                            "From: Armin Koefler <armin.koefler@student.tugraz.at>\r\n" +
                            "Newsgroups: tu-graz.test\r\n" +
                            "Subject: werwr\r\n" +
                            "Date: Mon, 23 Apr 2012 05:50:47 +0000 (UTC)\r\n" +
                            "Organization: Technische Universitaet Graz\r\n" +
                            "Lines: 1\r\n" +
                            "Message-ID: <jn2qjn$r8o$1@news.tugraz.at>\r\n" +
                            "NNTP-Posting-Host: webnews.tu-graz.ac.at\r\n" +
                            "Mime-Version: 1.0\r\n" +
                            "Content-Type: text/plain; charset=ISO-8859-1\r\n" +
                            "Content-Transfer-Encoding: 7bit\r\n" +
                            "X-Trace: news.tugraz.at 1335160247 27928 129.27.124.213 (23 Apr 2012 05:50:47 GMT)\r\n" +
                            "X-Complaints-To: news@tugraz.at\r\n" +
                            "NNTP-Posting-Date: Mon, 23 Apr 2012 05:50:47 +0000 (UTC)\r\n" +
                            "X-NNTP-Posting-Host: 77.118.252.161\r\n" +
                            "Xref: news.tugraz.at tu-graz.test:49630\r\n" +
                            "\r\n" +
                            "wrwerwrw\r\n" +
                            ".\r\n";

            FireDummy fire = new FireDummy();
            IParse parse = new ParseARTICLE(fire);
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            parse.parse(data);
            Assert.IsTrue(fire.Error == EInternalError.Unknown);

            Assert.IsFalse(fire.Article == null);
            Assert.IsTrue(fire.Article.ArticleNumber == 49630);
            Assert.IsTrue(fire.Article.Date.Year == 2012 && fire.Article.Date.Month == 4 &&
                fire.Article.Date.Day == 23 && fire.Article.Date.Hour == 7 &&
                fire.Article.Date.Minute == 50 && fire.Article.Date.Second == 47);
            Assert.IsTrue(fire.Article.From == "Armin Koefler <armin.koefler@student.tugraz.at>");
            Assert.IsTrue(fire.Article.MessageId == "<jn2qjn$r8o$1@news.tugraz.at>");
            Assert.IsTrue(fire.Article.Newsgroup == "tu-graz.test");
            Assert.IsTrue(fire.Article.Organization == "Technische Universitaet Graz");
            Assert.IsTrue(fire.Article.Subject == "werwr");
            Assert.IsTrue(fire.Article.Message == "wrwerwrw");
        }

        [TestMethod]
        public static void DataParserARTICLEFailedTest() {
            string text = "220 3000234 <45223423@example.com>\r\n" +
                            "Path: pathost!demo!whitehouse!not-for-mail\r\n" +
                            "From: \"Demo User\" <nobody@example.net>\r\n" +
                            "Newsgroups: misc.test\r\n" +
                            "Subject: I am just a test article\r\n" +
                            "Date: 6 Oct 1998 05:38:40 -0500\r\n" +
                            "Organization: An Example Net, Uncertain, Texas\r\n" +
                            "Message-ID: <45223423@example.com>\r\n" +
                            "This is just a test article.\r\n" +
                            "Line two.\r\n" +
                            ".\r\n";
            FireDummy fire = new FireDummy();
            IParse parse = new ParseARTICLE(fire);
            byte[] data = Utils.DefaultEncoding.GetBytes(text);
            parse.parse(data);

            Assert.IsTrue(fire.Error == EInternalError.ArticleParsingFailed);
        }

        #endregion

        #region STUFF

        [TestMethod]
        public static void ThreadTimer1Test() {
            int count = 0;

            ThreadTimer tht1 = new ThreadTimer();
            tht1.Interval = TimeSpan.FromMilliseconds(1000);
            ManualResetEvent wait1 = new ManualResetEvent(false);
            tht1.Tick += new ThreadTimer.EventHandler(delegate(object sender, EventArgs e) {
                count = count | 1;
                wait1.Set();
            });
            tht1.Start();
            wait1.Reset();

            ThreadTimer tht2 = new ThreadTimer();
            tht2.Interval = TimeSpan.FromMilliseconds(1000);
            DateTime date1 = DateTime.Now;
            ManualResetEvent wait2 = new ManualResetEvent(false);
            tht2.Tick += new ThreadTimer.EventHandler(delegate(object sender, EventArgs e) {
                TimeSpan span = DateTime.Now - date1;
                Assert.IsTrue(span.TotalMilliseconds > 1000);
                count = count | 2;
                wait2.Set();
            });
            tht2.Start();
            wait2.Reset();

            ThreadTimer tht3 = new ThreadTimer();
            tht3.Interval = TimeSpan.FromMilliseconds(500);
            DateTime date2 = DateTime.Now;
            ManualResetEvent wait3 = new ManualResetEvent(false);
            tht3.Tick += new ThreadTimer.EventHandler(delegate(object sender, EventArgs e) {
                TimeSpan span = DateTime.Now - date2;
                Assert.IsTrue(span.TotalMilliseconds > 500);
                count = count | 4;
                wait3.Set();
            });
            tht3.Start();
            wait3.Reset();

            ThreadTimer tht4 = new ThreadTimer();
            tht4.Interval = TimeSpan.FromMilliseconds(250);
            DateTime date3 = DateTime.Now;
            ManualResetEvent wait4 = new ManualResetEvent(false);
            tht4.Tick += new ThreadTimer.EventHandler(delegate(object sender, EventArgs e) {
                TimeSpan span = DateTime.Now - date3;
                Assert.IsTrue(span.TotalMilliseconds > 250);
                count = count | 8;
                wait4.Set();
            });
            tht4.Start();
            wait4.Reset();

            wait1.WaitOne(TIMEOUT);
            wait2.WaitOne(TIMEOUT);
            wait3.WaitOne(TIMEOUT);
            wait4.WaitOne(TIMEOUT);
            Assert.IsTrue(count == 15);
        }

        [TestMethod]
        public static void ThreadTimer2Test() {
            bool called = false;
            ThreadTimer tht2 = new ThreadTimer();
            tht2.Interval = TimeSpan.FromMilliseconds(500);

            ThreadTimer tht1 = new ThreadTimer();
            tht1.Interval = TimeSpan.FromMilliseconds(500);
            ManualResetEvent wait1 = new ManualResetEvent(false);
            tht1.Tick += new ThreadTimer.EventHandler(delegate(object sender, EventArgs e) {
                tht2.Start();
                tht2.Tick += new ThreadTimer.EventHandler(delegate(object sender2, EventArgs e2) {
                    called = true;
                    wait1.Set();
                });

            });
            tht1.Start();
            wait1.Reset();
            wait1.WaitOne(TIMEOUT);
            Assert.IsTrue(called);
        }

        [TestMethod]
        public static void ThreadTimer3Test() {
            int called = 0;
            ThreadTimer tht1 = new ThreadTimer();
            tht1.Interval = TimeSpan.FromMilliseconds(100);
            ManualResetEvent wait1 = new ManualResetEvent(false);
            tht1.Tick += new ThreadTimer.EventHandler(delegate(object sender, EventArgs e) {
                called += 1;
                wait1.Set();
            });
            tht1.Start();
            tht1.Start();
            tht1.Start();
            wait1.Reset();
            wait1.WaitOne(TIMEOUT);
            tht1.Start();
            wait1.Reset();
            wait1.WaitOne(TIMEOUT);
            tht1.Start();
            tht1.Start();
            wait1.Reset();
            wait1.WaitOne(TIMEOUT);
            Assert.IsTrue(called == 3);
        }

        [TestMethod]
        public static void UtilsCopyTest() {
            String test_str = "this is a simple test.";

            string sub_str = test_str.Substring(3, 5);
            
            byte[] sub_arry = Utils.DefaultEncoding.GetBytes(test_str);
            sub_arry = Utils.Copy(sub_arry, 3, 5);
            string sub_str2 = new string(Utils.DefaultEncoding.GetChars(sub_arry));

            Assert.IsTrue(sub_str == sub_str2);
        }

        [TestMethod]
        public static void UtilsIndexOfTest() {
            String test_str = "this is a \r\n\r\nsimple test.";

            byte[] data = Utils.DefaultEncoding.GetBytes(test_str);
            int index1 = test_str.IndexOf("\r\n\r\n");
            int index2 = Utils.IndexOf(data, Utils.DefaultEncoding.GetBytes("\r\n\r\n"));

            Assert.IsTrue(index1 == index2);
        }

        [TestMethod]
        public static void UtilsIndexOf2Test() {
            String test_str =  "211 2000 3000234 3002322 misc.test list follows\r\n" +
                "3000234\r\n" +
                "3000237\r\n" +
                "3000238\r\n" +
                "3000239\r\n" +
                "3002322\r\n" +
                ".\r\n";

            byte[] data = Utils.DefaultEncoding.GetBytes(test_str);
            int index1 = test_str.IndexOf("\r\n.\r\n");
            int index2 = Utils.IndexOf(data, Utils.DefaultEncoding.GetBytes("\r\n.\r\n"));

            Assert.IsTrue(index1 == index2);
        }

        [TestMethod]
        public static void UtilsIndexOf3Test() {
            String test_str = "\r\n\r\nthis is a simple test.";

            byte[] data = Utils.DefaultEncoding.GetBytes(test_str);
            int index1 = test_str.IndexOf("\r\n\r\n");
            int index2 = Utils.IndexOf(data, Utils.DefaultEncoding.GetBytes("\r\n\r\n"));

            Assert.IsTrue(index1 == index2);
        }

        [TestMethod]
        public static void UtilsCombine1Test() {
            String test_str1 = "this is a simple test.";
            String test_str2 = "adding this.";


            byte[] data1 = Utils.DefaultEncoding.GetBytes(test_str1);
            byte[] data2 = Utils.DefaultEncoding.GetBytes(test_str2);

            test_str1 += test_str2;

            byte[] data = Utils.Combine(data1, data2);
            string test_data = new string(Utils.DefaultEncoding.GetChars(data));

            Assert.IsTrue(test_data == test_str1);
        }

        [TestMethod]
        public static void UtilsCombine2Test() {
            String test_str1 = "";
            String test_str2 = "adding this.";


            byte[] data1 = Utils.DefaultEncoding.GetBytes(test_str1);
            byte[] data2 = Utils.DefaultEncoding.GetBytes(test_str2);

            test_str1 += test_str2;

            byte[] data = Utils.Combine(data1, data2);
            string test_data = new string(Utils.DefaultEncoding.GetChars(data));

            Assert.IsTrue(test_data == test_str1);
        }

        [TestMethod]
        public static void UtilsCombine3Test() {
            String test_str1 = "this is a simple test.";
            String test_str2 = "";


            byte[] data1 = Utils.DefaultEncoding.GetBytes(test_str1);
            byte[] data2 = Utils.DefaultEncoding.GetBytes(test_str2);

            test_str1 += test_str2;

            byte[] data = Utils.Combine(data1, data2);
            string test_data = new string(Utils.DefaultEncoding.GetChars(data));

            Assert.IsTrue(test_data == test_str1);
        }

        [TestMethod]
        public static void UtilsCombine4Test() {
            String test_str1 = "this is a simple test.";
            String test_str2 = null;


            byte[] data1 = Utils.DefaultEncoding.GetBytes(test_str1);
            byte[] data2 = null;

            test_str1 += test_str2;

            byte[] data = Utils.Combine(data1, data2);
            string test_data = new string(Utils.DefaultEncoding.GetChars(data));

            Assert.IsTrue(test_data == test_str1);
        }

        [TestMethod]
        public static void UtilsCombine5Test() {
            String test_str1 = null;
            String test_str2 = "adding this.";


            byte[] data1 = null;
            byte[] data2 = Utils.DefaultEncoding.GetBytes(test_str2); 

            test_str1 += test_str2;

            byte[] data = Utils.Combine(data1, data2);
            string test_data = new string(Utils.DefaultEncoding.GetChars(data));

            Assert.IsTrue(test_data == test_str1);
        }

        #endregion
    }
}
