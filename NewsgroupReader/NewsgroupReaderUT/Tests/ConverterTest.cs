﻿using System.Windows;
using System.Windows.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewsgroupReader.Converters;
using Microsoft.Silverlight.Testing;

namespace NewsgroupReaderUT
{
    [TestClass]
    [Tag("Converters")]
    public class ConverterTest
    {
        [TestMethod]
        [Description("This test checks the correctness of the BoolToVisibleConverter.")]
        public static void TestBoolToVisibleConverter()
        {
            BoolToVisibleConverter conv = new BoolToVisibleConverter();
            object output = conv.Convert((object)true, null, null, null);
            Assert.IsTrue(((System.Windows.Visibility)output) == System.Windows.Visibility.Visible);

            output = conv.Convert((object)false, null, null, null);
            Assert.IsTrue(((System.Windows.Visibility)output) == System.Windows.Visibility.Collapsed);

            output = conv.ConvertBack((object)System.Windows.Visibility.Collapsed, null, null, null);
            Assert.IsTrue(((bool)output) == false);

            output = conv.ConvertBack((object)System.Windows.Visibility.Visible, null, null, null);
            Assert.IsTrue(((bool)output) == true);
        }

        [TestMethod]
        [Description("This test checks the correctness of the IntToBackColorConverter.")]
        public static void TestIntToBackColorConverter()
        {
            IntToBackColorConverter conv = new IntToBackColorConverter();
            object output = conv.Convert((object)70, null, null, null);
            Color temp = (Color)Application.Current.Resources["PhoneAccentColor"];
            Color out1 = ((SolidColorBrush)output).Color;
            Assert.IsTrue((out1.A == temp.A) && (out1.B == temp.B) && (out1.G == temp.G) && (out1.R == temp.R));

            output = conv.Convert((object)0, null, null, null);
            temp.A = 128;
            out1 = ((SolidColorBrush)output).Color;
            Assert.IsTrue((out1.A == temp.A) && (out1.B == temp.B) && (out1.G == temp.G) && (out1.R == temp.R));
        }

        [TestMethod]
        [Description("This test checks the correctness of the IntToMarginLengthConverter.")]
        public static void TestIntToMarginLengthConverter()
        {
            IntToMarginLengthConverter conv = new IntToMarginLengthConverter();

            object output = conv.Convert((object)0, null, null, null);
            System.Windows.Thickness temp = new System.Windows.Thickness(25, 0, 0, 0);
            Assert.IsTrue(((System.Windows.Thickness)output) == temp);

            output = conv.Convert((object)5, null, null, null);
            temp = new System.Windows.Thickness(29, -5, 0, 0);
            Assert.IsTrue(((System.Windows.Thickness)output) == temp);

            output = conv.Convert((object)50, null, null, null);
            temp = new System.Windows.Thickness(0, -5, 0, 0);
            Assert.IsTrue(((System.Windows.Thickness)output) == temp);

            output = conv.Convert((object)550, null, null, null);
            temp = new System.Windows.Thickness(20, -15, 0, 0);
            Assert.IsTrue(((System.Windows.Thickness)output) == temp);
        }

        [TestMethod]
        [Description("This test checks the correctness of the IntToTextConverter.")]
        public static void TestIntToTextConverter()
        {
            IntToTextConverter conv = new IntToTextConverter();

            object output = conv.Convert((object)0, null, null, null);
            Assert.IsTrue(((string)output).Equals("°"));

            output = conv.Convert((object)10, null, null, null);
            Assert.IsTrue(((int)output) == 10);

            output = conv.Convert((object)110, null, null, null);
            Assert.IsTrue(((string)output).Equals(".."));
        }
    }
}
