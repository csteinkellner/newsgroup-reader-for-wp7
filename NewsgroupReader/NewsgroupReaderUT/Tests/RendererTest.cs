﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewsgroupReader.Classes;
using Microsoft.Silverlight.Testing;

namespace NewsgroupReaderUT.Tests
{
    [TestClass]
    [Tag("Renderer")]
    public class RendererTest
    {
        // Header
        public static String HTMLHeaderLight = "<!DOCTYPE html>\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\r\n<head>\r\n<meta name=\"HandheldFriendly\" content=\"true\" />\r\n<meta name=\"Viewport\" content=\"width=device-width\" />\r\n<meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\" />\r\n<title>Post</title>\r\n<style type=\"text/css\">\r\n<!--\r\nbody {\r\n  background-color: rgb(255, 255, 255);\r\n  color: rgb(0, 0, 0);\r\n  padding: 0px;\r\n  margin: 0px;\r\n}\r\n\r\ndiv {\r\n  padding-left: 7px;\r\n  padding-right: 7px;\r\n  padding-top: 2px;\r\n  padding-bottom: 2px;\r\n  margin-top: 2px;\r\n  margin-bottom: 2px;\r\n}\r\n\r\n.citation {\r\n  border-left-style:solid;\r\n  border-left-width:3px;\r\n  border-right-style:solid;\r\n  border-right-width:3px;\r\n}\r\n\r\n.signature {\r\n  background-color: rgb(242, 242, 242);\r\n  color: rgb(127, 127, 127);\r\n  border-top-style:dashed;\r\n  border-top-width:3px;\r\n  padding-top: 5px;\r\n}\r\n\r\n.bold {\r\n  font-weight: bold;\r\n}\r\n\r\n.italic {\r\n  font-style:italic;\r\n}\r\n\r\n.underline {\r\n  text-decoration:underline;\r\n}\r\n\r\n\n.level_0 {\n  background-color: rgba(27, 161, 226, 0.1);\n  border-color: rgb(27, 161, 226);\n}\n\n.level_1 {\n  background-color: rgba(160, 80, 0, 0.1);\n  border-color: rgb(160, 80, 0);\n}\n\n.level_2 {\n  background-color: rgba(51, 153, 51, 0.1);\n  border-color: rgb(51, 153, 51);\n}\n\n.level_3 {\n  background-color: rgba(162, 193, 57, 0.1);\n  border-color: rgb(162, 193, 57);\n}\n\n.level_4 {\n  background-color: rgba(216, 0, 115, 0.1);\n  border-color: rgb(216, 0, 115);\n}\n\n.level_5 {\n  background-color: rgba(240, 150, 9, 0.1);\n  border-color: rgb(240, 150, 9);\n}\n\n.level_6 {\n  background-color: rgba(230, 113, 184, 0.1);\n  border-color: rgb(230, 113, 184);\n}\n\n.level_7 {\n  background-color: rgba(162, 0, 255, 0.1);\n  border-color: rgb(162, 0, 255);\n}\n\n.level_8 {\n  background-color: rgba(229, 20, 0, 0.1);\n  border-color: rgb(229, 20, 0);\n}\n\n.level_9 {\n  background-color: rgba(0, 171, 169, 0.1);\n  border-color: rgb(0, 171, 169);\n}\n\r\n}\r\n-->\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n";
        public static String HTMLHeaderDark =  "<!DOCTYPE html>\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\r\n<head>\r\n<meta name=\"HandheldFriendly\" content=\"true\" />\r\n<meta name=\"Viewport\" content=\"width=device-width\" />\r\n<meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\" />\r\n<title>Post</title>\r\n<style type=\"text/css\">\r\n<!--\r\nbody {\r\n  background-color: rgb(0, 0, 0);\r\n  color: rgb(255, 255, 255);\r\n  padding: 0px;\r\n  margin: 0px;\r\n}\r\n\r\ndiv {\r\n  padding-left: 7px;\r\n  padding-right: 7px;\r\n  padding-top: 2px;\r\n  padding-bottom: 2px;\r\n  margin-top: 2px;\r\n  margin-bottom: 2px;\r\n}\r\n\r\n.citation {\r\n  border-left-style:solid;\r\n  border-left-width:3px;\r\n  border-right-style:solid;\r\n  border-right-width:3px;\r\n}\r\n\r\n.signature {\r\n  background-color: rgb(13, 13, 13);\r\n  color: rgb(127, 127, 127);\r\n  border-top-style:dashed;\r\n  border-top-width:3px;\r\n  padding-top: 5px;\r\n}\r\n\r\n.bold {\r\n  font-weight: bold;\r\n}\r\n\r\n.italic {\r\n  font-style:italic;\r\n}\r\n\r\n.underline {\r\n  text-decoration:underline;\r\n}\r\n\r\n\n.level_0 {\n  background-color: rgba(27, 161, 226, 0.1);\n  border-color: rgb(27, 161, 226);\n}\n\n.level_1 {\n  background-color: rgba(160, 80, 0, 0.1);\n  border-color: rgb(160, 80, 0);\n}\n\n.level_2 {\n  background-color: rgba(51, 153, 51, 0.1);\n  border-color: rgb(51, 153, 51);\n}\n\n.level_3 {\n  background-color: rgba(162, 193, 57, 0.1);\n  border-color: rgb(162, 193, 57);\n}\n\n.level_4 {\n  background-color: rgba(216, 0, 115, 0.1);\n  border-color: rgb(216, 0, 115);\n}\n\n.level_5 {\n  background-color: rgba(240, 150, 9, 0.1);\n  border-color: rgb(240, 150, 9);\n}\n\n.level_6 {\n  background-color: rgba(230, 113, 184, 0.1);\n  border-color: rgb(230, 113, 184);\n}\n\n.level_7 {\n  background-color: rgba(162, 0, 255, 0.1);\n  border-color: rgb(162, 0, 255);\n}\n\n.level_8 {\n  background-color: rgba(229, 20, 0, 0.1);\n  border-color: rgb(229, 20, 0);\n}\n\n.level_9 {\n  background-color: rgba(0, 171, 169, 0.1);\n  border-color: rgb(0, 171, 169);\n}\n\r\n}\r\n-->\r\n</style>\r\n</head>\r\n<body>\r\n<div>\r\n";
        public static String HTMLFooter = "</div>\r\n</body>\r\n</html>";

        [TestMethod]
        [Description("This test checks the correct visualization of bold text on a light background.")]
        public static void LightBoldTest()
        {
            String output = HTMLRenderer.renderPostToHTML("*Hello World*", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + "<span class=\"bold\">Hello World</span><br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of bold text on a dark background.")]
        public static void DarkBoldTest()
        {
            String output = HTMLRenderer.renderPostToHTML("*Hello World*", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + "<span class=\"bold\">Hello World</span><br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of italic text on a light background.")]
        public static void LightItalicTest()
        {
            String output = HTMLRenderer.renderPostToHTML("/Hello World/", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + "<span class=\"italic\">Hello World</span><br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of italic text on a dark background.")]
        public static void DarkItalicTest()
        {
            String output = HTMLRenderer.renderPostToHTML("/Hello World/", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + "<span class=\"italic\">Hello World</span><br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of underlined text on a light background.")]
        public static void LightUnderlineTest()
        {
            String output = HTMLRenderer.renderPostToHTML("_Hello World_", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + "<span class=\"underline\">Hello World</span><br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of underlined text on a dark background.")]
        public static void DarkUnderlineTest()
        {
            String output = HTMLRenderer.renderPostToHTML("_Hello World_", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + "<span class=\"underline\">Hello World</span><br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of quotes on a light background.")]
        public static void LightCitationTest()
        {
            String output = HTMLRenderer.renderPostToHTML("> Hello World", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + "<div class=\"citation level_0\"> Hello World<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of quotes on a light background.")]
        public static void DarkCitationTest()
        {
            String output = HTMLRenderer.renderPostToHTML("> Hello World", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + "<div class=\"citation level_0\"> Hello World<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :O.")]
        public static void DarkSmileyAstonished()
        {
            String output = HTMLRenderer.renderPostToHTML(":O", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyAstonishedLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :).")]
        public static void DarkSmileySmile()
        {
            String output = HTMLRenderer.renderPostToHTML(":)", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileySmileLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :(.")]
        public static void DarkSmileySad()
        {
            String output = HTMLRenderer.renderPostToHTML(":(", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileySadLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: o_O.")]
        public static void DarkSmileyWth()
        {
            String output = HTMLRenderer.renderPostToHTML("o_O", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyWthLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :P.")]
        public static void DarkSmileyTongue()
        {
            String output = HTMLRenderer.renderPostToHTML(":P", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyTongueLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :3.")]
        public static void DarkSmileyHappy()
        {
            String output = HTMLRenderer.renderPostToHTML(":3", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyHappyLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :D.")]
        public static void DarkSmileyBigSmile()
        {
            String output = HTMLRenderer.renderPostToHTML(":D", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyBigSmileLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: xD.")]
        public static void DarkSmileyLOL()
        {
            String output = HTMLRenderer.renderPostToHTML("xD", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyLOLLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: ;).")]
        public static void DarkSmileyWink()
        {
            String output = HTMLRenderer.renderPostToHTML(";)", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyWinkLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :-O.")]
        public static void DarkSmileyAstonishedAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-O", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyAstonishedLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :-).")]
        public static void DarkSmileySmileAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-)", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileySmileLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :-(.")]
        public static void DarkSmileySadAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-(", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileySadLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :-P.")]
        public static void DarkSmileyTongueAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-P", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyTongueLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :-3.")]
        public static void DarkSmileyHappyAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-3", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyHappyLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: :-D.")]
        public static void DarkSmileyBigSmileAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-D", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyBigSmileLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: XD.")]
        public static void DarkSmileyLOLAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML("XD", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyLOLLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (dark background) of the following smiley: ;-).")]
        public static void DarkSmileyWinkAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(";-)", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + NewsgroupReader.Resources.HTMLResources.SmileyWinkLight + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :O.")]
        public static void LightSmileyAstonished()
        {
            String output = HTMLRenderer.renderPostToHTML(":O", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyAstonishedDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :).")]
        public static void LightSmileySmile()
        {
            String output = HTMLRenderer.renderPostToHTML(":)", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileySmileDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :(.")]
        public static void LightSmileySad()
        {
            String output = HTMLRenderer.renderPostToHTML(":(", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileySadDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: o_O.")]
        public static void LightSmileyWth()
        {
            String output = HTMLRenderer.renderPostToHTML("o_O", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyWthDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :P.")]
        public static void LightSmileyTongue()
        {
            String output = HTMLRenderer.renderPostToHTML(":P", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyTongueDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :3.")]
        public static void LightSmileyHappy()
        {
            String output = HTMLRenderer.renderPostToHTML(":3", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyHappyDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :D.")]
        public static void LightSmileyBigSmile()
        {
            String output = HTMLRenderer.renderPostToHTML(":D", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyBigSmileDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: xD.")]
        public static void LightSmileyLOL()
        {
            String output = HTMLRenderer.renderPostToHTML("xD", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyLOLDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: ;).")]
        public static void LightSmileyWink()
        {
            String output = HTMLRenderer.renderPostToHTML(";)", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyWinkDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :-O.")]
        public static void LightSmileyAstonishedAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-O", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyAstonishedDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :-).")]
        public static void LightSmileySmileAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-)", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileySmileDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :-(.")]
        public static void LightSmileySadAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-(", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileySadDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :-P.")]
        public static void LightSmileyTongueAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-P", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyTongueDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :-3.")]
        public static void LightSmileyHappyAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-3", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyHappyDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: :-D.")]
        public static void LightSmileyBigSmileAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(":-D", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyBigSmileDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: XD.")]
        public static void LightSmileyLOLAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML("XD", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyLOLDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization (light background) of the following smiley: ;-).")]
        public static void LightSmileyWinkAlternative()
        {
            String output = HTMLRenderer.renderPostToHTML(";-)", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + NewsgroupReader.Resources.HTMLResources.SmileyWinkDark + "<br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of signatures on a light background.")]
        public static void LightSignatureTest()
        {
            String output = HTMLRenderer.renderPostToHTML("Hello World!\n-- \nHello Signature!", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + "Hello World!<br /><div class=\"signature\">Hello Signature!</div>\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of signatures on a dark background.")]
        public static void DarkSignatureTest()
        {
            String output = HTMLRenderer.renderPostToHTML("Hello World!\n-- \nHello Signature!", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + "Hello World!<br /><div class=\"signature\">Hello Signature!</div>\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of some generic text on a dark background.")]
        public static void DarkNormalTest()
        {
            String output = HTMLRenderer.renderPostToHTML("Hello World!\n*Bold*_Underlined_/Italic/", false, Colors.Black);
            String expectedOutput = HTMLHeaderDark + "Hello World!<br /><span class=\"bold\">Bold</span><span class=\"underline\">Underlined</span><span class=\"italic\">Italic</span><br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }

        [TestMethod]
        [Description("This test checks the correct visualization of some generic text on a light background.")]
        public static void LightNormalTest()
        {
            String output = HTMLRenderer.renderPostToHTML("Hello World!\n*Bold*_Underlined_/Italic/", true, Colors.Black);
            String expectedOutput = HTMLHeaderLight + "Hello World!<br /><span class=\"bold\">Bold</span><span class=\"underline\">Underlined</span><span class=\"italic\">Italic</span><br />\r\n" + HTMLFooter;
            Assert.IsTrue(output == expectedOutput);
        }
    }
}
