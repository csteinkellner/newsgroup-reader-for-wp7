﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using NNTPLibrary.Parsing;
using NNTPLibrary.Models;
using System.Net.Sockets;

namespace NewsgroupReaderUT.Tests
{
    public class FireDummy : IFire
    {
        public NNTPGroupList Groups=null;
        public NNTPArticleList Articles = null;
        public NNTPArticle Article = null;

        public bool ConnectionEstablished = false;
        public ENNTPConnection Status = ENNTPConnection.Unknown;
        public EInternalError Error = EInternalError.Unknown;

        public void CallAllNewsgroupsCompleted(NNTPGroupList groups)
        {
            Groups = groups;
        }

        public void CallNewsgroupAndListArticlesCompleted(NNTPArticleList articles)
        {
            Articles = articles;
        }

        public void CallArticleCompleted(NNTPArticle article)
        {
            Article = article;
        }

        public void CallInternalError(EInternalError error) {
            Error = error;
        }

        public void CallConnectionStatus(bool establised, SocketError error, ENNTPConnection status) {
            ConnectionEstablished = establised;
            this.Status = status;
        }
    }
}
