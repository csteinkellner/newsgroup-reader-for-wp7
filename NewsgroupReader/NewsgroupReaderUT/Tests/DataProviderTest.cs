﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Silverlight.Testing;
using NewsgroupReader.DataProvider;
using System.Collections.Generic;
using NewsgroupReader.Models;

namespace NewsgroupReaderUT.Tests {
    [TestClass]
    [Tag("DataProvider")]
    public class DataProviderTest {
        const string NNTP_SERVER_ADDRESS = "news.tugraz.at";
        const string NNTP_SERVER_NEWSGROUP = "tu-graz.test";

        //[TestMethod]
        //public static void ConnectionTest()
        //{
        //    DataProvider.getInstance(NNTP_SERVER_ADDRESS).GetAllThreadsFromServer(NNTP_SERVER_NEWSGROUP);
        //    DataProvider.getInstance(NNTP_SERVER_ADDRESS).GetAllThreadsEvent_Handler
        //        += new DataProvider.GetAllThreadsEvent(
        //            delegate(List<NewsgroupReader.Models.ArticleItem> rootPosts)
        //            {
        //                Assert.IsTrue(rootPosts.Count > 0);
        //            });
        //}

        private static NntpDatabase nntpDB;
        private static DataProvider dataProvider;

        private static ServerItem getServerItem()
        {
            ServerItem item = new ServerItem("name", "description", "url", "userName", "userMail", 10);
            GroupItem group1 = new GroupItem("group1");
            item.SubscribedGroups.Add(group1);
            GroupItem group2 = new GroupItem("group2");
            item.SubscribedGroups.Add(group2);
            GroupItem group3 = new GroupItem("group3");
            item.SubscribedGroups.Add(group3);
            return item;
        }

        static int test_count = 0;
        private static void fillDB()
        {
            if (test_count == 0)
            {
                NewsgroupReader.DataProvider.NntpDatabase.AddNewsServer(getServerItem());
                nntpDB = new NntpDatabase(getServerItem().Url);

                DateTime date = new DateTime(2012, 6, 7);
                List<ArticleItem> articleItems = new List<ArticleItem>();
                string[] references1 = { };
                string[] references2 = { "messageId1" };
                string[] references3 = { "messageId1", "messageId2" };

                articleItems.Add(new ArticleItem(11, "path", "from", "group1", "subject", date, "org", "messageId1", "message", true, references1, "utf-8", "trans"));
                articleItems.Add(new ArticleItem(22, "path", "from", "group1", "subject", date, "org", "messageId2", "message", true, references2, "utf-8", "trans"));
                articleItems.Add(new ArticleItem(33, "path", "from", "group1", "subject", date, "org", "messageId3", "message", true, references3, "utf-8", "trans"));
                articleItems.Add(new ArticleItem(44, "path", "from", "group2", "subject", date, "org", "messageId4", "message", true, references1, "utf-8", "trans"));
                articleItems.Add(new ArticleItem(55, "path", "from", "group3", "subject", date, "org", "messageId5", "message", true, references1, "utf-8", "trans"));
                articleItems.Add(new ArticleItem(66, "path", "from", "group3", "subject", date, "org", "messageId6", "message", true, references1, "utf-8", "trans"));

                nntpDB.AddArticles(articleItems);

                dataProvider = DataProvider.getInstance("url");
            }
        }

        private static void deleteDB()
        {
            if (test_count == 1)
                DatabaseAccess.ViewModels.NNTPDatabase.DeleteNewsServer(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0]);
            test_count++;
        }

        //[TestMethod]
        //public static void TestGetAllThreadPosts()
        //{
        //    fillDB();

        //    DataProvider dataProvider = DataProvider.getInstance("url");
        //    DateTime date = new DateTime(2012, 6, 7);
        //    string[] references1 = { };
        //    ArticleItem article = new ArticleItem(11, "path", "from", "group1", "subject", date, "org", "messageId1", "message", true, references1, "utf-8", "trans");
        //    List<ArticleItem> items = dataProvider.GetAllThreadPosts(article);

        //    Assert.IsTrue(items.Count == 2);
        //    Assert.IsTrue(items[0].ArticleNumber == 22);
        //    Assert.IsTrue(items[1].ArticleNumber == 33);

        //    deleteDB();
        //}

        //[TestMethod]
        //public static void TestGetAllThreads()
        //{
        //    fillDB();

        //    List<ArticleItem> items = dataProvider.GetAllThreadsFromDB("group1");

        //    Assert.IsTrue(items.Count == 1);
        //    Assert.IsTrue(items[0].ArticleNumber == 11);

        //    deleteDB();
        //}
    }
}
