﻿using System.Windows;
using System.Windows.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Silverlight.Testing;
using DatabaseAccess;
using DatabaseAccess.Models;
using DatabaseAccess.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using NewsgroupReader;
using NewsgroupReader.DataProvider;
using NewsgroupReader.Models;

namespace NewsgroupReaderUT.Tests
{
    [TestClass]
    [Tag("nntpDB")]
    public class NntpDatabaseTest
    {
        private static NntpDatabase nntpDB;
        private static ServerItem getServerItem()
        {
            ServerItem item = new ServerItem("name", "description", "url", "userName", "userMail", 10);
            GroupItem group1 = new GroupItem("group1");
            item.SubscribedGroups.Add(group1);
            GroupItem group2 = new GroupItem("group2");
            item.SubscribedGroups.Add(group2);
            GroupItem group3 = new GroupItem("group3");
            item.SubscribedGroups.Add(group3);
            return item;
        }

        private static void fillDB()
        {
            NewsgroupReader.DataProvider.NntpDatabase.AddNewsServer(getServerItem());
            nntpDB = new NntpDatabase(getServerItem().Url);

            DateTime date = new DateTime(2012, 6, 7);
            List<ArticleItem> articleItems = new List<ArticleItem>();
            string[] references = { "eins", "zwei", "drei" };

            articleItems.Add(new ArticleItem(11, "path", "from", "group1", "subject", date, "org", "messageId1", "message", true, references, "utf-8", "trans"));
            articleItems.Add(new ArticleItem(22, "path", "from", "group1", "subject", date, "org", "messageId2", "message", true, references, "utf-8", "trans"));
            articleItems.Add(new ArticleItem(33, "path", "from", "group1", "subject", date, "org", "messageId3", "message", true, references, "utf-8", "trans"));
            articleItems.Add(new ArticleItem(44, "path", "from", "group2", "subject", date, "org", "messageId4", "message", true, references, "utf-8", "trans"));
            articleItems.Add(new ArticleItem(55, "path", "from", "group3", "subject", date, "org", "messageId5", "message", true, references, "utf-8", "trans"));
            articleItems.Add(new ArticleItem(66, "path", "from", "group3", "subject", date, "org", "messageId6", "message", true, references, "utf-8", "trans"));

            nntpDB.AddArticles(articleItems);
        }

        private static void deleteDB()
        {
            DatabaseAccess.ViewModels.NNTPDatabase.DeleteNewsServer(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers[0]);
        }

        [TestMethod]
        [Description("Test Add single Article")]
        public static void TestAddArticle()
        {
            fillDB();

            DateTime date = new DateTime(2012, 6, 7);
            string[] references = { "eins", "zwei", "drei" };
            ArticleItem item = new ArticleItem(77, "path", "from", "group3", "subject", date, "org", "messageId7", "message", true, references, "utf-8", "trans");
            nntpDB.AddArticle(item);
            List<ArticleItem> list3 = nntpDB.GetArticlesByGroup("group3");
            Assert.IsTrue(list3.Count == 3);

            deleteDB();
        }

        [TestMethod]
        [Description("Test Newsserver Creation")]
        public static void TestServerCreate()
        {
            fillDB();

            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers.Count == 1);

            deleteDB();
        }

        [TestMethod]
        [Description("Test Newsgroup creation")]
        public static void TestGroupCreate()
        {
            fillDB();

            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count == 3);

            deleteDB();
        }

        [TestMethod]
        [Description("Test Article creation")]
        public static void TestArticleCreate()
        {
            fillDB();

            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count == 6);

            deleteDB();
        }

        [TestMethod]
        [Description("Test Get all Articles by Group")]
        public static void TestGetAllArticlesByGroup()
        {
            fillDB();

            List<ArticleItem> list1 = nntpDB.GetArticlesByGroup("group1");
            Assert.IsTrue(list1.Count == 3);

            List<ArticleItem> list2 = nntpDB.GetArticlesByGroup("group2");
            Assert.IsTrue(list2.Count == 1);

            List<ArticleItem> list3 = nntpDB.GetArticlesByGroup("group3");
            Assert.IsTrue(list3.Count == 2);

            Assert.IsTrue(list1[1].ArticleNumber == 22);

            deleteDB();
        }

        [TestMethod]
        [Description("Test Get Server Items")]
        public static void TestGetServerItems()
        {
            fillDB();

            List<ServerItem> list = NewsgroupReader.DataProvider.NntpDatabase.getServerItems();
            Assert.IsTrue(list.Count == 1);
            Assert.IsTrue(list[0].Name.Equals("name"));

            deleteDB();
        }

        [TestMethod]
        [Description("Test Get Groups")]
        public static void TestGetGroups()
        {
            fillDB();

            List<GroupItem> list = nntpDB.getGroups();
            Assert.IsTrue(list.Count == 3);
            Assert.IsTrue(list[1].Name.Equals("group2"));

            deleteDB();
        }

        [TestMethod]
        [Description("Test Get Articles")]
        public static void TestGetArticles()
        {
            fillDB();

            List<ArticleItem> list = nntpDB.GetArticlesByServer();
            Assert.IsTrue(list.Count == 6);
            Assert.IsTrue(list[3].ArticleNumber == 44);

            deleteDB();
        }

        [TestMethod]
        [Description("Test newsServer deletion")]
        public static void TestDeleteNewsServer()
        {
            fillDB();
            ServerItem item = getServerItem();
            NewsgroupReader.DataProvider.NntpDatabase.DeleteNewsServer(item);
            
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllArticles.Count == 0);
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count == 0);
            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsServers.Count == 0);
        }

        [TestMethod]
        [Description("Test update server")]
        public static void TestUpdateServer()
        {
            fillDB();

            ServerItem item = new ServerItem("name", "description", "url", "userName", "userMail", 10);
            GroupItem group1 = new GroupItem("group1");
            item.SubscribedGroups.Add(group1);
            GroupItem group2 = new GroupItem("group4");
            item.SubscribedGroups.Add(group2);

            nntpDB.UpdateGroups(item);

            Assert.IsTrue(DatabaseAccess.ViewModels.NNTPDatabase.AllNewsGroups.Count == 2);
            List<GroupItem> items = nntpDB.getGroups();
            Assert.IsTrue(items[0].Description.Equals("group1") || items[1].Description.Equals("group1"));
            Assert.IsTrue(items[0].Description.Equals("group4") || items[1].Description.Equals("group4"));

            deleteDB();
        }
    }
}
