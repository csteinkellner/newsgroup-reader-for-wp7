﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using Microsoft.Phone.Data.Linq;
using Microsoft.Phone.Data.Linq.Mapping;
using System.ComponentModel;

namespace DatabaseAccess.Models
{
    public class NNTPDataContext : DataContext
    {
        // Pass the connection string to the base class.
        public NNTPDataContext(string connectionString)
            : base(connectionString)
        {
 
        }

        // Specify a table for the to-do items.
        public Table<NewsServer> Servers;

        // Specify a table for the categories.
        public Table<NewsGroup> NewsGroups;

        public Table<Article> Articles;
    }

    // Define the NewsServer items database table.
    [Table]
    public class NewsServer : INotifyPropertyChanged, INotifyPropertyChanging
    {
        // Define ID: private field, public property, and database column.
        private int _newsServerId;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int NewsServerId
        {
            get
            {
                return _newsServerId;
            }
            set
            {
                if (_newsServerId != value)
                {
                    NotifyPropertyChanging("NewsServerId");
                    _newsServerId = value;
                    NotifyPropertyChanged("NewsServerId");
                }
            }
        }

        private string _newsServerName;
        [Column]
        public string NewsServerName
        {
            get { return _newsServerName; }
            set
            {
                if (_newsServerName != value)
                {
                    NotifyPropertyChanging("NewsServerName");
                    _newsServerName = value;
                    NotifyPropertyChanged("NewsServerName");
                }
            }
        }

        private string _newsServerDescription;
        [Column]
        public string NewsServerDescription
        {
            get { return _newsServerDescription; }
            set
            {
                if (_newsServerDescription != value)
                {
                    NotifyPropertyChanging("NewsServerDescription");
                    _newsServerDescription = value;
                    NotifyPropertyChanged("NewsServerDescription");
                }
            }
        }

        private string _newsServerUrl;
        [Column]
        public string NewsServerUrl
        {
            get { return _newsServerUrl; }
            set
            {
                if (_newsServerUrl != value)
                {
                    NotifyPropertyChanging("NewsServerUrl");
                    _newsServerUrl = value;
                    NotifyPropertyChanged("NewsServerUrl");
                }
            }
        }

        private string _newsServerUserName;
        [Column]
        public string NewsServerUserName
        {
            get { return _newsServerUserName; }
            set
            {
                if (_newsServerUserName != value)
                {
                    NotifyPropertyChanging("NewsServerUserName");
                    _newsServerUserName = value;
                    NotifyPropertyChanged("NewsServerUserName");
                }
            }
        }

        private string _newsServerUserMail;
        [Column]
        public string NewsServerUserMail
        {
            get { return _newsServerUserMail; }
            set
            {
                if (_newsServerUserMail != value)
                {
                    NotifyPropertyChanging("NewsServerUserMail");
                    _newsServerUserMail = value;
                    NotifyPropertyChanged("NewsServerUserMail");
                }
            }
        }

        private EntitySet<NewsGroup> _newsGroups;
        [Association(Storage = "_newsGroups", OtherKey = "_newsServerId", ThisKey = "NewsServerId")]
        public EntitySet<NewsGroup> NewsGroups
        {
            get { return this._newsGroups; }
            set { this._newsGroups.Assign(value); }
        }

        // Assign handlers for the add and remove operations, respectively.
        public NewsServer()
        {
            _newsGroups = new EntitySet<NewsGroup>(
                new Action<NewsGroup>(this.attach_NewsGroup),
                new Action<NewsGroup>(this.detach_NewsGroup)
                );
        }

        // Called during an add operation
        private void attach_NewsGroup(NewsGroup newsGroup)
        {
            NotifyPropertyChanging("NewsGroup");
            newsGroup.NewsServer = this;
        }

        // Called during a remove operation
        private void detach_NewsGroup(NewsGroup newsGroup)
        {
            NotifyPropertyChanging("NewsGroup");
            newsGroup.NewsServer = null;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify that a property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify that a property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }

    //---------------------------------------------------------------------------------------------
    [Table]
    public class NewsGroup : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _newsGroupId;

        [Column(DbType = "INT NOT NULL IDENTITY", IsDbGenerated = true, IsPrimaryKey = true)]
        public int NewsGroupId
        {
            get { return _newsGroupId; }
            set
            {
                NotifyPropertyChanging("NewsGroupId");
                _newsGroupId = value;
                NotifyPropertyChanged("NewsGroupId");
            }
        }


        private string _newsGroupName;

        [Column]
        public string NewsGroupName
        {
            get { return _newsGroupName; }
            set
            {
                NotifyPropertyChanging("NewsGroupName");
                _newsGroupName = value;
                NotifyPropertyChanged("NewsGroupName");
            }
        }

        [Column]
        internal int _newsServerId;
        private EntityRef<NewsServer> _newsServer;
        [Association(Storage = "_newsServer", ThisKey = "_newsServerId", OtherKey = "NewsServerId", IsForeignKey = true)]
        public NewsServer NewsServer
        {
            get { return _newsServer.Entity; }
            set
            {
                NotifyPropertyChanging("NewsServer");
                _newsServer.Entity = value;

                if (value != null)
                {
                    _newsServerId = value.NewsServerId;
                }

                NotifyPropertyChanging("NewsServer");
            }
        }

        private EntitySet<Article> _articles;
        [Association(Storage = "_articles", OtherKey = "_newsGroupId", ThisKey = "NewsGroupId")]
        public EntitySet<Article> Articles
        {
            get { return this._articles; }
            set { this._articles.Assign(value); }
        }
        
        public NewsGroup()
        {
            _articles = new EntitySet<Article>(
                new Action<Article>(this.attach_Article),
                new Action<Article>(this.detach_Article)
                );
        }

        // Called during an add operation
        private void attach_Article(Article article)
        {
            NotifyPropertyChanging("Article");
            article.NewsGroup = this;
        }

        // Called during a remove operation
        private void detach_Article(Article article)
        {
            NotifyPropertyChanging("Article");
            article.NewsGroup = null;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify that a property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify that a property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }

    //---------------------------------------------------------------------------------------------
    [Table]
    public class Article : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _articleId;

        [Column(DbType = "INT NOT NULL IDENTITY", IsDbGenerated = true, IsPrimaryKey = true)]
        public int ArticleId
        {
            get { return _articleId; }
            set
            {
                NotifyPropertyChanging("ArticleId");
                _articleId = value;
                NotifyPropertyChanged("ArticleId");
            }
        }

        private int _articleNumber;

        [Column]
        public int ArticleNumber
        {
            get { return _articleNumber; }
            set
            {
                NotifyPropertyChanging("ArticleNumber");
                _articleNumber = value;
                NotifyPropertyChanged("ArticleNumber");
            }
        }

        private string _articlePath;

        [Column]
        public string ArticlePath
        {
            get { return _articlePath; }
            set
            {
                NotifyPropertyChanging("ArticlePath");
                _articlePath = value;
                NotifyPropertyChanged("ArticlePath");
            }
        }

        private string _articleFrom;

        [Column]
        public string ArticleFrom
        {
            get { return _articleFrom; }
            set
            {
                NotifyPropertyChanging("ArticleFrom");
                _articleFrom = value;
                NotifyPropertyChanged("ArticleFrom");
            }
        }

        private string _articleNG;

        [Column]
        public string ArticleNG
        {
            get { return _articleNG; }
            set
            {
                NotifyPropertyChanging("ArticleNG");
                _articleNG = value;
                NotifyPropertyChanged("ArticleNG");
            }
        }

        private string _articleSubject;

        [Column]
        public string ArticleSubject
        {
            get { return _articleSubject; }
            set
            {
                NotifyPropertyChanging("ArticleSubject");
                _articleSubject = value;
                NotifyPropertyChanged("ArticleSubject");
            }
        }

        private DateTime _articleDate;

        [Column]
        public DateTime ArticleDate
        {
            get { return _articleDate; }
            set
            {
                NotifyPropertyChanging("ArticleDate");
                _articleDate = value;
                NotifyPropertyChanged("ArticleDate");
            }
        }

        private string _articleOrganization;

        [Column]
        public string ArticleOrganization
        {
            get { return _articleOrganization; }
            set
            {
                NotifyPropertyChanging("ArticleOrganization");
                _articleOrganization = value;
                NotifyPropertyChanged("ArticleOrganization");
            }
        }

        private string _articleMessageId;

        [Column]
        public string ArticleMessageId 
        {
            get { return _articleMessageId; }
            set
            {
                NotifyPropertyChanging("ArticleMessageId");
                _articleMessageId = value;
                NotifyPropertyChanged("ArticleMessageId");
            }
        }

        private string _articleReferences;
        [Column]
        public string ArticleReferences
        {
            get { return _articleReferences; }
            set
            {
                NotifyPropertyChanging("ArticleReferences");
                _articleReferences = value;
                NotifyPropertyChanged("ArticleReferences");
            }
        }

        private string _articleMessage;

        [Column(DbType = "NText", CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
        public string ArticleMessage
        {
            get { return _articleMessage; }
            set
            {
                NotifyPropertyChanging("ArticleMessage");
                _articleMessage = value;
                NotifyPropertyChanged("ArticleMessage");
            }
        }

        private bool _articleUnread;

        [Column]
        public bool ArticleUnread
        {
            get { return _articleUnread; }
            set
            {
                NotifyPropertyChanging("ArticleUnread");
                _articleUnread = value;
                NotifyPropertyChanged("ArticleUnread");
            }
        }

        private string _articleContentType;

        [Column]
        public string ArticleContentType
        {
            get { return _articleContentType; }
            set
            {
                NotifyPropertyChanging("ArticleContentType");
                _articleContentType = value;
                NotifyPropertyChanged("ArticleContentType");
            }
        }

        private string _articleContentTransferEncoding;

        [Column]
        public string ArticleContentTransferEncoding
        {
            get { return _articleContentTransferEncoding; }
            set
            {
                NotifyPropertyChanging("ArticleContentTransferEncoding");
                _articleContentTransferEncoding = value;
                NotifyPropertyChanged("ArticleContentTransferEncoding");
            }
        }

        [Column]
        internal int _newsGroupId;

        private EntityRef<NewsGroup> _newsGroup;
        [Association(Storage = "_newsGroup", ThisKey = "_newsGroupId", OtherKey = "NewsGroupId", IsForeignKey = true)]
        public NewsGroup NewsGroup
        {
            get { return _newsGroup.Entity; }
            set
            {
                NotifyPropertyChanging("NewsGroup");
                _newsGroup.Entity = value;

                if (value != null)
                {
                    _newsGroupId = value.NewsGroupId;
                }

                NotifyPropertyChanging("NewsGroup");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify that a property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify that a property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
