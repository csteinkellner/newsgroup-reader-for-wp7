﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using Microsoft.Phone.Data.Linq;
using Microsoft.Phone.Data.Linq.Mapping;
using System.ComponentModel;

namespace DatabaseAccess
{
    public class DataContext
    {

    }

    public class ToDoDataContext : DataContext
    {
        // Specify the connection string as a static, used in main page and app.xaml.
        public static string DBConnectionString = "Data Source=isostore:/ToDo.sdf";

        // Pass the connection string to the base class.
        public ToDoDataContext(string connectionString): base(connectionString) { }

        // Specify a single table for the to-do items.
        public Table<ToDoItem> ToDoItems;
    }

    // Define the NewsServer items database table.
    [Table]
    public class ToDoItem : INotifyPropertyChanged, INotifyPropertyChanging
    {
        // Define ID: private field, public property, and database column.
        private int _newsServerId;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int NewsServerId
        {
            get
            {
                return _newsServerId;
            }
            set
            {
                if (_newsServerId != value)
                {
                    NotifyPropertyChanging("NewsServerId");
                    _newsServerId = value;
                    NotifyPropertyChanged("NewsServerId");
                }
            }
        }

        private string _newsServerName;

        [Column]
        public string NewsServerName
        {
            get { return _newsServerName; }
            set
            {
                if (_newsServerName != value)
                {
                    NotifyPropertyChanging("NewsServerName");
                    _newsServerName = value;
                    NotifyPropertyChanged("NewsServerName");
                }
            }
        }

        private string _newsServerAddress;

        [Column]
        public string NewsServerAddress
        {
            get { return _newsServerAddress; }
            set
            {
                if (_newsServerAddress != value)
                {
                    NotifyPropertyChanging("NewsServerAddress");
                    _newsServerName = value;
                    NotifyPropertyChanged("NewsServerAddress");
                }
            }
        }

        private string _newsServerUserName;

        [Column]
        public string NewsServerUserName
        {
            get { return _newsServerUserName; }
            set
            {
                if (_newsServerUserName != value)
                {
                    NotifyPropertyChanging("NewsServerUserName");
                    _newsServerUserName = value;
                    NotifyPropertyChanged("NewsServerUserName");
                }
            }
        }

        private string _newsServerUserMail;

        [Column]
        public string NewsServerUserMail
        {
            get { return _newsServerUserMail; }
            set
            {
                if (_newsServerUserMail != value)
                {
                    NotifyPropertyChanging("NewsServerUserMail");
                    _newsServerUserMail = value;
                    NotifyPropertyChanged("NewsServerUserMail");
                }
            }
        }

        [Column]
        internal int _newsGroupForeignId;

        private EntityRef<NewsGroup> _newsGroupForeign;
        [Association(Storage = "_newsGroupForeign", ThisKey = "_newsGroupForeignId", OtherKey = "NewsGroupId", IsForeignKey = true)]
        public NewsGroup NewsGroupForeign
        {
            get { return _newsGroupForeign.Entity; }
            set
            {
                NotifyPropertyChanging("NewsGroupForeign");
                _newsGroupForeign.Entity = value;

                if (value != null)
                {
                    _newsGroupForeignId = value.NewsGroupId;
                }

                NotifyPropertyChanging("NewsGroupForeign");
            }
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify that a property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify that a property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }

    //---------------------------------------------------------------------------------------------
    [Table]
    public class NewsGroup : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _newsGroupId;

        [Column(DbType = "INT NOT NULL IDENTITY", IsDbGenerated = true, IsPrimaryKey = true)]
        public int NewsGroupId
        {
            get { return _newsGroupId; }
            set
            {
                NotifyPropertyChanging("NewsGroupId");
                _newsGroupId = value;
                NotifyPropertyChanged("NewsGroupId");
            }
        }


        private string _newsGroupName;

        [Column]
        public string NewsGroupName
        {
            get { return _newsGroupName; }
            set
            {
                NotifyPropertyChanging("NewsGroupName");
                _newsGroupName = value;
                NotifyPropertyChanged("NewsGroupName");
            }
        }

        private string _newsGroupAddress;

        [Column]
        public string NewsGroupAddress
        {
            get { return _newsGroupAddress; }
            set
            {
                NotifyPropertyChanging("NewsGroupAddress");
                _newsGroupAddress = value;
                NotifyPropertyChanged("NewsGroupAddress");
            }
        }


        private int _newsGroupUnreadArticles;

        [Column]
        public int NewsGroupUnreadArticles
        {
            get { return _newsGroupUnreadArticles; }
            set
            {
                NotifyPropertyChanging("NewsGroupUnreadArticles");
                _newsGroupUnreadArticles = value;
                NotifyPropertyChanged("NewsGroupUnreadArticles");
            }
        }


        [Column]
        internal int _articleForeignId;

        private EntityRef<Article> _articleForeign;
        [Association(Storage = "_articleForeign", ThisKey = "_articleForeignId", OtherKey = "ArticleId", IsForeignKey = true)]
        public Article ArticleForeign
        {
            get { return _articleForeign.Entity; }
            set
            {
                NotifyPropertyChanging("ArticleForeign");
                _articleForeign.Entity = value;

                if (value != null)
                {
                    _articleForeignId = value.ArticleId;
                }

                NotifyPropertyChanging("ArticleForeign");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify that a property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify that a property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }

    //---------------------------------------------------------------------------------------------
    [Table]
    public class Article : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _articleId;

        [Column(DbType = "INT NOT NULL IDENTITY", IsDbGenerated = true, IsPrimaryKey = true)]
        public int ArticleId
        {
            get { return _articleId; }
            set
            {
                NotifyPropertyChanging("ArticleId");
                _articleId = value;
                NotifyPropertyChanged("ArticleId");
            }
        }

        private int _articleNumber;

        [Column]
        public int ArticleNumber
        {
            get { return _articleNumber; }
            set
            {
                NotifyPropertyChanging("ArticleNumber");
                _articleNumber = value;
                NotifyPropertyChanged("ArticleNumber");
            }
        }

        private string _articlePath;

        [Column]
        public string ArticlePath
        {
            get { return _articlePath; }
            set
            {
                NotifyPropertyChanging("ArticlePath");
                _articlePath = value;
                NotifyPropertyChanged("ArticlePath");
            }
        }

        private string _articleFrom;

        [Column]
        public string ArticleFrom
        {
            get { return _articleFrom; }
            set
            {
                NotifyPropertyChanging("ArticleFrom");
                _articleFrom = value;
                NotifyPropertyChanged("ArticleFrom");
            }
        }

        private string _articleNG;

        [Column]
        public string ArticleNG
        {
            get { return _articleNG; }
            set
            {
                NotifyPropertyChanging("ArticleNG");
                _articleNG = value;
                NotifyPropertyChanged("ArticleNG");
            }
        }

        private string _articleSubject;

        [Column]
        public string ArticleSubject
        {
            get { return _articleSubject; }
            set
            {
                NotifyPropertyChanging("ArticleSubject");
                _articleSubject = value;
                NotifyPropertyChanged("ArticleSubject");
            }
        }

        private DateTime _articleDate;

        [Column]
        public DateTime ArticleDate
        {
            get { return _articleDate; }
            set
            {
                NotifyPropertyChanging("ArticleDate");
                _articleDate = value;
                NotifyPropertyChanged("ArticleDate");
            }
        }

        private string _articleOrganization;

        [Column]
        public string ArticleOrganization
        {
            get { return _articleOrganization; }
            set
            {
                NotifyPropertyChanging("ArticleOrganization");
                _articleOrganization = value;
                NotifyPropertyChanged("ArticleOrganization");
            }
        }

        private int _articleMessageId;

        [Column]
        public int ArticleMessageId
        {
            get { return _articleMessageId; }
            set
            {
                NotifyPropertyChanging("ArticleMessageId");
                _articleMessageId = value;
                NotifyPropertyChanged("ArticleMessageId");
            }
        }

        private string _articleMessage;

        [Column]
        public string ArticleMessage
        {
            get { return _articleMessage; }
            set
            {
                NotifyPropertyChanging("ArticleMessage");
                _articleMessage = value;
                NotifyPropertyChanged("ArticleMessage");
            }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify that a property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify that a property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
