﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace DatabaseAccess
{
    public class Class1
    {
        public void bla()
        {
            using (ToDoDataContext db = new ToDoDataContext("isostore:/ToDo.sdf"))
            {
                if (db.DatabaseExists() == false)
                {
                    // Create the database.
                    db.CreateDatabase();
                }


            }
            using (ToDoDataContext db = new ToDoDataContext(("isostore:/ToDo.sdf")))
            {
                //Create the database schema updater
                DatabaseSchemaUpdater dbUpdate = db.CreateDatabaseSchemaUpdater();

                //Get database version
                int dbVersion = dbUpdate.DatabaseSchemaVersion;

                //Update database as applicable
                if (dbVersion < 5)
                {   //Copy data from existing database to new database 
                    MigrateDatabaseToLatestVersion();
                }
                else if (dbVersion == 5)
                {   //Add column to existing database to match the data context
                    dbUpdate.AddColumn<ToDoItem>("TaskURL");
                    dbUpdate.DatabaseSchemaVersion = 6;
                    dbUpdate.Execute();
                }
            }

        }
    }
}
