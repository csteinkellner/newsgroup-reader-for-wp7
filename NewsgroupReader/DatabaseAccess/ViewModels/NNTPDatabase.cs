﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
 
using DatabaseAccess.Models;
using System;

namespace DatabaseAccess.ViewModels
{
    public class NNTPDatabase// : INotifyPropertyChanged
    {
        // LINQ to SQL data context for the local db
        private static NNTPDataContext nntpDB;

        // Class constructor, create the data context object.
        public NNTPDatabase(string nntpDBConnectionString)
        {
            nntpDB = new NNTPDataContext(nntpDBConnectionString);
            LoadCollectionsFromDatabase();
        }

        // All newsservers.
        private static List<NewsServer> _allNewsServers;
        public static List<NewsServer> AllNewsServers
        {
            get { return _allNewsServers; }
            set
            {
                if (_allNewsServers != value)
                {
                    _allNewsServers = value;
                }
            }
        }




        public static NewsServer GetNewsServerById(int id)
        {
            var query = from NewsServer server in nntpDB.Servers
                        where (server.NewsServerId == id)
                        select server;

            List<NewsServer> value = new List<NewsServer>(query.ToList<NewsServer>());
            if (value.Count == 0)
                return null;
            else
                return value[0];
        }


        public static int GetNewsServerIdByName(string server_dns)
        {
            var query = from NewsServer server1 in nntpDB.Servers
                        where (server1.NewsServerUrl == server_dns) 
                        select server1;
            List<NewsServer> result = new List<NewsServer>(query.ToList<NewsServer>());
            if (result.Count == 0)
                return -1;
            else
                return result[0].NewsServerId;
        }

        public static List<NewsServer> GetNewsServerByProperty(string name, string url)
        {
            var query = from NewsServer server1 in nntpDB.Servers
                        where (server1.NewsServerName == name) && (server1.NewsServerUrl == url)
                        select server1;
            return new List<NewsServer>(query.ToList<NewsServer>());
        }

        //----------------------------------------------------------------------
        // Newsgroup
        private static List<NewsGroup> _allNewsGroups;
        public static List<NewsGroup> AllNewsGroups
        {
            get { return _allNewsGroups; }
            set
            {
                if (_allNewsGroups != value)
                {
                    _allNewsGroups = value;
                }
            }
        }

        public static List<NewsGroup> GetNewsGroupsByServer(int server_id)
        {
            var query = from NewsGroup groups in nntpDB.NewsGroups
                        where groups.NewsServer.NewsServerId == server_id
                        select groups;
            return new List<NewsGroup>(query.ToList<NewsGroup>());
        }


        public static NewsGroup GetNewsGroupById(int id)
        {
            var query = from NewsGroup group1 in nntpDB.NewsGroups
                        where group1.NewsGroupId == id
                        select group1;

            List<NewsGroup> value = new List<NewsGroup>(query.ToList<NewsGroup>());
            if (value.Count == 0)
                return null;
            else
                return value[0];
        }

        // Maybe TODO: Get NewsGroups by Properties

        //----------------------------------------------------------------------
        // Articles
        private static List<Article> _allArticles;
        public static List<Article> AllArticles
        {
            get { return _allArticles; }
            set
            {
                if (_allArticles != value)
                {
                    _allArticles = value;
                }
            }
        }


        public static List<Article> GetArticlesByGroup(int groupId)
        {
            var query = from Article articles in nntpDB.Articles
                        where articles.NewsGroup.NewsGroupId == groupId
                        select articles;
            List<Article> result = new List<Article>(query.ToList<Article>());
            return result;
        }


        public static List<Article> GetArticlesByGroup(string dns_server, string newsGroup_name)
        {
            var query1 = from NewsGroup news_group in nntpDB.NewsGroups
                         where news_group.NewsServer.NewsServerUrl == dns_server
                         select news_group;
            List<NewsGroup> groups = new List<NewsGroup>(query1.ToList<NewsGroup>());
            NewsGroup group1 = null;
            for (int iterator = 0; iterator < groups.Count; iterator++)
            {
                if (groups[iterator].NewsGroupName.Equals(newsGroup_name))
                {
                    group1 = groups[iterator];
                    break;
                }
            }

            if (group1 == null)
                return null;
            return GetArticlesByGroup(group1.NewsGroupId);
        }

        public static List<Article> GetArticlesByGroup(int server_id, string newsGroup_name)
        {
            List<NewsGroup> groups = GetNewsGroupsByServer(server_id);
            NewsGroup group1 = null;
            for (int iterator = 0; iterator < groups.Count; iterator++)
            {
                if (groups[iterator].NewsGroupName.Equals(newsGroup_name))
                {
                    group1 = groups[iterator];
                    break;
                }
            }

            if (group1 == null)
                return null;
            return GetArticlesByGroup(group1.NewsGroupId);
        }

        public static Article GetArticleById(int id)
        {
            var query = from Article article in nntpDB.Articles
                        where article.ArticleId == id
                        select article;

            ObservableCollection<Article> value = new ObservableCollection<Article>(query);
            if (value.Count == 0)
                return null;
            else
                return value[0];
        }


        // Write changes in the data context to the database.
        public void SaveChangesToDB()
        {
            nntpDB.SubmitChanges();
        }

        // Query database and load the collections and list used by the pivot pages.
        private void LoadCollectionsFromDatabase()
        {

            // Specify the query for newsserver items in the database.
            var newsServersInDB = from NewsServer server in nntpDB.Servers
                                select server;
             
            // Query the database and load all newsserver items.
            AllNewsServers = new List<NewsServer>(newsServersInDB.ToList<NewsServer>());

            // Specify the query for all categories in the database.
            var newsGroupsInDB = from NewsGroup newsgroup in nntpDB.NewsGroups
                                     select newsgroup;
            AllNewsGroups = new List<NewsGroup>(newsGroupsInDB.ToList<NewsGroup>());

            var articlesInDB = from Article article in nntpDB.Articles select article;
            AllArticles = new List<Article>(articlesInDB.ToList<Article>());

        }

        //TODO Error-Handling
        // ---------------------------------------------------------------------
        // Add NewsServer
        public static void AddNewsServer(NewsServer server)
        {
            var query = from NewsServer newsServer in nntpDB.Servers
                        where (newsServer.NewsServerUrl == server.NewsServerUrl)
                        select newsServer;
            List<NewsServer> newsservers = new List<NewsServer>(query.ToList<NewsServer>());
            if (newsservers.Count == 0)
            {
                nntpDB.Servers.InsertOnSubmit(server);
                nntpDB.SubmitChanges();
                AllNewsServers.Add(server);
            }
            else
            {
                newsservers[0].NewsServerDescription = server.NewsServerDescription;
                newsservers[0].NewsServerName = server.NewsServerName;
                newsservers[0].NewsServerUrl = server.NewsServerUrl;
                newsservers[0].NewsServerUserMail = server.NewsServerUserMail;
                newsservers[0].NewsServerUserName = server.NewsServerUserName;
                nntpDB.SubmitChanges();
            }
        }

        public static void AddNewsServer(string name, string description, string url, string user_name, string user_mail)
        {
            NewsServer newServer = new NewsServer
            {
                NewsServerName = name,
                NewsServerDescription = description,
                NewsServerUrl = url,
                NewsServerUserName = user_name,
                NewsServerUserMail = user_mail
            };
            AddNewsServer(newServer);
        }

        // ---------------------------------------------------------------------
        // Delete NewsServer
        public static void DeleteNewsServer(NewsServer server)
        {
            NewsServer test_exists = GetNewsServerById(server.NewsServerId);
            if (test_exists == null)
                return;

            List<NewsGroup> groups = GetNewsGroupsByServer(server.NewsServerId);
            if (groups != null)
            {
                foreach (NewsGroup group1 in groups)
                {
                    List<Article> articles = GetArticlesByGroup(group1.NewsGroupId);
                    if (articles != null)
                    {
                        foreach (Article article in articles)
                            DeleteArticle(article);
                    }
                    DeleteNewsGroup(group1);
                }
            }

            AllNewsServers.Remove(server);
            nntpDB.Servers.DeleteOnSubmit(server);
            nntpDB.SubmitChanges();
        }

        public static void DeleteNewsServer(int server_id)
        {
            NewsServer to_delete = GetNewsServerById(server_id);
            if (to_delete != null)
                DeleteNewsServer(to_delete);
        }

        // ---------------------------------------------------------------------
        // Add NewsGroup
        public static void AddNewsGroup(NewsGroup group1)
        {
            var query = from NewsGroup newsGroup in nntpDB.NewsGroups
                        where ((newsGroup.NewsGroupName == group1.NewsGroupName) && (newsGroup.NewsServer.NewsServerId == group1.NewsServer.NewsServerId))
                        select newsGroup;
            List<NewsGroup> newsgroups = new List<NewsGroup>(query.ToList<NewsGroup>());
            if (newsgroups.Count == 0)
            {
                nntpDB.NewsGroups.InsertOnSubmit(group1);
                nntpDB.SubmitChanges();
                AllNewsGroups.Add(group1);
            }
        }

        public static void AddNewsGroup(string name,NewsServer server)
        {
            NewsGroup newGroup = new NewsGroup
            {
                NewsGroupName = name,
                NewsServer = server
            };
            AddNewsGroup(newGroup);
        }

        public static void AddNewsGroup(string name, int server_id)
        {
            NewsServer server = GetNewsServerById(server_id);
            if (server != null)
                AddNewsGroup(name, server);
        }

        // ---------------------------------------------------------------------
        // Delete NewsGroup
        public static void DeleteNewsGroup(NewsGroup group1)
        {
            NewsGroup test_exists = GetNewsGroupById(group1.NewsGroupId);
            if (test_exists == null)
                return;

            List<Article> articles = GetArticlesByGroup(group1.NewsGroupId);
            if (articles != null)
                foreach (Article article in articles)
                    DeleteArticle(article);

            AllNewsGroups.Remove(group1);
            nntpDB.NewsGroups.DeleteOnSubmit(group1);
            nntpDB.SubmitChanges();
        }

        public static void DeleteNewsGroup(int group_id)
        {
            NewsGroup group1 = GetNewsGroupById(group_id);
            if (group1 != null)
                DeleteNewsGroup(group1);
        } 

        // ---------------------------------------------------------------------
        // Add Article
        public static void AddArticle(Article article)
        {
            var query = from Article article1 in nntpDB.Articles
                        where ((article1.ArticleMessageId == article.ArticleMessageId) && (article1.NewsGroup.NewsServer.NewsServerId == article.NewsGroup.NewsServer.NewsServerId))
                        select article1;
            List<Article> articles = new List<Article>(query.ToList<Article>());
            if (articles.Count == 0)
            {
                nntpDB.Articles.InsertOnSubmit(article);
                nntpDB.SubmitChanges();
                AllArticles.Add(article);
            }
        }

        public static void AddArticle(DateTime date, string from, string message, string newsgroup, string message_id,
            int article_number, string organization, string path, string subject, bool unread, string references, string contentType, string contentTransferEncoding)
        {
            Article article = new Article
            {
                ArticleDate = date,
                ArticleFrom = from,
                ArticleMessage = message,
                ArticleNG = newsgroup,
                ArticleMessageId = message_id,
                ArticleNumber = article_number,
                ArticleOrganization = organization,
                ArticlePath = path,
                ArticleSubject = subject,
                ArticleUnread = unread,
                ArticleReferences = references,
                ArticleContentType = contentType,
                ArticleContentTransferEncoding = contentTransferEncoding
            };

            var query = from NewsGroup group1 in nntpDB.NewsGroups
                        where group1.NewsGroupName == newsgroup
                        select group1;

            List<NewsGroup> value = new List<NewsGroup>(query.ToList<NewsGroup>());
            if (value.Count != 0)
            {
                article.NewsGroup = value[0];
                AddArticle(article);
            }
        }

        public static void AddArticle(DateTime date, string from, string message, int newsGroup_id, string message_id,
            int article_number, string organization, string path, string subject, bool unread, string references, string contentType, string contentTransferEncoding)
        {
            Article article = new Article
            {
                ArticleDate = date,
                ArticleFrom = from,
                ArticleMessage = message,
                ArticleMessageId = message_id,
                ArticleNumber = article_number,
                ArticleOrganization = organization,
                ArticlePath = path,
                ArticleSubject = subject,
                ArticleUnread = unread,
                ArticleReferences = references,
                ArticleContentType = contentType,
                ArticleContentTransferEncoding = contentTransferEncoding
            };
            
            var query = from NewsGroup group1 in nntpDB.NewsGroups
                        where group1.NewsGroupId == newsGroup_id
                        select group1;

            List<NewsGroup> value = new List<NewsGroup>(query.ToList<NewsGroup>());
            if (value.Count != 0)
            {
                article.NewsGroup = value[0];
                article.ArticleNG = value[0].NewsGroupName;
                AddArticle(article);
            }
        }


        // ---------------------------------------------------------------------
        // Delete Article
        public static void DeleteArticle(Article article)
        {
            Article test_exists = GetArticleById(article.ArticleId);
            if (test_exists == null)
                return;
            AllArticles.Remove(article);
            nntpDB.Articles.DeleteOnSubmit(article);
            nntpDB.SubmitChanges();
        }

        public static void DeleteArticle(int article_id)
        {
            Article to_delete = GetArticleById(article_id);
            if (to_delete != null)
                DeleteArticle(to_delete);
        }

        // ---------------------------------------------------------------------
        // Change Articles
        public static void SetArticleUnread(bool unread, int article_id)
        {
            Article article = GetArticleById(article_id);
            if (article == null)
                return;
            article.ArticleUnread = unread;
            nntpDB.SubmitChanges();
        }

        public static void SetArticleMessage(string message, int article_id)
        {
            Article article = GetArticleById(article_id);
            if (article == null)
                return;
            article.ArticleMessage = message; 
            nntpDB.SubmitChanges();
        }
    }
}
